<%@ page import="java.sql.*" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%!
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static String HOST = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
	private static String PORT = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
	private static final String URL = "jdbc:mysql://"+HOST+":"+PORT+"/codingraja";
	private static final String USERNAME = System.getenv("OPENSHIFT_MYSQL_DB_USERNAME");
	private static final String PASSWORD = System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD");
	
	private static Connection connection = null;
	static {
		if(connection==null){
			try{
				Class.forName(DRIVER);
				connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			}catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		}
	}
%>

<%
	int i = 0;
	if(request.getMethod().equals("POST")) {
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String query = request.getParameter("query");
		String desc = request.getParameter("description");
		
		try{
			String sql = "INSERT INTO contact_master(full_name,email,query,description) VALUES(?,?,?,?)";
	
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1, name);
			pStatement.setString(2, email);
			pStatement.setString(3, query);
			pStatement.setString(4, desc);
			
			i = pStatement.executeUpdate();
	
		}catch (Exception ex) {
			
		}
	}
%>

<!doctype html>
<!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-gb" class="no-js">
<!--<![endif]-->
<head>
<title>Contact Us | CodingRAJA</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp"%>

</head>

<body>

	<div class="site_wrapper">

		<!-- #####################  Header Course Menu ######################## -->
		<%@ include file="/fragments/header.jsp"%>


		<div class="clearfix"></div>

		<div class="content_fullwidth less2">
			<div class="accor_dion">
				<div class="container">

					<div class="one_third last">
				        <div class="address_info two">
				          <h4 class="caps">Corporate Address</h4>
				          <ul>
				            <li> <strong>www.codingraja.com</strong><br />
				              Bangalore<br />
				              E-mail: <a href="#">info@codingraja.com</a><br />
				              Website: <a href="#">www.codingraja.com</a> </li>
				          </ul>
				         </div>
				    </div><!-- end section -->
        
					<div class="two_third">

						<div>
							<div id="form_status">
								<h3>Send your query here. Our technical team will contact you ASAP.</h3>
								<% if(i > 0) {%>
									<h4 style="color: green;">Your Query has been sent successfully!</h4>
								<% } %>
							</div>
							<form action="<c:url value="/support" />" method="post">
							    <div class="form-group row">
							      <label for="name" class="col-sm-2 col-form-label">Name *</label>
							      <div class="col-sm-10">
							        <input type="text" class="form-control" name="name" placeholder="Name" required="required">
							      </div>
							    </div>
							    <div class="form-group row">
							      <label for="email" class="col-sm-2 col-form-label">Email *</label>
							      <div class="col-sm-10">
							        <input type="email" class="form-control" name="email" placeholder="Email" required="required">
							      </div>
							    </div>
							    <div class="form-group row">
							      <label for="query" class="col-sm-2 col-form-label">Query *</label>
							      <div class="col-sm-10">
							        <input type="text" class="form-control" name="query" placeholder="Query" required="required">
							      </div>
							    </div>
							    <div class="form-group row">
							      <label for="description" class="col-sm-2 col-form-label">Description *</label>
							      <div class="col-sm-10">
							        <textarea rows="5" class="form-control" name="description" placeholder="Description" required="required"></textarea>
							      </div>
							    </div>
							    <div class="form-group row">
							      <div class="col-sm-12">
							      	<input type="submit" class="btn btn-danger btn-xl pull-right" value="Send Query">
							      </div>
							    </div>
							</form>
						</div>
					</div> <!-- end section -->
					
				</div> <!-- end container -->
			</div>
		</div>

		<div class="clearfix"></div>

		<!-- ##################   Footer   ################# -->
		<%@ include file="/fragments/footer.jsp"%>

		<div class="clearfix"></div>


		<a href="${resourceUrl}/#" class="scrollup">Scroll</a>
		<!-- end scroll to top of the page-->

	</div>


	<!-- ################## Footer Links ################# -->
	<%@ include file="/fragments/footer-links.jsp"%>

</body>

</html>
