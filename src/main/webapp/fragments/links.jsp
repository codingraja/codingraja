<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:url value="/resources" var="resourceUrl" />

	<!-- Favicon --> 
	<link rel="shortcut icon" href="${resourceUrl}/images/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
    
    <!--[if lt IE 9]>
		<script src="${resourceUrl}/http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="${resourceUrl}/css/reset.css" type="text/css" />
	<link rel="stylesheet" href="${resourceUrl}/css/style.css" type="text/css" />
    
    <!-- font awesome icons -->
    <link rel="stylesheet" href="${resourceUrl}/css/font-awesome/css/font-awesome.min.css">
	
    <!-- simple line icons -->
	<link rel="stylesheet" type="text/css" href="${resourceUrl}/css/simpleline-icons/simple-line-icons.css" media="screen" />
        
    <!-- animations -->
    <link href="${resourceUrl}/js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="${resourceUrl}/css/responsive-leyouts.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="${resourceUrl}/css/shortcodes.css" type="text/css" /> 
    
    <!-- style switcher -->
    <link rel = "stylesheet" media = "screen" href = "js/style-switcher/color-switcher.css" />
    
    <!-- mega menu -->
    <link href="${resourceUrl}/js/mainmenu/bootstrap.min.css" rel="stylesheet">
	<link href="${resourceUrl}/js/mainmenu/demo.css" rel="stylesheet">
	<link href="${resourceUrl}/js/mainmenu/menu.css" rel="stylesheet">
	
	<!-- owl carousel -->
    <link href="${resourceUrl}/js/carouselowl/owl.transitions.css" rel="stylesheet">
    <link href="${resourceUrl}/js/carouselowl/owl.carousel.css" rel="stylesheet">
    
    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="${resourceUrl}/js/accordion/style.css" />
    <link rel="stylesheet" href="${resourceUrl}/js/timeline/timeline.css">
    
	<script type="text/javascript" src="${resourceUrl}/syntaxhighlighter/scripts/shCore.js"></script>
	<script type="text/javascript" src="${resourceUrl}/syntaxhighlighter/scripts/shBrushJava.js"></script>
	<script type="text/javascript" src="${resourceUrl}/syntaxhighlighter/scripts/shBrushXml.js"></script>
	<script type="text/javascript" src="${resourceUrl}/syntaxhighlighter/scripts/shBrushCss.js"></script>
	<script type="text/javascript" src="${resourceUrl}/syntaxhighlighter/scripts/shBrushJScript.js"></script>
	<script type="text/javascript" src="${resourceUrl}/syntaxhighlighter/scripts/shBrushSql.js"></script>
	<script type="text/javascript">SyntaxHighlighter.all();</script>

	<link type="text/css" rel="stylesheet" href="${resourceUrl}/syntaxhighlighter/styles/shCoreEclipse.css"/>
  