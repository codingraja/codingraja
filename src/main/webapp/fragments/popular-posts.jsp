<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:url value="/resources" var="resourceUrl" />

<h2 style="margin-top: 20px;" class="caps"><strong>Popular Posts</strong></h2>
<div class="one_half"> 
        <div class="popular-posts-area">

           	<div class="clearfix margin_bottom1"></div>
               
			<ul class="recent_posts_list">
				<li>
					<span><a href="<c:url value="/articles/interviews/spring" />"><img style="height: 30px; width: 30px;" src="${resourceUrl}/images/icons/spring-icon.png" alt="Spring" /></a></span>
					<a href="<c:url value="/articles/interviews/spring" />">Spring Interview Questions and Answers</a>
					<i>August 25, 2017</i> 
				</li>
                   
			</ul>
			
		</div>
      </div><!-- end recent posts -->
      
      
      <div class="one_half last"> 
        <div class="popular-posts-area">

           	<div class="clearfix margin_bottom1"></div>
               
			<ul class="recent_posts_list">
				<li>
					<span><a href="<c:url value="/articles/interviews/hibernate" />"><img style="height: 30px; width: 30px;" src="${resourceUrl}/images/icons/hibernate-icon.png" alt="Hibernate" /></a></span>
					<a href="<c:url value="/articles/interviews/hibernate" />">Hibernate Interview Questions and Answers</a>
					<i>August 25, 2017</i> 
				</li>
                  
			</ul>
			
		</div>
      </div><!-- end popular posts -->
          
<div class="clearfix"></div>