<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:url value="/articles/images" var="imageUrl" />
<c:url value="/articles/core-java" var="coreJavaUrl" />
<c:url value="/articles/spring" var="springUrl" />
<c:url value="/articles/hibernate" var="hibernateUrl" />
<c:url value="/articles/servlet" var="servletUrl" />
<c:url value="/articles/spring" var="springUrl" />
<c:url value="/articles/interviews" var="interviewsUrl"></c:url>
<c:url value="/articles/logical-coding" var="logicalCodingUrl"></c:url>

<div class="top_nav">
	<div class="container">

		<div class="left">

			<div class="select-style">
				<select>
					<option value="english">English</option>
				</select>
			</div>

		</div>
		<!-- end left -->

		<div class="right">
			<ul class="tplinks">
				<!-- <li><strong><i class="fa fa-phone"></i> +91 9742 900
						696</strong></li> -->
				<li><a href="#"><img
						src="${resourceUrl}/images/site-icon1.png" alt="" />
						info@codingraja.com</a></li>
				<li><a href="<c:url value="/support" />"><img
						src="${resourceUrl}/images/site-icon3.png" alt="" /> Support</a></li>
			</ul>

		</div>
		<!-- end right -->

	</div>
</div>
<!-- end top navigation links -->


<div class="clearfix"></div>


<header class="header">

	<div class="container">

		<!-- Logo -->
		<div class="logo">
			<a href="<c:url value="/"/>" id="logo"></a>
		</div>

		<!-- Navigation Menu -->
		<div class="menu_main">

			<div class="navbar yamm navbar-default">

				<div class="navbar-header">
					<div class="navbar-toggle .navbar-collapse .pull-right "
						data-toggle="collapse" data-target="#navbar-collapse-1">
						<button type="button">
							<i class="fa fa-bars"></i>
						</button>
					</div>
				</div>

				<div id="navbar-collapse-1"
					class="navbar-collapse collapse pull-right">

					<nav>

						<ul class="nav navbar-nav">

							<li class="dropdown"><a href="<c:url value="/"/>"
								class="dropdown-toggle">Home</a></li>

							<%-- <li class="dropdown yamm-fw"><a href="${resourceUrl}/hosting1.html" class="dropdown-toggle">C</a>
                    <ul class="dropdown-menu">
                    <li> 
                    <div class="yamm-content">
                      <div class="row">
                                          
                     	<div class="mega-menu-contnew">
                        
                        	<div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting1.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Shared Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting2.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Reseller Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting3.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Cloud Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
						</div>
                        
					</div>
                    </div>
                    </li>
                    </ul>
                </li>
				
				<li class="dropdown yamm-fw"><a href="${resourceUrl}/hosting1.html" class="dropdown-toggle">DS</a>
                    <ul class="dropdown-menu">
                    <li> 
                    <div class="yamm-content">
                      <div class="row">
                                          
                     	<div class="mega-menu-contnew">
                        
                        	<div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting1.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Shared Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting2.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Reseller Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting3.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Cloud Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
						</div>
                        
					</div>
                    </div>
                    </li>
                    </ul>
                </li>
				
				<li class="dropdown yamm-fw"><a href="${resourceUrl}/hosting1.html" class="dropdown-toggle">Core Java</a>
                    <ul class="dropdown-menu">
                    <li> 
                    <div class="yamm-content">
                      <div class="row">
                                          
                     	<div class="mega-menu-contnew">
                        
                        	<div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting1.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Shared Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting2.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Reseller Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting3.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Cloud Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
						</div>
                        
					</div>
                    </div>
                    </li>
                    </ul>
                </li>
				
				<li class="dropdown yamm-fw"><a href="${resourceUrl}/hosting1.html" class="dropdown-toggle">Servlet</a>
                    <ul class="dropdown-menu">
                    <li> 
                    <div class="yamm-content">
                      <div class="row">
                                          
                     	<div class="mega-menu-contnew">
                        
                        	<div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting1.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Shared Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting2.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Reseller Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting3.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Cloud Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
						</div>
                        
					</div>
                    </div>
                    </li>
                    </ul>
                </li>
				
				<li class="dropdown yamm-fw"><a href="${resourceUrl}/hosting1.html" class="dropdown-toggle">JSP</a>
                    <ul class="dropdown-menu">
                    <li> 
                    <div class="yamm-content">
                      <div class="row">
                                          
                     	<div class="mega-menu-contnew">
                        
                        	<div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting1.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Shared Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting2.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Reseller Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting3.html"><span aria-hidden="true" class="icon-layers"></span> <strong>Cloud Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
                            <div class="section-box">
                            
                            	<a href="${resourceUrl}/hosting4.html"><span aria-hidden="true" class="icon-layers"></span> <strong>VPS Hosting</strong> Web sites versions the years</a>
                                
                            	<p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting5.html"><i class="icon-layers"></i> <strong>WordPress Hosting</strong> Web sites versions the years</a>
                                
                                <p class="clearfix margin_bottom4"></p>
                                
                                <a href="${resourceUrl}/hosting6.html"><i class="icon-layers"></i> <strong>Dedicated Hosting</strong> Web sites versions the years</a>
                                
							</div><!-- -->
                            
						</div>
                        
					</div>
                    </div>
                    </li>
                    </ul>
                </li> --%>

				<%-- 			<li class="dropdown yamm-fw"><a href="${hibernateUrl}/"
								class="dropdown-toggle">Hibernate</a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">

												<div class="mega-menu-contnew">

													<div class="section-box">

														<a href="${hibernateUrl}/introduction/"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Introduction</strong>
														</a>

														<p class="clearfix margin_bottom4"></p>

														<a href="${hibernateUrl}/crud-operations/"><span
															aria-hidden="true" class="icon-layers"></span> <strong>CRUD
																Operations</strong> </a>

														<p class="clearfix margin_bottom4"></p>

														<a href="${hibernateUrl}/collections-mapping/"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Collections
																Mapping</strong> </a>

													</div>
													<!-- -->

													<div class="section-box">

														<a href="${hibernateUrl}/association-mapping/"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Association
																Mapping</strong> </a>

														<p class="clearfix margin_bottom4"></p>

														<a href="${hibernateUrl}/inheritance-mapping/"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Inheritance
																Mapping</strong> </a>

														<p class="clearfix margin_bottom4"></p>

														<a href="${hibernateUrl}/hibernate-query-language/"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Hibernate
																Query Language</strong> </a>

													</div>
													<!-- -->

													<div class="section-box">

														<a href="${hibernateUrl}/criteria-and-criterion/"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Criteria
																and Criterion</strong> </a>

														<p class="clearfix margin_bottom4"></p>

														<a href="${hibernateUrl}/hibernate-cache/"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Hibernate
																Cache</strong> </a>

														<p class="clearfix margin_bottom4"></p>

														<a href="${hibernateUrl}/connection-pooling/"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Connection
																Pooling</strong> </a>

													</div>
													<!-- -->

												</div>

											</div>
										</div>
									</li>
								</ul></li>

							<li class="dropdown yamm-fw">
								<a href="${springUrl}/" class="dropdown-toggle">Spring</a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">

												<div class="mega-menu-contnew">

													<div class="section-box">

														<a href="${springUrl}/introduction"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Introduction</strong>
														</a>

														<p class="clearfix margin_bottom4"></p>

														<a href="${springUrl}/ioc"><span aria-hidden="true"
															class="icon-layers"></span> <strong>Spring Core
																Container</strong> </a>


													</div>
													<!-- -->

													<div class="section-box">

														<a href="${springUrl}/mvc"><span aria-hidden="true"
															class="icon-layers"></span> <strong>Spring Web
																MVC</strong> </a>
														<p class="clearfix margin_bottom4"></p>

														<a href="${springUrl}/orm"><span aria-hidden="true"
															class="icon-layers"></span> <strong>Spring DAO,
																ORM and Txn</strong> </a>

													</div>
													<!-- -->

													<div class="section-box">

														<a href="${springUrl}/aop"><span aria-hidden="true"
															class="icon-layers"></span> <strong>Spring AOP</strong> </a>
														<p class="clearfix margin_bottom4"></p>

														<a href="${springUrl}/security"><span
															aria-hidden="true" class="icon-layers"></span> <strong>Spring
																Security</strong> </a>

													</div>
													<!-- -->

												</div>

											</div>
										</div>
									</li>
								</ul>
							</li> --%>
							
							<!-- Start Core Java -->
							<li class="dropdown yamm-fw">
								<a href="${coreJavaUrl}/" class="dropdown-toggle">Core Java</a>
							</li> <!-- Ends Core Java -->
							
							<!-- Start Logical Coding -->
							<li class="dropdown yamm-fw">
								<a href="${logicalCodingUrl}/" class="dropdown-toggle">Logical Coding</a>
							</li> <!-- Ends Logical Coding -->
							
							<!-- Start Servlet -->
							<li class="dropdown yamm-fw">
								<a href="${servletUrl}/" class="dropdown-toggle">Servlet</a>
							</li> <!-- Ends Servlet -->

							<!-- Start Interview Questions and Answers -->
							<li class="dropdown yamm-fw">
								<a href="${interviewsUrl}/" class="dropdown-toggle">Interviews</a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">

												<div class="mega-menu-contnew">

													<div class="section-box">

														<a href="${interviewsUrl}/core-java">
															<span aria-hidden="true" class="icon-layers"></span> 
															<strong>Core Java</strong>
														</a>

														<p class="clearfix margin_bottom4"></p>

														<a href="${interviewsUrl}/servlet-and-jsp">
															<span aria-hidden="true" class="icon-layers"></span> 
															<strong>Servlet and JSP</strong> 
														</a>
													</div>
													<!-- -->

													<div class="section-box">

														<a href="${interviewsUrl}/hibernate">
															<span aria-hidden="true" class="icon-layers"></span> 
															<strong>Hibernate</strong> 
														</a>
														<p class="clearfix margin_bottom4"></p>

														<a href="${interviewsUrl}/spring">
															<span aria-hidden="true" class="icon-layers"></span> 
															<strong>Spring</strong> 
														</a>

													</div>
													<!-- -->

													<div class="section-box">

														<a href="${interviewsUrl}/web-services">
															<span aria-hidden="true" class="icon-layers"></span> 
															<strong>Web Services</strong> 
														</a>
														<p class="clearfix margin_bottom4"></p>

														<a href="${interviewsUrl}/manager-round">
															<span aria-hidden="true" class="icon-layers"></span> 
															<strong>Manager Round</strong> 
														</a>

													</div>
													<!-- -->
												</div>

											</div>
										</div>
									</li>
								</ul>
							</li> <!-- Ends Interview Question and Answers -->
						</ul>

					</nav>

				</div>

			</div>
		</div>
		<!-- end Navigation Menu -->

	</div>

</header>