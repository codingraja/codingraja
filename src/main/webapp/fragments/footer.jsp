
<footer>

<div class="footer">

<%-- <div class="ftop">
<div class="container">

    <div class="left">
    	<h4 class="caps light"><strong>Need Help?</strong> Call Us 24/7:</h4>
        <h1>123-456-7890</h1>
    </div><!-- end left -->
    
    <div class="right">
    	<p>Sign up to Newsletter for get special offers</p>
    	<form method="get" action="http://gsrthemes.com/arkahost/demo1/index.html">
        	<input class="newsle_eminput" name="samplees" id="samplees" value="Please enter your email..." onFocus="if(this.value == 'Please enter your email...') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Please enter your email...';}" type="text">
            <input name="" value="Sign Up" class="input_submit" type="submit">
        </form>
    </div><!-- end right -->
    
</div>
</div><!-- end top section -->

<div class="clearfix"></div>

<div class="secarea">
<div class="container">
	
    <div class="one_fourth">
    	<h4 class="white">Hosting Packages</h4>
        <ul class="foolist">
            <li><a href="${resourceUrl}/#">Web Hosting</a></li>
            <li><a href="${resourceUrl}/#">Reseller Hosting</a></li>
            <li><a href="${resourceUrl}/#">VPS Hosting</a></li>
            <li><a href="${resourceUrl}/#">Dedicated Servers</a></li>
            <li><a href="${resourceUrl}/#">Windows Hosting</a></li>
            <li><a href="${resourceUrl}/#">Cloud Hosting</a></li>
            <li><a href="${resourceUrl}/#">Linux Servers</a></li>
        </ul>
	</div><!-- end section -->
    
    <div class="one_fourth">
    	<h4 class="white">Our Products</h4>
        <ul class="foolist">
            <li><a href="${resourceUrl}/#">Website Builder</a></li>
            <li><a href="${resourceUrl}/#">Web Design</a></li>
            <li><a href="${resourceUrl}/#">Logo Design</a></li>
            <li><a href="${resourceUrl}/#">Register Domains</a></li>
            <li><a href="${resourceUrl}/#">Traffic Booster</a></li>
            <li><a href="${resourceUrl}/#">Search Advertising</a></li>
            <li><a href="${resourceUrl}/#">Email Marketing</a></li>
        </ul>
	</div><!-- end section -->
    
    <div class="one_fourth">
    	<h4 class="white">Company</h4>
        <ul class="foolist">
            <li><a href="${resourceUrl}/#">About Us</a></li>
            <li><a href="${resourceUrl}/#">Press &amp; Media</a></li>
            <li><a href="${resourceUrl}/#">News / Blogs</a></li>
            <li><a href="${resourceUrl}/#">Careers</a></li>
            <li><a href="${resourceUrl}/#">Awards &amp; Reviews</a></li>
            <li><a href="${resourceUrl}/#">Testimonials</a></li>
            <li><a href="${resourceUrl}/#">Affiliate Program</a></li>
        </ul>
	</div><!-- end section -->
    
    <div class="one_fourth last aliright">
		<div class="address">
        	<img src="${resourceUrl}/images/footer-logo.png" alt="" />
            <br /><br />
            2901 Marmora Road, Glassgow,
            Seattle, WA 98122-1090
            <div class="clearfix margin_bottom1"></div>
            <strong>Phone:</strong> <b>1 -234 -456 -7890</b>
            <br />
            <strong>Mail:</strong> <a href="${resourceUrl}/mailto:info@arkahost.com">info@arkahost.com</a>
            <br />
            <a href="${resourceUrl}/contact.html" class="smbut">View Directions</a>
            <br /><br />
            <img src="${resourceUrl}/images/payment-logos.png" alt="" />
        	
        </div>
        
	</div><!-- end section -->
    
    
    <div class="clearfix margin_bottom3"></div>
    
    
    <div class="one_fourth">
    	<h4 class="white">Follow Us</h4>
        <ul class="foosocial">
			<li class="faceboox"><a href="${resourceUrl}/#"><i class="fa fa-facebook"></i></a></li>
            <li class="twitter"><a href="${resourceUrl}/#"><i class="fa fa-twitter"></i></a></li>
            <li class="gplus"><a href="${resourceUrl}/#"><i class="fa fa-google-plus"></i></a></li>
            <li class="youtube"><a href="${resourceUrl}/#"><i class="fa fa-youtube-play"></i></a></li>
            <li class="linkdin"><a href="${resourceUrl}/#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
	</div><!-- end section -->
    
    <div class="one_fourth">
    	<h4 class="white">Resources</h4>
        <ul class="foolist">
            <li><a href="${resourceUrl}/#">How to Create a Website</a></li>
            <li><a href="${resourceUrl}/#">How to Transfer a Website</a></li>
            <li><a href="${resourceUrl}/#">Start a Web Hosting Business</a></li>
            <li><a href="${resourceUrl}/#">How to Start a Blog</a></li>
        </ul>
	</div><!-- end section -->
    
    <div class="one_fourth">
    	<h4 class="white">Support</h4>
        <ul class="foolist">
            <li><a href="${resourceUrl}/#">Product Support</a></li>
            <li><a href="${resourceUrl}/#">Contact Us</a></li>
            <li><a href="${resourceUrl}/#">Knowledge Base</a></li>
            <li><a href="${resourceUrl}/#">Tutorials</a></li>
        </ul>
	</div><!-- end section -->
    
    <div class="one_fourth last aliright">
    	<p class="clearfix margin_bottom1"></p>
		<img src="${resourceUrl}/images/app-logo1.png" alt="" /> &nbsp; <img src="${resourceUrl}/images/app-logo1.png" alt="" />
	</div><!-- end section -->
    
</div>
</div>
    

<div class="clearfix"></div> --%>


<div class="copyrights">
<div class="container">

	<div class="one_half"><strong>Copyright � 2017 CodingRAJA. All rights reserved.</strong></div>
    <div class="one_half last aliright">
    	<strong>
	    	<a href="${resourceUrl}/#">Terms of Service</a>|
	    	<a href="${resourceUrl}/#">Privacy Policy</a>|
	    	<a href="${resourceUrl}/#">Site Map</a>
    	</strong>
    </div>

</div>
</div><!-- end copyrights -->


</div>

</footer><!-- end footer -->
