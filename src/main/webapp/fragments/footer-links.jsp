<!-- get jQuery used for the theme -->
<script type="text/javascript" src="${resourceUrl}/js/universal/jquery.js"></script>
<script src="${resourceUrl}/js/style-switcher/styleselector.js"></script>
<script src="${resourceUrl}/js/animations/js/animations.min.js" type="text/javascript"></script>
<script src="${resourceUrl}/js/mainmenu/bootstrap.min.js"></script> 
<script src="${resourceUrl}/js/mainmenu/customeUI.js"></script>
<script src="${resourceUrl}/js/masterslider/jquery.easing.min.js"></script>

<script src="${resourceUrl}/js/scrolltotop/totop.js" type="text/javascript"></script>
<script type="text/javascript" src="${resourceUrl}/js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="${resourceUrl}/js/mainmenu/modernizr.custom.75180.js"></script>
<script type="text/javascript" src="${resourceUrl}/js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="${resourceUrl}/js/cubeportfolio/main.js"></script>

<script src="${resourceUrl}/js/aninum/jquery.animateNumber.min.js"></script>
<script src="${resourceUrl}/js/carouselowl/owl.carousel.js"></script>

<script type="text/javascript" src="${resourceUrl}/js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="${resourceUrl}/js/accordion/custom.js"></script>

<script type="text/javascript" src="${resourceUrl}/js/universal/custom.js"></script>
