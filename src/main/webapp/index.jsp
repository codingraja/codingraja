﻿<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:url value="/resources" var="resourceUrl" />

<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-gb" class="no-js">
<!--<![endif]-->
<head>
<title>CodingRAJA - A Programming Solution</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- timeline -->

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp"%>

</head>

<body>

	<div class="site_wrapper">

		<!-- #####################  Header Course Menu ######################## -->
		<%@ include file="/fragments/header.jsp"%>


		<div class="clearfix"></div>


		<div class="page_title1 sty11">
			<div class="container">

				<h1>A Programming Solution</h1>
				<div class="pagenation">
					<!-- #####################  Header Course Menu ######################## -->
					<%@ include file="/fragments/ads/googleHeaderAds.jsp"%>
				</div>

			</div>
		</div>
		<!-- end page title -->

		<div class="content_fullwidth less">
			<div class="feature_section16">

				<div id="cd-timeline" class="cd-container">

					<%-- <div class="cd-timeline-block">
						<div class="cd-timeline-img cd-picture">
							<img src="${resourceUrl}/images/icons/hibernate-icon.png"
								alt="Hibernate Tutorials" />
						</div>
						<!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>Hibernate Tutorials</h2>
							<p class="text">Hibernate is an open-source
								ORM(Object/Relational Mapping) solution for Java applications
								that is developed by <strong>Gavin King</strong> in 2001.</p>
							<a href="<c:url value="/articles/hibernate/" />" class="cd-read-more">Read more</a> 
							<!-- <span class="cd-date"><strong>August 18</strong></span> -->
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block --> --%>

					<%-- <div class="cd-timeline-block">
						<div class="cd-timeline-img cd-movie">
							<img src="${resourceUrl}/images/post-simg2.jpg" alt="" />
						</div>
						<!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>Title of section 2</h2>
							<p class="text">Lorem Ipsum as their default model text, and
								a search for 'lorem ipsum' will uncover many web sites still in
								their infancy. Various versions have evolved over the years.</p>
							<a href="#" class="cd-read-more">Read more</a> <span
								class="cd-date"><b>August 17</b></span>
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->

					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-picture">
							<img src="${resourceUrl}/images/post-simg3.jpg" alt="" />
						</div>
						<!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>Title of section 3</h2>
							<p class="text">Majority have suffered alteration in some
								form, by injected humour, or randomised words which don't look
								even slightly believable. If you are going to use a passage of
								Lorem Ipsum, you need to be sure there isn't anything
								embarrassing hidden in the middle of text. All Lorem Ipsum
								generators making this the first true generator on the Internet.</p>
							<a href="#" class="cd-read-more">Read more</a> <span
								class="cd-date"><strong>August 16</strong></span>
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->

					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-location">
							<img src="${resourceUrl}/images/post-simg4.jpg" alt="" />
						</div>
						<!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>Title of section 4</h2>
							<p class="text">Generate Lorem Ipsum which looks reasonable.
								The generated Lorem Ipsum is therefore always.</p>
							<a href="#" class="cd-read-more">Read more</a> <span
								class="cd-date"><b>August 15</b></span>
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->

					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-picture">
							<img src="${resourceUrl}/images/post-simg5.jpg" alt="" />
						</div>
						<!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>Title of section 5</h2>
							<p class="text">Majority have suffered alteration in some
								form, by injected humour, or randomised words which don't look
								even slightly believable. If you are going to use a passage of
								Lorem Ipsum, you need to be sure there isn't anything
								embarrassing hidden in the middle of text. All Lorem Ipsum
								generators making this the first true generator on the Internet.</p>
							<a href="#" class="cd-read-more">Read more</a> <span
								class="cd-date"><strong>August 14</strong></span>
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->

					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-location">
							<img src="${resourceUrl}/images/post-simg6.jpg" alt="" />
						</div>
						<!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>Title of section 6</h2>
							<p class="text">Generate Lorem Ipsum which looks reasonable.
								The generated Lorem Ipsum is therefore always.</p>
							<a href="#" class="cd-read-more">Read more</a> <span
								class="cd-date"><b>August 13</b></span>
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->

					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-picture">
							<img src="${resourceUrl}/images/post-simg3.jpg" alt="" />
						</div>
						<!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>Title of section 7</h2>
							<p class="text">Majority have suffered alteration in some
								form, by injected humour, or randomised words which don't look
								even slightly believable. If you are going to use a passage of
								Lorem Ipsum, you need to be sure there isn't anything
								embarrassing hidden in the middle of text. All Lorem Ipsum
								generators making this the first true generator on the Internet.</p>
							<a href="#" class="cd-read-more">Read more</a> <span
								class="cd-date"><strong>August 12</strong></span>
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->

					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-location">
							<img src="${resourceUrl}/images/post-simg4.jpg" alt="" />
						</div>
						<!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>Title of section 8</h2>
							<p class="text">Generate Lorem Ipsum which looks reasonable.
								The generated Lorem Ipsum is therefore always.</p>
							<a href="#" class="cd-read-more">Read more</a> <span
								class="cd-date"><b>August 11</b></span>
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->
					--%>
				</div> 

			</div>
		</div>


		<div class="clearfix"></div>

		<!-- ##################   Footer   ################# -->
		<%@ include file="/fragments/footer.jsp"%>

		<div class="clearfix"></div>


		<a href="${resourceUrl}/#" class="scrollup">Scroll</a>
		<!-- end scroll to top of the page-->

	</div>


	<!-- ################## Footer Links ################# -->
	<%@ include file="/fragments/footer-links.jsp"%>

	<script src="${resourceUrl}/js/timeline/modernizr.js"></script>
	<script>
		(function($) {
			"use strict";

			jQuery(document)
					.ready(
							function($) {
								var $timeline_block = $('.cd-timeline-block');

								//hide timeline blocks which are outside the viewport
								$timeline_block
										.each(function() {
											if ($(this).offset().top > $(window)
													.scrollTop()
													+ $(window).height() * 0.75) {
												$(this)
														.find(
																'.cd-timeline-img, .cd-timeline-content')
														.addClass('is-hidden');
											}
										});

								//on scolling, show/animate timeline blocks when enter the viewport
								$(window)
										.on(
												'scroll',
												function() {
													$timeline_block
															.each(function() {
																if ($(this)
																		.offset().top <= $(
																		window)
																		.scrollTop()
																		+ $(
																				window)
																				.height()
																		* 0.75
																		&& $(
																				this)
																				.find(
																						'.cd-timeline-img')
																				.hasClass(
																						'is-hidden')) {
																	$(this)
																			.find(
																					'.cd-timeline-img, .cd-timeline-content')
																			.removeClass(
																					'is-hidden')
																			.addClass(
																					'bounce-in');
																}
															});
												});
							});

		})(jQuery);
	</script>
</body>

</html>
