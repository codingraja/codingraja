<!doctype html> <!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Hibernate Core API's</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/hibernate/hibernate-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Hibernate Core API's</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
		        <p class="blog_content">
		        	
		        </p>
		        
		        <ol>
		            <li>org.hibernate.cfg.Configuration</li>
		            <li>org.hibernate.SessionFactory</li>
		            <li>org.hibernate.Session</li>
		            <li>org.hibernate.Transaction</li>
		            <li>org.hibernate.Query</li>
		            <li>org.hibernate.Criteria</li>
		         </ol>
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Configuration</h1>		        
		        <div class="blog_content">
					
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>SessionFactory</h1>		        
		        <div class="blog_content">
					
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>SessionFactory</h1>		        
		        <div class="blog_content">
					
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Transaction</h1>		        
		        <div class="blog_content">
					
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Query</h1>		        
		        <div class="blog_content">
					
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Criteria</h1>		        
		        <div class="blog_content">
					
				</div>
		    </div>
		</div><!-- /# end post -->
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
