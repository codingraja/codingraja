<!doctype html> <!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Hibernate Mapping File</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/hibernate/hibernate-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Hibernate Mapping</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
		        <p class="blog_content">
		        	Hibernate Mapping file tells to hibernate that how to load and store 
		        	objects of the persistent class. The mapping file tells Hibernate what 
		        	table in the database it has to access, and what columns in that table 
		        	it should use. We save this file with <span>bean_class_name.hbm.xml</span> .
		        </p>
		        <p class="blog_content">
		        </p>
		    </div>
		</div><!-- /# end post -->
		
		<div class="program_code">
			<h2 class="file_name">Customer.hbm.xml</h2>
			<pre class="brush: xml;">
			&lt;?xml version="1.0"?&gt;
			&lt;!DOCTYPE hibernate-mapping PUBLIC
			        "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
			        "http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd"&gt;
			 
			&lt;hibernate-mapping package="com.codingraja.hibernate.bean"&gt;
			        &lt;class name="Customer" table="CUSTOMER"&gt;
					&lt;id name="custId"&gt;
						&lt;generator class="identity" /&gt;
					&lt;/id&gt;
					&lt;property name="custName" /&gt;
					&lt;property name="mobile" /&gt;
				&lt;/class&gt;
			&lt;/hibernate-mapping&gt;</pre>
		</div> <!-- # End Code -->
		
		<div class="blog_note">
			<h3>REMEMBER</h3>
		  	<p>
		  		Hibernate DTD is not optional you must have to define. It provides 
		  		auto-completion of XML mapping elements and attributes in your editor or IDE.
		  	</p>
			<br>
		  	<p>	
				Hibernate will not load the DTD file from the web, but first 
				look it up from the <span>classpath</span> of the application. The DTD file 
				is included in <span>hibernate-core.jar</span>
		  	</p>
		</div> <!-- End Blog Note -->
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
