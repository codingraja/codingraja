﻿<!doctype html> <!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Hibernate Cache</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/hibernate/hibernate-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Hibernate Cache</h1>
	            <ul class="post_meta_links">
	            	<li><a href="#" class="date">27 August 2015</a></li>
	                <li class="post_by"><i>by:</i> <a href="#">Adam Harrison</a></li>
	                <li class="post_categoty"><i>in:</i> <a href="#">Web tutorials</a></li>
	                <li class="post_comments"><i>note:</i> <a href="#">18 Comments</a></li>
	            </ul>
	            
		        <div class="clearfix"></div>
		        <div class="margin_top1"></div>
		        
		        <div class="blog_content">
					ARKAHOST is a Responsive HTML5 / CSS3 Desktop, Tablet, Mobile phone… Simple, 
					Clean &amp; <i>Professional Template</i>. It comes with Unique Pages, Awesome 
					Slideshows, Unique Color Variations. Easy-to-customize and fully featured design.
					 This theme suitable for Company, Business, Blog and Portfolio. Create 
					 Outstanding Website in Minutes!
				</div>
				
				<div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<div class="image_frame">
		        	<img src="${resourceUrl}/images/blog/blog-img-07.jpg" alt="" />
		        </div>
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Features and Advantages of Hibernate</h1>
		        
		        <ul>
		            <li>
		                <i class="fa fa-check fati4 two"></i>
			            Lorem Ipsum is simply dummy text of theprinting and typesetting it
			            has the randomised words.
		            </li>
		            <li>
		                <i class="fa fa-check fati4 two"></i>
			            Lorem Ipsum is simply dummy text of theprinting and typesetting it
			            has the randomised words.
		            </li>
		            <li>
		                <i class="fa fa-check fati4 two"></i>
			            Lorem Ipsum is simply dummy text of theprinting and typesetting it
			            has the randomised words.
		            </li>
		        </ul>
		    </div>
		</div><!-- /# end post -->
		
		
		<div class="program_code">
			<h2 class="file_name">NumberPattern1.java</h2>
			<pre class="brush: java;">
			package com.codingraja.pattern;

			import java.util.Scanner;

			public class NumberPattern1 
			{
				public static void main(String[] args) 
				{
					System.out.print("Enter the No. of Rows :");
					int number = new Scanner(System.in).nextInt();
					
					for(int i=1; i&lt;=number; i++)   //Outer Loop for number of rows
					{
						for(int j=1; j&lt;=i; j++)
						{							//Inner Loop for number of culumn
							System.out.print(i);
						}
						System.out.println();       // for new line
					}		
				}
			}
			</pre>
		</div> <!-- # End Code -->
		
		<div class="blog_note">
			<h3>Change a few things up and try submitting again.</h3>
		  	<p>
		  		Change a few things up and try submitting again.
		  		Change a few things up and try submitting again.
		  		Change a few things up and try submitting again.
		  	</p>
		  	<ul>
		  		<li><i class="fa fa-caret-right"></i>
		  			Change a few things up and try submitting again.
		  		</li>
		  		<li><i class="fa fa-caret-right"></i>
		  			Change a few things up and try submitting again.
		  		</li>
		  		<li><i class="fa fa-caret-right"></i>
		  			Change a few things up and try submitting again.
		  		</li>
		  	</ul>
		</div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
