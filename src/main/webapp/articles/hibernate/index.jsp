﻿<!doctype html> <!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Hibernate Framework Tutorials</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/hibernate/hibernate-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
	
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Introduction to Hibernate Framework</h1>
		        
	            <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <ol class="topic-list">
		            <li>
			            Hibernate is an open-source <span>ORM(Object/Relational Mapping)</span> solution for
			             Java applications that is developed by <span>Gavin King</span> in <span>2001</span>. 
		            </li>
		            <li>
			            Object/Relational Mapping is a technique of mapping data from an object
			             model representation to a relational data model representation. 
		            </li>
		            <li>
			            Hibernate provides data query and retrieval facilities that significantly 
			            reduce complexity and   development time.
		            </li>
		            <li>
			            Hibernate provides the mapping for association, inheritance, 
			            polymorphism, composition, and collections directly with 
			            Relational Database.
		            </li>
		        </ol>
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_note">
			<h3>Note:</h3>
		  	<p>
		  		Working with Object-Oriented Software and Relational Databases can be 
			    very complex and time consuming. Hibernate makes it very simple, 
			    maintainable and reduce development time.
		  	</p>
		</div> <!-- End Blog Note -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Architecture of Hibernate </h1>
				<div class="image_frame">
		        	<img src="${imageUrl}/hibernate/hibernate-architecture.jpg" alt="Hibernate Architecture" />
		        </div>
		    </div>
		</div><!-- /# end post -->
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
