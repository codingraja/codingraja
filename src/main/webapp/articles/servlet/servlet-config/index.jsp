<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>ServletConfig Interface in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/servlet/servlet-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>ServletConfig Interface</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <p>
					A <em>ServletConfig</em> object is used by a servlet container  
					to pass information to a servlet during initialization.
				</p>
				<p>
					Before Servlet <em>initialization</em>, web container <em>creates</em> 
					a ServletConfig object for every servlet, then pass it to the <em>init()</em> 
					method to initialize the servlet.
				</p>
				<p>
					Programmer can pass own parameters and values through <em>web.xml</em> file. 
					In Annotation Based Servlet Mapping through <em>@WebInitParam</em>.
				</p>
				
				<h4>Methods of ServletConfig</h4>
				
				<div class="table-responsive">
				<table class="table bordered"> 
				<tr>
				<th><em>SNo.</em></th>
				<th><em>Methods</em></th>
				<th><em>Description</em></th>
				</tr>
				
				<tr>
				<td><em>1</em></td>
				<td><em>public String getServletName();</em></td>
				<td>return the name of the servlet instance.</td>
				</tr>
				
				<tr>
				<td><em>2</em></td>
				<td><em>public ServletContext getServletContext();</em></td>
				<td>Returns a reference to the ServletContext in which the caller is executing.</td>
				</tr>
				
				<tr>
				<td><em>3</em></td>
				<td><em>public String getInitParameter(String name);</em></td>
				<td>Returns a String containing the value of the  named initialization parameter, or null if  the parameter does not exist.</td>
				</tr>
				
				<tr>
				<td><em>4</em></td>
				<td><em>public Enumeration getInitParameterNames();</em></td>
				<td>return an Enumeration of String objects containing the names of the servlet's initialization parameters.</td>
				</tr>
				</table>
				</div>
			
		    </div>
		</div><!-- /# end post -->
		
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../servlet-app-list.jsp" %>
	        </div>
	    </div>
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
