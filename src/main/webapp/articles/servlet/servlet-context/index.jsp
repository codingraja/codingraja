<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>ServletContext Interface in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/servlet/servlet-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>ServletContext Object</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <ol class="topic-list">
					<li>ServletContext is an object that is created by servlet container when we deploy our application.</li>
					<li>Servlet container creates one ServletContext object per web application.</li>
					<li>We can share any parameter or information between all the servlet by using ServletContext Object.</li>
					<li>ServletContext interface defines a set of methods that a servlet uses to communicate with it's servlet container.</li>
					<li>Programmer can set, get and remove attributes from the ServletContext Object. </li>
				</ol>
					
				<h4>Frequently used Methods of ServletContext</h4>
				
				<div class="table-responsive">
					<table class="table table-strippted">
						<tr>
							<th>Methods</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><em>String getInitParameter(String name)</em></td>
							<td>Returns a  initialization parameter value, or null if the parameter 
							does not exist.</td>
						</tr>
						<tr>
							<td><em>void setAttribute(String name, Object obj)</em></td>
							<td>Sets an object to a given attribute name in this servlet context.</td>
						</tr>
						<tr>
							<td><em>Object getAttribute(String name)</em></td>
							<td>Returns the servlet container attribute with the given name, or null if there is no 
							attribute by that name.</td>
						</tr>
						<tr>
							<td><em>void removeAttribute(String name)</em></td>
							<td>Removes the attribute with the given name from the servlet context.</td>
						</tr>
						<tr>
							<td><em>String getServletContextName()</em></td>
							<td>Returns the name of this web application, specified in the web.xml by the 
							<b>display-name</b> element.</td>
						</tr>
						<tr>
							<td><em>String getMimeType()</em></td>
							<td>Returns the MIME type of the specified file, or null if the MIME type is 
							not known.</td>
						</tr>
					</table>
				</div>
				
				<h1>Passing Custom Parameters and values</h1>
				<p>
					We can pass own parameters and values  in ServletContext object. To 
					initialize these parameters, we need to define these parameters and values  
					in <strong>web.xml</strong> file.
				</p>
				
				<p>
					<em>web.xml</em> file defines some elements to pass parameters and values.
					Programmer can pass any number of parameters, that will be <strong>shared</strong> 
					between all the servlets.
				</p>
				
				<pre class="brush: xml;">
				&lt;context-param&gt;
					&lt;param-name&gt;username&lt;/param-name&gt;
					&lt;param-value&gt;root&lt;/param-value&gt;
				&lt;/context-param&gt;
				
				&lt;context-param&gt;
					&lt;param-name&gt;password&lt;/param-name&gt;
					&lt;param-value&gt;admin&lt;/param-value&gt;
				&lt;/context-param&gt;
				</pre>
				
				<p>
					Here we have  defined two parameters - <strong>username</strong> and 
					<strong>password</strong>, when we get username, it 
					will return root as a value and password will return admin as a value.
				</p>
				
				<h1>Get ServletContext Object</h1>
				<p>
					We can get ServletContext object by calling <em>getServletContext()</em> method 
					of ServletConfig Object.
				</p>
				
				<pre class="brush: java;">
				public ServletContext  getServletContext();
				</pre>
			
		    </div>
		</div><!-- /# end post -->
		
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../servlet-app-list.jsp" %>
	        </div>
	    </div>
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
