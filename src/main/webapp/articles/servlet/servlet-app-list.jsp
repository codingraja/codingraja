<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:url value="/articles/servlet/applications" var="servletAppUrl" />

<div class="interview">
	<h1 class="interview-question-cat">
		Useful Servlet Applications
	</h1>
	<ol>
		<li><a href="${servletAppUrl}/servlet-single-file-uploading">Servlet Single File Uploading</a></li>
		<li><a href="${servletAppUrl}/servlet-multiple-file-uploading">Servlet Multiple File Uploading</a></li>
		<li><a href="${servletAppUrl}/servlet-reading-and-writing-image-into-database">Reading and Writing Image File into Database</a></li>
		<li><a href="${servletAppUrl}/servlet-image-uploading-using-ajax">Servlet Image Uploading Using AJAX</a></li>
		<li><a href="${servletAppUrl}/servlet-jsp-jdbc-crud-operations">Servlet + JSP + JDBC CRUD Operations</a></li>
		<li><a href="${servletAppUrl}/servlet-jsp-hibernate-crud-operations">Servlet + JSP + Hibernate CRUD Operations</a></li>
	</ol>
</div>