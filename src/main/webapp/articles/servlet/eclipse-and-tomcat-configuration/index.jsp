<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Eclipse and Tomcat Configuration</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/servlet/servlet-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Eclipse and Tomcat Configuration</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>

				<h1>Install Eclipse</h1>
				<p>To use Eclipse for Java programming, you need to first install 
				Java Development Kit (JDK).
				<a href="#">Read</a> "How to Install JDK (on Windows)".</p>
				 
				<p><b>Download</b><br>
				Download Eclipse from <a href="http://www.eclipse.org/downloads">Here</a>. 
				For beginners, choose "Eclipse IDE for Java Developers" (32-bit or 64-bit).</p>
				
				<p><b>Unzip</b><br>
				To install Eclipse, simply unzip the downloaded file into a directory of your choice 
				<b>(e.g., "d:\myproject")</b>. There is no need to run any installer. Moreover, you 
				can simply delete the entire Eclipse directory when it is no longer needed
				 (without running any un-installer).
				<p> You are free to move or rename the directory. You can install (unzip) multiple copies 
				of Eclipse in the same machine.</p>
				
				<h4>Step: 1</h4>
				<p>Now go inside Eclipse Directory and double click on <b>eclipse.exe</b></p>
				<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration1.png" alt="Install Eclipse" />
				
				<h4>Step: 2</h4>
				<p>In between eclipse loading, Eclipse show you the workspace Location. You can leave the 
				default location or you can change to anywhere in your PC.</p>
				<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration2.png" alt="Install Eclipse" />
				
				<h4>Step: 3</h4>
				<p>Now Eclipse is loaded, close the <b>welcome file</b> .</p>
				<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration3.png" alt="Install Eclipse" />
				
				<h4>Step: 4</h4>
				<p>Now you can start your java project here.</p>
				<div class="image_frame">
					<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration4.png" alt="Install Eclipse" />
				</div>
				
				<h1>Configure Tomcat Server</h1>
				
				<h4>Step: 5</h4>
				<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration5.png" alt="Configure Tomcat With Eclipse" />
				
				<h4>Step: 6</h4>
				<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration6.png" alt="Configure Tomcat With Eclipse" />
				
				<h4>Step: 7</h4>
				<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration7.png" alt="Configure Tomcat With Eclipse" />
				
				<h4>Step: 8</h4>
				<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration8.png" alt="Configure Tomcat With Eclipse" />
				
				<h4>Step: 9</h4>
				<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration9.png" alt="Configure Tomcat With Eclipse" />
				
				<h1>Start Tomcat Server</h1>
				
				<h4>Step: 10</h4>
				<div class="image_frame">
					<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration10.png" alt="Configure Tomcat With Eclipse" />
				</div>
				<h4>Step: 11</h4>
				<div class="image_frame">
					<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration11.png" alt="Configure Tomcat With Eclipse" />
				</div>
				<pre class="brush: xml">
				C:\Program Files
					->Apache Software Foundation	
						->Tomcat 8.0
							->webapps
								->ROOT
				</pre>
				
				<pre class="brush: xml">
				workspace
					->.metadata
						->.plugins
							->org.eclipse.wst.server.core
								->tmp0
									->wtpwebapps
										->ROOT
				</pre>
				
				<h4>Step: 12</h4>
				<div class="image_frame">
					<img class="img-thumbnail img-responsive" src="${imageUrl}/servlet/eclipse-and-tomcat-configuration12.png" alt="Configure Tomcat With Eclipse" />
				</div>
		    </div>
		</div><!-- /# end post -->
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
