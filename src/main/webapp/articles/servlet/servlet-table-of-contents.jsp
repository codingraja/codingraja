<div class="sidebar_title"><h4 class="caps">Table of Contents</h4></div>
<div class="clearfix"></div>
<div id="st-accordion-four" class="st-accordion-four">
	<ul>
		<li>
			<a href="${servletUrl}/#">Servlet Introduction<span class="st-arrow">Open or Close</span></a>
			<div class="st-content">
				<ol class="arrows_list1">		
					<li><a href="${servletUrl}/what-is-servlet"><i class="fa fa-angle-right"></i> What is Servlet</a></li>
					<li><a href="${servletUrl}/eclipse-and-tomcat-configuration"><i class="fa fa-angle-right"></i> Eclipse and Tomcat Config</a></li>
					<li><a href="${servletUrl}/generic-servlet"><i class="fa fa-angle-right"></i> GenericServlet</a></li>
					<li><a href="${servletUrl}/http-servlet"><i class="fa fa-angle-right"></i> HttpServlet</a></li>
					<li><a href="${servletUrl}/servlet-config"><i class="fa fa-angle-right"></i> ServletConfig</a></li>
					<li><a href="${servletUrl}/servlet-context"><i class="fa fa-angle-right"></i> ServletContext</a></li>
				</ol>
			</div>
		</li>
		
	</ul>
</div>



<!-- #####################   Recent Posts   #################### -->
<div class="recent_posts">
	<%@ include file="/fragments/recent-posts.jsp" %>
</div> <!-- #####################  End Recent Posts   #################### -->