<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>What is Servlet</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/servlet/servlet-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>What is Servlet</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="blog_content">
					We can explain the <em>Servlet</em> by many ways-
				</div>
				
				<div class="blog_content">
					A Servlet is a <em>Java Technology</em> that is used to develop web-applications in Java.
				</div>
				<div class="blog_content">
					Servlet is an <em>API's</em> that is used to develop web-applications in Java. These API's 
					enables the Server capabilities in Java Applications. Capabilities are receiving the 
					requests and sending the responses etc.
				</div>
				<div class="blog_content">
					In Servlet API's, Servlet is an <em>Interface</em> that defines the life-cycle of servlet. 
					A Java class that implements the Servlet interfcae is clalled <em>Servlet Class</em>, 
					that can receive and respond to requests from Web clients, usually across <b>HTTP</b>, 
					the HyperText Transfer Protocol.
				</div>
				<div class="blog_content">
					A Servlet is a <em>Small Java Program</em> that executes on Server.
				</div>
			
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Types of Servlet</h1>
		        
		        <div class="blog_content">
					There are two types of <em>Servlet</em>-
				</div>
				<ol>
					<li><a href="<c:url value="${servletUrl}/generic-servlet" />">GenericServlet</a></li>
					<li><a href="<c:url value="${servletUrl}/http-servlet" />">HttpServlet</a></li>
				</ol>
			
				<img alt="Servlet Hierarchy" src="">
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Creating Servlet Class</h1>
		        
		        <div class="blog_content">
					There are three ways to create a servlet class-
				</div>
				<ol>
					<li><a href="<c:url value="${servletUrl}/servlet-interface" />">By Implementing Servlet Interface</a></li>
					<li><a href="<c:url value="${servletUrl}/generic-servlet" />">By Extending GenericServlet Class</a></li>
					<li><a href="<c:url value="${servletUrl}/http-servlet" />">By Extending HttpServlet Class</a></li>
				</ol>
		    </div>
		</div><!-- /# end post -->
		
		<div class="blog_note">
			<h3>Note:</h3>
		  	<p>
		  		To understand the working and implementation of Servlet Applications.
		  		 First you should know about these terms:
		  	</p>
		  	<ul>
		  		<li><i class="fa fa-caret-right"></i>
		  			Core Java
		  		</li>
		  		<li><i class="fa fa-caret-right"></i>
		  			JDBC (Java Database Connectivity)
		  		</li>
		  		<li><i class="fa fa-caret-right"></i>
		  			Collections Framework
		  		</li>
		  		<li><i class="fa fa-caret-right"></i>
		  			SQL, XML and HTML
		  		</li>
		  	</ul>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../servlet-app-list.jsp" %>
	        </div>
	    </div>
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
