﻿<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Servlet Tutorials</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/servlet/servlet-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Servlet Tutorials and Useful Applications</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="blog_content">
					In this tutorial, we will cover all the topics of <em>Servlet Technology</em> 
					and all the useful applications with real time scenario.
				</div>
				
		        <h4>Tools, Versions and Design Patterns</h4>
		        <div class="table-responsive">
		        	<table class="table table-strippted">
		        		<tr>
		        			<th>Name</th>
		        			<th>Used</th>
		        			<th>Recommended</th>
		        		</tr>
		        		<tr>
		        			<td>Java</td>
		        			<td>jdk-1.8</td>
		        			<td>jdk1.7 or Higher</td>
		        		</tr>
		        		<tr>
		        			<td>Servlet</td>
		        			<td>3.1.0</td>
		        			<td>3.0 or Higher</td>
		        		</tr>
		        		<tr>
		        			<td>IDE</td>
		        			<td>Eclipse 4.6 (Neon)</td>
		        			<td>Eclipse 4.6 (Neon) or Higher</td>
		        		</tr>
		        		<tr>
		        			<td>Server</td>
		        			<td>Apache Tomcat-8.0</td>
		        			<td>Apache Tomcat-8.0 or Higher</td>
		        		</tr>
		        		<tr>
		        			<td>Project Management Tool</td>
		        			<td>Maven-4.0</td>
		        			<td>Maven-3.0 or Higher</td>
		        		</tr>
		        		<tr>
		        			<td>Design Patterns</td>
		        			<td>Singleton, MVC, DAO</td>
		        			<td>Any if required</td>
		        		</tr>
		        		<tr>
		        			<td>Database</td>
		        			<td>MySQL</td>
		        			<td>MySQL, Oracle-11g-12c, PostgreSQL</td>
		        		</tr>
		        	</table>
		        </div>
		    </div>
		</div>
		        
		<div class="blog_note">
			<h3>Note:</h3>
		  	<p>
		  		All the basics and advanced applications in this tutorial are created 
		  		as a <em>Maven Project</em> and used <em>webapp-javaee7</em> 
		  		<strong>archetype</strong>. If you don't know, how to create <em>Maven</em> 
		  		project than first read this article- 
		  		<a href="#">Creating Maven Project</a>
		  	</p>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        
		        <%@ include file="servlet-app-list.jsp" %>
			
		    </div>
		</div><!-- /# end post -->
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
