<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Reading and Writing Image File into Database</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/servlet/servlet-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Reading and Writing Image File into Database</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="blog_content">
					In this tutorial, we will cover all the topics of <em>Servlet Technology</em> 
					and all the useful applications with real time scenario.
				</div>
				
				<%@ include file="../../servlet-app-list.jsp" %>
		        
		        <h1>Eclipse Project Structure</h1>
		        <img alt="File Upload" src="${imageUrl}/servlet/servlet-reading-and-writing-image-into-database.jpg">
			
				<h1>Create Table in MySQL Database</h1>
		        <pre class="brush: sql;">
		        CREATE TABLE image_master(
					image_id BIGINT UNIQUE NOT NULL AUTO_INCREMENT,
					image_name VARCHAR(50),
					image_data LONGBLOB,
					PRIMARY KEY(image_id)
				);
				
				/*TINYBLOB   :     maximum length of 255 bytes  
				BLOB       :     maximum length of 65,535 bytes  
				MEDIUMBLOB :     maximum length of 16,777,215 bytes  
				LONGBLOB   :     maximum length of 4,294,967,295 bytes*/</pre>
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">pom.xml</h2>
			<pre class="brush: xml;">
			&lt;?xml version="1.0" encoding="UTF-8"?&gt;
			&lt;project xmlns="http://maven.apache.org/POM/4.0.0" 
					 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
					 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
					 			http://maven.apache.org/xsd/maven-4.0.0.xsd"&gt;
			    &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;
			
			    &lt;groupId&gt;com.codingraja.servlet&lt;/groupId&gt;
			    &lt;artifactId&gt;servlet-reading-and-writing-image-into-database&lt;/artifactId&gt;
			    &lt;version&gt;0.0.1-SNAPSHOT&lt;/version&gt;
			    &lt;packaging&gt;war&lt;/packaging&gt;
			
			    &lt;name&gt;servlet-reading-and-writing-image-into-database&lt;/name&gt;
			
			    &lt;dependencies&gt;
			    	&lt;!-- JEE 7.0 Dependency --&gt;
			        &lt;dependency&gt;
			            &lt;groupId&gt;javax&lt;/groupId&gt;
			            &lt;artifactId&gt;javaee-web-api&lt;/artifactId&gt;
			            &lt;version&gt;7.0&lt;/version&gt;
			            &lt;scope&gt;provided&lt;/scope&gt;
			        &lt;/dependency&gt;
			        
			        &lt;!-- MySQL Driver Dependency --&gt;
			        &lt;dependency&gt;
			        	&lt;groupId&gt;mysql&lt;/groupId&gt;
			        	&lt;artifactId&gt;mysql-connector-java&lt;/artifactId&gt;
			        	&lt;version&gt;5.1.6&lt;/version&gt;
			        &lt;/dependency&gt;
			    &lt;/dependencies&gt;
			
			    &lt;build&gt;
			        &lt;plugins&gt;
			            &lt;plugin&gt;
			                &lt;groupId&gt;org.apache.maven.plugins&lt;/groupId&gt;
			                &lt;artifactId&gt;maven-compiler-plugin&lt;/artifactId&gt;
			                &lt;version&gt;3.2&lt;/version&gt;
			                &lt;configuration&gt;
			                    &lt;source&gt;1.8&lt;/source&gt;
			                    &lt;target&gt;1.8&lt;/target&gt;
			                &lt;/configuration&gt;
			            &lt;/plugin&gt;
			            &lt;plugin&gt;
			                &lt;groupId&gt;org.apache.maven.plugins&lt;/groupId&gt;
			                &lt;artifactId&gt;maven-war-plugin&lt;/artifactId&gt;
			                &lt;version&gt;2.4&lt;/version&gt;
			                &lt;configuration&gt;
			                    &lt;failOnMissingWebXml&gt;false&lt;/failOnMissingWebXml&gt;
			                &lt;/configuration&gt;
			            &lt;/plugin&gt;
			        &lt;/plugins&gt;
			    &lt;/build&gt;
			
			&lt;/project&gt;</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Database.java</h2>
			<pre class="brush: java;">
			package com.codingraja.db;

			import java.sql.Connection;
			import java.sql.DriverManager;
			
			public class Database {
				private static final String DRIVER = "com.mysql.jdbc.Driver";
				private static final String URL = "jdbc:mysql://localhost:3306/test";
				private static final String USERNAME = "root";
				private static final String PASSWORD = "root";
				
				private static Connection connection = null;
				
				public static Connection getConnection(){
					if(connection==null){
						try{
							//Loading The Driver Class
							Class.forName(DRIVER);
							
							//Getting the connection Object
							connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
						}catch (Exception ex) {
							System.out.println(ex.getMessage());
						}
					}
					
					return connection;
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">Image.java</h2>
			<pre class="brush: java;">
			package com.codingraja.domain;

			public class Image {
				private String imageName;
				private byte[] imageData;
			
				public Image() {
					// Do Nothing
				}
			
				public String getImageName() {
					return imageName;
				}
			
				public void setImageName(String imageName) {
					this.imageName = imageName;
				}
			
				public byte[] getImageData() {
					return imageData;
				}
			
				public void setImageData(byte[] imageData) {
					this.imageData = imageData;
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">ImageUploadServlet.java</h2>
			<pre class="brush: java;">
			package com.codingraja.servlet;

			import java.io.IOException;
			import java.io.InputStream;
			import java.io.PrintWriter;
			import java.sql.Connection;
			import java.sql.PreparedStatement;
			import java.sql.ResultSet;
			import java.sql.Statement;
			
			import javax.servlet.ServletException;
			import javax.servlet.annotation.MultipartConfig;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			import javax.servlet.http.Part;
			
			import com.codingraja.db.Database;
			
			@MultipartConfig(maxFileSize = 1024 * 1024 * 1)
			@WebServlet("/ImageUploadServlet")
			public class ImageUploadServlet extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				peivate Connection connection = Database.getConnection();
			
				public ImageUploadServlet() {
					// Do Nothing
				}
			
				protected void doGet(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
					request.getRequestDispatcher("index.html").forward(request, response);
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
					response.setContentType("text/html");
					PrintWriter out = response.getWriter();
			
					// Getting File data
					Part part = request.getPart("fileData");
			
					// Getting Image Name
					String imageName = part.getSubmittedFileName();
					
					if(validateImage(imageName)){
						//Getting Image Bytes
						InputStream in = part.getInputStream();
				
						long imageId = writeBytesIntoDatabase(imageName, in);
				
						if(imageId &gt; 0) {
							out.println("&lt;h1 style=\"text-align:center;\"&gt;"
										+"Image has been stored successfully!"
										+ "&lt;/h1&gt;");
							out.println("&lt;h3 style=\"text-align:center;\"&gt;&lt;a "
										+"href=\"DisplayImageServlet?imageId="+imageId
										+"\"&gt;Display Image&lt;/a&gt;&lt;/h3&gt;");
						}else{
							out.print("&lt;h1&gt;Some Database Exception has been occurred!&lt;/h1&gt;");
						}
					}else {
						out.print("&lt;h1&gt;Invalid Image format , Select (.jpg, .png, .gif)!&lt;/h1&gt;");
					}
				}
				
				//Private Method to write Image bytes into database
				private long writeBytesIntoDatabase(String imageName, InputStream in){
					String sql = "INSERT INTO image_master(image_name, image_data) VALUES(?,?)";
					try{
						PreparedStatement pStatement = connection.prepareStatement(sql,
											                    Statement.RETURN_GENERATED_KEYS);
						pStatement.setString(1, imageName);
						pStatement.setBlob(2, in);
						
						pStatement.executeUpdate();
						
						//Get Generated Image_id
						ResultSet rs = pStatement.getGeneratedKeys();
						if(rs.next())
							return rs.getLong(1);
							
					}catch (Exception ex) {
						return 0;
					}
					return 0;
				}
				
				//Validates uploaded file is Image or not
				private boolean validateImage(String imageName){
					String fileExt = imageName.substring(imageName.length()-3);
					if("jpg".equals(fileExt) || "png".equals(fileExt) || "gif".equals(fileExt))
						return true;
					
					return false;
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">DisplayImageServlet.java</h2>
			<pre class="brush: java;">
			package com.codingraja.servlet;

			import java.io.IOException;
			import java.sql.Connection;
			import java.sql.PreparedStatement;
			import java.sql.ResultSet;
			
			import javax.servlet.ServletException;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			
			import com.codingraja.db.Database;
			import com.codingraja.domain.Image;
			
			@WebServlet("/DisplayImageServlet")
			public class DisplayImageServlet extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				//Get Connection Object
				private Connection connection = Database.getConnection();
				
				public DisplayImageServlet() {
					// Do Nothing
				}
			
				protected void doGet(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
					
					long imageId = Long.parseLong(request.getParameter("imageId"));
					
					// Reading Image Bytes by passing Image ID
					Image image = readBytesFromDatabase(imageId);
			
					if (image == null)
						response.getWriter().println("&lt;h1>Image Not Found Exception&lt;/h1>");
					else {
						// Get Image Name From Image Object
						String imageName = image.getImageName();
			
						// Get Content-Type of Image
						String contentType = this.getServletContext().getMimeType(imageName);
			
						// Set Content Type
						response.setHeader("Content-Type", contentType);
			
						// Set Content-Length of Image Data
						response.setHeader("Content-Length", String.valueOf(image.getImageData().length));
			
						// Set Content-Disposition
						response.setHeader("Content-Disposition", 
										   "inline; filename=\"" + image.getImageName() + "\"");
						
						//Write Image Data to Response
						response.getOutputStream().write(image.getImageData());
					}
				}
			
				// Private Method Implementation to read Image name and Bytes from Database
				private Image readBytesFromDatabase(long imageId) {
					String sql = "SELECT * FROM image_master WHERE image_id=?";
					try {
						PreparedStatement pStatement = connection.prepareStatement(sql);
						pStatement.setLong(1, imageId);
			
						ResultSet resultSet = pStatement.executeQuery();
			
						if (resultSet.next()) {
							Image image = new Image();
							image.setImageName(resultSet.getString(2));
							image.setImageData(resultSet.getBytes(3));
							return image;
						}
					} catch (Exception ex) {
						return null;
					}
					return null;
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">index.html</h2>
			<pre class="brush: xml;">
			&lt;!DOCTYPE html&gt;
			&lt;html&gt;
			    &lt;head&gt;
			        &lt;title&gt;Servlet Writing Image File into Database&lt;/title&gt;
			        &lt;meta http-equiv="Content-Type" content="text/html; charset=UTF-8"&gt;
			    &lt;/head&gt;
			    &lt;body style="text-align: center;"&gt;
			        &lt;h1&gt;Servlet Writing Image File into Database&lt;/h1&gt;
			        
			        &lt;form action="ImageUploadServlet" 
			        	  method="post" 
			        	  enctype="multipart/form-data"&gt;
			        	&lt;label&gt;Select File: &lt;/label&gt;
			        	&lt;input type="file" name="fileData" required="required"&gt;
			        	&lt;input type="submit" value="Upload"&gt;
			        &lt;/form&gt;
			    &lt;/body&gt;
			&lt;/html&gt;</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Deploy Application on Server</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="image_frame">
		        	<img alt="File Upload" src="${imageUrl}/servlet/servlet-reading-and-writing-image-into-database1.jpg">
				</div>
				
				<div class="image_frame">
		        	<img alt="File Upload" src="${imageUrl}/servlet/servlet-reading-and-writing-image-into-database2.jpg">
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="download-box">
			<c:url value="/articles/servlet/src" var="servletSrcUrl" />	
			<a href="${servletSrcUrl}/servlet-reading-and-writing-image-into-database.zip" 
			   data-download="${servletSrcUrl}/servlet-reading-and-writing-image-into-database.zip">
				<img alt="Download" src="${imageUrl}/download.jpg">
			</a>
		</div>
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
