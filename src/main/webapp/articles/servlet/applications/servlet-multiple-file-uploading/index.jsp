<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Servlet Multiple File Uploading</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/servlet/servlet-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Servlet Multiple File Uploading</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="blog_content">
					In this tutorial, we will cover all the topics of <em>Servlet Technology</em> 
					and all the useful applications with real time scenario.
				</div>
				
				<%@ include file="../../servlet-app-list.jsp" %>
		        
		        <h1>Eclipse Project Structure</h1>
		        <img alt="File Upload" src="${imageUrl}/servlet/servlet-multiple-file-uploading.jpg">
			
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">pom.xml</h2>
			<pre class="brush: xml;">
			&lt;?xml version="1.0" encoding="UTF-8"?&gt;
			&lt;project xmlns="http://maven.apache.org/POM/4.0.0" 
					 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
					 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
					 			http://maven.apache.org/xsd/maven-4.0.0.xsd"&gt;
			    &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;
			
			    &lt;groupId&gt;com.codingraja.servlet&lt;/groupId&gt;
			    &lt;artifactId&gt;servlet-multiple-file-uploading&lt;/artifactId&gt;
			    &lt;version&gt;0.0.1-SNAPSHOT&lt;/version&gt;
			    &lt;packaging&gt;war&lt;/packaging&gt;
			
			    &lt;name&gt;servlet-multiple-file-uploading&lt;/name&gt;
			    
			    &lt;dependencies&gt;
			        &lt;dependency&gt;
			            &lt;groupId&gt;javax&lt;/groupId&gt;
			            &lt;artifactId&gt;javaee-web-api&lt;/artifactId&gt;
			            &lt;version&gt;7.0&lt;/version&gt;
			            &lt;scope&gt;provided&lt;/scope&gt;
			        &lt;/dependency&gt;
			     &lt;/dependencies&gt;
			
			    &lt;build&gt;
			        &lt;plugins&gt;
			            &lt;plugin&gt;
			                &lt;groupId&gt;org.apache.maven.plugins&lt;/groupId&gt;
			                &lt;artifactId&gt;maven-compiler-plugin&lt;/artifactId&gt;
			                &lt;version&gt;3.2&lt;/version&gt;
			                &lt;configuration&gt;
			                    &lt;source&gt;1.8&lt;/source&gt;
			                    &lt;target&gt;1.8&lt;/target&gt;
			                &lt;/configuration&gt;
			            &lt;/plugin&gt;
			            &lt;plugin&gt;
			                &lt;groupId&gt;org.apache.maven.plugins&lt;/groupId&gt;
			                &lt;artifactId&gt;maven-war-plugin&lt;/artifactId&gt;
			                &lt;version&gt;2.4&lt;/version&gt;
			                &lt;configuration&gt;
			                    &lt;failOnMissingWebXml&gt;false&lt;/failOnMissingWebXml&gt;
			                &lt;/configuration&gt;
			            &lt;/plugin&gt;
			        &lt;/plugins&gt;
			    &lt;/build&gt;
			
			&lt;/project&gt;</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">MultipleFileUploadServlet.java</h2>
			<pre class="brush: java;">
			package com.codingraja.servlet;

			import java.io.File;
			import java.io.IOException;
			import java.io.PrintWriter;
			import java.util.Collection;
			
			import javax.servlet.ServletException;
			import javax.servlet.annotation.MultipartConfig;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			import javax.servlet.http.Part;
			
			@MultipartConfig(maxFileSize = 1024 * 1024 * 2, 
							 maxRequestSize = 1024 * 1024 * 10)
			@WebServlet("/MultipleFileUploadServlet")
			public class MultipleFileUploadServlet extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				public MultipleFileUploadServlet() {
					// Do Nothing
				}
			
				protected void doGet(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
					request.getRequestDispatcher("index.html").forward(request, response);
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
					response.setContentType("text/html");
					PrintWriter out = response.getWriter();
			
					// Getting Files data
					Collection&lt;Part> parts = request.getParts();
			
					// Getting Application Path
					String appPath = request.getServletContext().getRealPath("");
			
					// File path where all files will be stored
					String filePath = appPath + "docs";
			
					// Creates the file directory if it does not exists
					File fileDir = new File(filePath);
					if (!fileDir.exists()) {
						fileDir.mkdirs();
					}
			
					String message = "All Files have been uploaded successfully!";
					
					//Checks Files are selected or not
					if(parts.isEmpty())
						message = "You did not select any file";
			
					// Writing Files Data
					for (Part part : parts) {
						try {
							part.write(filePath + File.separator + part.getSubmittedFileName());
						} catch (Exception ex) {
							message = "Exception: " + ex.getMessage();
						}
					}
			
					out.println("&lt;h1 style=\"text-align:center;\">" + message + "&lt;/h1>");
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">index.html</h2>
			<pre class="brush: xml;">
			&lt;!DOCTYPE html&gt;
			&lt;html&gt;
			    &lt;head&gt;
			        &lt;title&gt;Servlet Multiple File Uploading&lt;/title&gt;
			        &lt;meta http-equiv="Content-Type" content="text/html; charset=UTF-8"&gt;
			    &lt;/head&gt;
			    &lt;body style="text-align: center;"&gt;
			        &lt;h1&gt;Servlet Multiple File Uploading&lt;/h1&gt;
			        
			        &lt;form action="MultipleFileUploadServlet" 
			        		 method="post" 
			        		 enctype="multipart/form-data"&gt;
			        	&lt;label&gt;Select Files: &lt;/label&gt;
			        	&lt;input type="file" multiple="multiple" 
        		   				  name="file"  required="required"&gt;
			        	&lt;input type="submit" value="Upload"&gt;
			        &lt;/form&gt;
			    &lt;/body&gt;
			&lt;/html&gt;</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Deploy Application on Server</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="image_frame">
		        	<img alt="File Upload" src="${imageUrl}/servlet/servlet-multiple-file-uploading1.jpg">
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="download-box">
			<c:url value="/articles/servlet/src" var="servletSrcUrl" />	
			<a href="${servletSrcUrl}/servlet-multiple-file-uploading.zip" 
			   data-download="${servletSrcUrl}/servlet-multiple-file-uploading.zip">
				<img alt="Download" src="${imageUrl}/download.jpg">
			</a>
		</div>
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
