<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Servlet, JSP and JDBC CRUD Operations</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/servlet/servlet-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Servlet, JSP and JDBC CRUD Operations</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="blog_content">
					In this tutorial, we will cover all the topics of <em>Servlet Technology</em> 
					and all the useful applications with real time scenario.
				</div>
				
				<%@ include file="../../servlet-app-list.jsp" %>
		        
		        <h1>Eclipse Project Structure</h1>
		        <img alt="CRUD Operations" src="${imageUrl}/servlet/servlet-jsp-jdbc-crud-operations.jpg">
		        
		        <h1>Create Table in MySQL Database</h1>
		        <pre class="brush: sql;">
		        CREATE TABLE customer_master(
					customer_id BIGINT UNIQUE NOT NULL AUTO_INCREMENT,
					first_name VARCHAR(50),
					last_name VARCHAR(50),
					email VARCHAR(50) UNIQUE NOT NULL,
					mobile VARCHAR(50),
					PRIMARY KEY(customer_id)
				);</pre>
			
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">pom.xml</h2>
			<pre class="brush: xml;">
			&lt;?xml version="1.0" encoding="UTF-8"?&gt;
			&lt;project xmlns="http://maven.apache.org/POM/4.0.0" 
					 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
					 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
					 			http://maven.apache.org/xsd/maven-4.0.0.xsd"&gt;
			    &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;
			
			    &lt;groupId&gt;com.codingraja.servlet&lt;/groupId&gt;
			    &lt;artifactId&gt;servlet-jsp-jdbc-crud-operations&lt;/artifactId&gt;
			    &lt;version&gt;0.0.1-SNAPSHOT&lt;/version&gt;
			    &lt;packaging&gt;war&lt;/packaging&gt;
			
			    &lt;name&gt;servlet-jsp-jdbc-crud-operations&lt;/name&gt;
			    
			    &lt;dependencies&gt;
			        &lt;dependency&gt;
			            &lt;groupId&gt;javax&lt;/groupId&gt;
			            &lt;artifactId&gt;javaee-web-api&lt;/artifactId&gt;
			            &lt;version&gt;7.0&lt;/version&gt;
			            &lt;scope&gt;provided&lt;/scope&gt;
			        &lt;/dependency&gt;
			        &lt;dependency&gt;
			        	&lt;groupId&gt;jstl&lt;/groupId&gt;
			        	&lt;version&gt;1.2&lt;/version&gt;
			        	&lt;artifactId&gt;jstl&lt;/artifactId&gt;
			        &lt;/dependency&gt;
			        &lt;dependency&gt;
			        	&lt;groupId&gt;mysql&lt;/groupId&gt;
			        	&lt;artifactId&gt;mysql-connector-java&lt;/artifactId&gt;
			        	&lt;version&gt;5.1.6&lt;/version&gt;
			        &lt;/dependency&gt;
			    &lt;/dependencies&gt;
			
			    &lt;build&gt;
			        &lt;plugins&gt;
			            &lt;plugin&gt;
			                &lt;groupId&gt;org.apache.maven.plugins&lt;/groupId&gt;
			                &lt;artifactId&gt;maven-compiler-plugin&lt;/artifactId&gt;
			                &lt;version&gt;3.2&lt;/version&gt;
			                &lt;configuration&gt;
			                    &lt;source&gt;1.8&lt;/source&gt;
			                    &lt;target&gt;1.8&lt;/target&gt;
			                &lt;/configuration&gt;
			            &lt;/plugin&gt;
			            &lt;plugin&gt;
			                &lt;groupId&gt;org.apache.maven.plugins&lt;/groupId&gt;
			                &lt;artifactId&gt;maven-war-plugin&lt;/artifactId&gt;
			                &lt;version&gt;2.4&lt;/version&gt;
			                &lt;configuration&gt;
			                    &lt;failOnMissingWebXml&gt;false&lt;/failOnMissingWebXml&gt;
			                &lt;/configuration&gt;
			            &lt;/plugin&gt;
			        &lt;/plugins&gt;
			    &lt;/build&gt;
			
			&lt;/project&gt;</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Database.java</h2>
			<pre class="brush: java;">
			package com.codingraja.db;

			import java.sql.Connection;
			import java.sql.DriverManager;
			
			public class Database {
				private static final String DRIVER = "com.mysql.jdbc.Driver";
				private static final String URL = "jdbc:mysql://localhost:3306/test";
				private static final String USERNAME = "root";
				private static final String PASSWORD = "root";
			
				private static Connection connection = null;
			
				public static Connection getConnection() {
					if (connection == null) {
						try {
							// Loading The Driver Class
							Class.forName(DRIVER);
			
							// Getting the connection Object
							connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
						} catch (Exception ex) {
							System.out.println(ex.getMessage());
						}
					}
			
					return connection;
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">Customer.java</h2>
			<pre class="brush: java;">
			package com.codingraja.domain;

			public class Customer {
				private Long id;
				private String firstName;
				private String lastName;
				private String email;
				private String mobile;
			
				public Customer() {
					// Do Nothing
				}
			
				public Customer(String firstName, String lastName, String email, String mobile) {
					this.firstName = firstName;
					this.lastName = lastName;
					this.email = email;
					this.mobile = mobile;
				}
			
				public Long getId() {
					return id;
				}
			
				public void setId(Long id) {
					this.id = id;
				}
			
				public String getFirstName() {
					return firstName;
				}
			
				public void setFirstName(String firstName) {
					this.firstName = firstName;
				}
			
				public String getLastName() {
					return lastName;
				}
			
				public void setLastName(String lastName) {
					this.lastName = lastName;
				}
			
				public String getEmail() {
					return email;
				}
			
				public void setEmail(String email) {
					this.email = email;
				}
			
				public String getMobile() {
					return mobile;
				}
			
				public void setMobile(String mobile) {
					this.mobile = mobile;
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">CustomerDao.java</h2>
			<pre class="brush: java;">
			package com.codingraja.dao;

			import java.util.List;
			
			import com.codingraja.domain.Customer;
			
			public interface CustomerDao {
				long saveCustomer(Customer customer);
			
				void updateCustomer(Customer customer);
			
				void deleteCustomer(Long id);
			
				Customer findCustomerById(Long id);
			
				List&lt;Customer&gt; findAllCustomers();
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">CustomerDaoImpl.java</h2>
			<pre class="brush: java;">
			package com.codingraja.dao.impl;

			import java.sql.Connection;
			import java.sql.PreparedStatement;
			import java.sql.ResultSet;
			import java.sql.Statement;
			import java.util.ArrayList;
			import java.util.List;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.db.Database;
			import com.codingraja.domain.Customer;
			
			public class CustomerDaoImpl implements CustomerDao {
			
				private static CustomerDaoImpl customerDaoImpl = null;
			
				private Connection connection = Database.getConnection();
			
				public long saveCustomer(Customer customer) {
					String sql = "INSERT INTO customer_master"+
								+"(first_name, last_name, email, mobile)" 
								+ "VALUES(?,?,?,?)";
					long id = 0;
			
					try {
						PreparedStatement pstmt = connection.prepareStatement(sql, 
													Statement.RETURN_GENERATED_KEYS);
						pstmt.setString(1, customer.getFirstName());
						pstmt.setString(2, customer.getLastName());
						pstmt.setString(3, customer.getEmail());
						pstmt.setString(4, customer.getMobile());
			
						// Creating Customer Account
						if (pstmt.executeUpdate() > 0) {
							// Returns Generated Primary Key
							ResultSet rs = pstmt.getGeneratedKeys(); 
																		 
							if (rs.next())
								id = rs.getLong(1);
						}
					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}
			
					return id;
				}
			
				public void updateCustomer(Customer customer) {
					String sql = "UPDATE customer_master SET" 
								+" first_name=?, last_name=?, email=?, mobile=? " 
								+ "WHERE customer_id=?";
			
					try {
						PreparedStatement pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, customer.getFirstName());
						pstmt.setString(2, customer.getLastName());
						pstmt.setString(3, customer.getEmail());
						pstmt.setString(4, customer.getMobile());
						pstmt.setLong(5, customer.getId());
			
						// Update Customer Account
						pstmt.executeUpdate();
			
					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}
				}
			
				public void deleteCustomer(Long id) {
					String sql = "DELETE FROM customer_master WHERE customer_id=?";
			
					try {
						PreparedStatement pstmt = connection.prepareStatement(sql);
						pstmt.setLong(1, id);
			
						// Delete Customer Account
						pstmt.executeUpdate();
			
					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}
				}
			
				public Customer findCustomerById(Long id) {
					String sql = "SELECT * FROM customer_master WHERE customer_id=?";
			
					try {
						PreparedStatement pstmt = connection.prepareStatement(sql);
						pstmt.setLong(1, id);
			
						// Getting Customer Detail
						ResultSet resultSet = pstmt.executeQuery();
						if (resultSet.next()) {
							Customer customer = new Customer();
							customer.setId(resultSet.getLong(1));
							customer.setFirstName(resultSet.getString(2));
							customer.setLastName(resultSet.getString(3));
							customer.setEmail(resultSet.getString(4));
							customer.setMobile(resultSet.getString(5));
			
							return customer;
						}
			
					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}
			
					return null;
				}
			
				public List&lt;Customer&gt; findAllCustomers() {
					String sql = "SELECT * FROM customer_master";
					List&lt;Customer&gt; customers = null;
					try {
						PreparedStatement pstmt = connection.prepareStatement(sql);
			
						// Getting Customer's Detail
						ResultSet resultSet = pstmt.executeQuery();
						while (resultSet.next()) {
							if (customers == null)
								customers = new ArrayList&lt;&gt;();
			
							Customer customer = new Customer();
							customer.setId(resultSet.getLong(1));
							customer.setFirstName(resultSet.getString(2));
							customer.setLastName(resultSet.getString(3));
							customer.setEmail(resultSet.getString(4));
							customer.setMobile(resultSet.getString(5));
			
							customers.add(customer);
						}
			
					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}
			
					return customers;
				}
			
				public static CustomerDao getInstance() {
					if (customerDaoImpl == null)
						customerDaoImpl = new CustomerDaoImpl();
			
					return customerDaoImpl;
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">HomeController.java</h2>
			<pre class="brush: java;">
			package com.codingraja.controller;

			import java.io.IOException;
			import java.util.List;
			
			import javax.servlet.ServletException;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.dao.impl.CustomerDaoImpl;
			import com.codingraja.domain.Customer;
			
			@WebServlet("/")
			public class HomeController extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				private CustomerDao customerDao = CustomerDaoImpl.getInstance();
				
				public HomeController() {
					// Do Nothing
				}
			
				protected void doGet(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
			
					List&lt;Customer&gt; customers = customerDao.findAllCustomers();
			
					request.setAttribute("customerList", customers);
			
					request.getRequestDispatcher("home.jsp").forward(request, response);
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
					doGet(request, response);
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">CustomerRegistrationController.java</h2>
			<pre class="brush: java;">
			package com.codingraja.controller;

			import java.io.IOException;
			import javax.servlet.ServletException;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.dao.impl.CustomerDaoImpl;
			import com.codingraja.domain.Customer;
			
			@WebServlet("/customer/register")
			public class CustomerRegistrationController extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				private CustomerDao customerDao = CustomerDaoImpl.getInstance();
			
				public CustomerRegistrationController() {
					// Do Nothing
				}
			
				protected void doGet(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
					request.getRequestDispatcher("/").forward(request, response);
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
			
					String custId = request.getParameter("id");
					String firstName = request.getParameter("firstName");
					String lastName = request.getParameter("lastName");
					String email = request.getParameter("email");
					String mobile = request.getParameter("mobile");
			
					Customer customer = new Customer(firstName, lastName, email, mobile);
			
					if (custId == null || custId == "")
						customerDao.saveCustomer(customer);
					else {
						Long id = Long.parseLong(custId);
						customer.setId(id);
						customerDao.updateCustomer(customer);
					}
			
					response.sendRedirect(request.getContextPath() + "/");
				}
			
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">UpdateCustomerController.java</h2>
			<pre class="brush: java;">
			package com.codingraja.controller;

			import java.io.IOException;
			
			import javax.servlet.ServletException;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.dao.impl.CustomerDaoImpl;
			import com.codingraja.domain.Customer;
			
			@WebServlet("/customer/update")
			public class UpdateCustomerController extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				public UpdateCustomerController() {
					// Do Nothing
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
			
					String custId = request.getParameter("custId");
			
					if (custId == "" || custId == null)
						request.getRequestDispatcher("/").forward(request, response);
					else {
						Long id = Long.parseLong(custId);
						CustomerDao customerDao = CustomerDaoImpl.getInstance();
						Customer customer = customerDao.findCustomerById(id);
			
						request.setAttribute("customer", customer);
			
						request.getRequestDispatcher("/").forward(request, response);
					}
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">DeleteCustomerController.java</h2>
			<pre class="brush: java;">
			package com.codingraja.controller;

			import java.io.IOException;
			
			import javax.servlet.ServletException;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.dao.impl.CustomerDaoImpl;
			
			@WebServlet("/customer/delete")
			public class DeleteCustomerController extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				public DeleteCustomerController() {
					// Do Nothing
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
			
					String custId = request.getParameter("custId");
			
					if (custId == "" || custId == null)
						request.getRequestDispatcher("/").forward(request, response);
					else {
						Long id = Long.parseLong(custId);
						CustomerDao customerDao = CustomerDaoImpl.getInstance();

						customerDao.deleteCustomer(id);
			
						response.sendRedirect(request.getContextPath() + "/");
					}
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">home.jsp</h2>
			<pre class="brush: xml;">
			&lt;%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
			    pageEncoding="ISO-8859-1"%&gt;
			&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %&gt;
			&lt;!DOCTYPE&gt;
			&lt;html&gt;
			&lt;head&gt;
			&lt;meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"&gt;
			&lt;title&gt;Servlet, JSP and JDBC CRUD Operations&lt;/title&gt;
			
			&lt;style type="text/css"&gt;
				body{
					text-align: center;
				}
				table {
					margin-left: 15%;
					min-width: 70%; 
					border: 1px solid #CCC;
					border-collapse: collapse; 
				}
				table tr{line-height: 30px;}
				table tr th { background: #000033; color: #FFF;}
				table tr td { border:1px solid #CCC; margin: 5px;}
				input[type=text], input[type=email], input[type=tel]{
					min-width: 60%;
				}
				input[type=submit], a{
					background: green;
					padding: 5px;
					margin: 5px;
					color: #FFF;
				}
				a{
					text-decoration: none;
				}
			&lt;/style&gt;
			&lt;/head&gt;
			&lt;body&gt;
				&lt;h1&gt;Servlet, JSP and JDBC CRUD Operations&lt;/h1&gt;
				&lt;c:url value="/customer/register" var="registerUrl" /&gt;
				&lt;form action="${'${'}registerUrl}" method="post"&gt;
					&lt;table&gt;
						&lt;c:if test="${'${'}customer.id ne null}"&gt;
							&lt;tr&gt;
							&lt;td&gt;Customer ID:&lt;/td&gt;
								&lt;td&gt;&lt;input type="text" name="id" value="${'${'}customer.id}" readonly="readonly"&gt;&lt;/td&gt;
							&lt;/tr&gt;
						&lt;/c:if&gt;
						&lt;tr&gt;
							&lt;td&gt;First Name:&lt;/td&gt;
							&lt;td&gt;&lt;input type="text" name="firstName" value="${'${'}customer.firstName}" required&gt;&lt;/td&gt;
						&lt;/tr&gt;
						&lt;tr&gt;
							&lt;td&gt;Last Name:&lt;/td&gt;
							&lt;td&gt;&lt;input type="text" name="lastName" value="${'${'}customer.lastName}" required&gt;&lt;/td&gt;
						&lt;/tr&gt;
						&lt;tr&gt;
							&lt;td&gt;Email:&lt;/td&gt;
							&lt;td&gt;&lt;input type="email" name="email" value="${'${'}customer.email}" required&gt;&lt;/td&gt;
						&lt;/tr&gt;
						&lt;tr&gt;
							&lt;td&gt;Mobile:&lt;/td&gt;
							&lt;td&gt;&lt;input type="tel" pattern="[789][0-9]{9}" name="mobile" value="${'${'}customer.mobile}" required&gt;&lt;/td&gt;
						&lt;/tr&gt;
			
						&lt;c:if test="${'${'}customer.id ne null}"&gt;
							&lt;tr&gt;
								&lt;td colspan="2"&gt;&lt;input type="submit" value="Update"&gt;&lt;/td&gt;
							&lt;/tr&gt;
						&lt;/c:if&gt;
						&lt;c:if test="${'${'}customer.id eq null}"&gt;
							&lt;tr&gt;
								&lt;td colspan="2"&gt;&lt;input type="submit" value="Save"&gt;&lt;/td&gt;
							&lt;/tr&gt;
						&lt;/c:if&gt;
					&lt;/table&gt;
				&lt;/form&gt;
				&lt;br&gt;
				&lt;h1&gt;List of Customers&lt;/h1&gt;
				&lt;table&gt;
					&lt;tr&gt;
						&lt;th&gt;ID&lt;/th&gt;
						&lt;th&gt;First Name&lt;/th&gt;
						&lt;th&gt;Last Name&lt;/th&gt;
						&lt;th&gt;Email&lt;/th&gt;
						&lt;th&gt;Mobile&lt;/th&gt;
						&lt;th&gt;Update&lt;/th&gt;
						&lt;th&gt;Delete&lt;/th&gt;
					&lt;/tr&gt;
					&lt;c:forEach items="${'${'}customerList}" var="customer"&gt;
						&lt;tr&gt;
							&lt;td&gt;${'${'}customer.id}&lt;/td&gt;
							&lt;td&gt;${'${'}customer.firstName}&lt;/td&gt;
							&lt;td&gt;${'${'}customer.lastName}&lt;/td&gt;
							&lt;td&gt;${'${'}customer.email}&lt;/td&gt;
							&lt;td&gt;${'${'}customer.mobile}&lt;/td&gt;
							
							&lt;td&gt;
								&lt;form action="&lt;c:url value="/customer/update"/&gt;" method="post"&gt;
									&lt;input type="hidden" name="custId" value="${'${'}customer.id}"&gt;
									&lt;input type="submit" value="Update"&gt;
								&lt;/form&gt;
							&lt;td&gt;
								&lt;form action="&lt;c:url value="/customer/delete"/&gt;" method="post"&gt;
									&lt;input type="hidden" name="custId" value="${'${'}customer.id}"&gt;
									&lt;input style="background: #F00;" type="submit" value="Delete"&gt;
								&lt;/form&gt;
							&lt;/td&gt;
						&lt;/tr&gt;
					&lt;/c:forEach&gt;
				&lt;/table&gt;
			&lt;/body&gt;
			&lt;/html&gt;</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Deploy Application on Server</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="image_frame">
		        	<img alt="CRUD Operations" src="${imageUrl}/servlet/servlet-jsp-jdbc-crud-operations1.jpg">
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="download-box">
			<c:url value="/articles/servlet/src" var="servletSrcUrl" />	
			<a href="${servletSrcUrl}/servlet-jsp-jdbc-crud-operations.zip"
			   data-download="${servletSrcUrl}/servlet-jsp-jdbc-crud-operations.zip">
				<img alt="Download" src="${imageUrl}/download.jpg">
			</a>
		</div>
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
