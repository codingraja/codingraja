<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Servlet, JSP and Hibernate CRUD Operations</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/servlet/servlet-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Servlet, JSP and Hibernate  CRUD Operations</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="blog_content">
					In this tutorial, we will cover all the topics of <em>Servlet Technology</em> 
					and all the useful applications with real time scenario.
				</div>
				
				<%@ include file="../../servlet-app-list.jsp" %>
		        
		        <h1>Eclipse Project Structure</h1>
		        <img alt="CRUD Operations" src="${imageUrl}/servlet/servlet-jsp-hibernate-crud-operations.jpg">
			
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">pom.xml</h2>
			<pre class="brush: xml;">
			&lt;?xml version="1.0" encoding="UTF-8"?&gt;
			&lt;project xmlns="http://maven.apache.org/POM/4.0.0" 
					 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
					 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
					 			http://maven.apache.org/xsd/maven-4.0.0.xsd"&gt;
			    &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;
			
			    &lt;groupId&gt;com.codingraja.servlet&lt;/groupId&gt;
			    &lt;artifactId&gt;servlet-jsp-hibernate-crud-operations&lt;/artifactId&gt;
			    &lt;version&gt;0.0.1-SNAPSHOT&lt;/version&gt;
			    &lt;packaging&gt;war&lt;/packaging&gt;
			
			    &lt;name&gt;servlet-jsp-hibernate-crud-operations&lt;/name&gt;
			    
			    &lt;dependencies&gt;
			        &lt;dependency&gt;
			            &lt;groupId&gt;javax&lt;/groupId&gt;
			            &lt;artifactId&gt;javaee-web-api&lt;/artifactId&gt;
			            &lt;version&gt;7.0&lt;/version&gt;
			            &lt;scope&gt;provided&lt;/scope&gt;
			        &lt;/dependency&gt;
			        
			        &lt;dependency&gt;
			        	&lt;groupId&gt;jstl&lt;/groupId&gt;
			        	&lt;artifactId&gt;jstl&lt;/artifactId&gt;
			        	&lt;version&gt;1.2&lt;/version&gt;
			        &lt;/dependency&gt;
			        
			        &lt;dependency&gt;
			        	&lt;groupId&gt;org.hibernate&lt;/groupId&gt;
			        	&lt;artifactId&gt;hibernate-core&lt;/artifactId&gt;
			        	&lt;version&gt;5.1.0.Final&lt;/version&gt;
			        &lt;/dependency&gt;
			        
			        &lt;dependency&gt;
			        	&lt;groupId&gt;mysql&lt;/groupId&gt;
			        	&lt;artifactId&gt;mysql-connector-java&lt;/artifactId&gt;
			        	&lt;version&gt;5.1.6&lt;/version&gt;
			        &lt;/dependency&gt;
			    &lt;/dependencies&gt;
			
			    &lt;build&gt;
			        &lt;plugins&gt;
			            &lt;plugin&gt;
			                &lt;groupId&gt;org.apache.maven.plugins&lt;/groupId&gt;
			                &lt;artifactId&gt;maven-compiler-plugin&lt;/artifactId&gt;
			                &lt;version&gt;3.2&lt;/version&gt;
			                &lt;configuration&gt;
			                    &lt;source&gt;1.8&lt;/source&gt;
			                    &lt;target&gt;1.8&lt;/target&gt;
			                &lt;/configuration&gt;
			            &lt;/plugin&gt;
			            &lt;plugin&gt;
			                &lt;groupId&gt;org.apache.maven.plugins&lt;/groupId&gt;
			                &lt;artifactId&gt;maven-war-plugin&lt;/artifactId&gt;
			                &lt;version&gt;2.4&lt;/version&gt;
			                &lt;configuration&gt;
			                    &lt;failOnMissingWebXml&gt;false&lt;/failOnMissingWebXml&gt;
			                &lt;/configuration&gt;
			            &lt;/plugin&gt;
			        &lt;/plugins&gt;
			    &lt;/build&gt;
			
			&lt;/project&gt;</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">hibernate.cfg.xml</h2>
			<pre class="brush: xml;">
				&lt;?xml version="1.0" encoding="UTF-8"?&gt;

				&lt;!DOCTYPE hibernate-configuration PUBLIC
					"-//Hibernate/Hibernate Configuration DTD 3.0//EN"
					"http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd"&gt;
					
				&lt;hibernate-configuration&gt;
					&lt;session-factory&gt;
						
						&lt;!-- Datasource Details --&gt;
						&lt;property name="hibernate.connection.driver_class"&gt;
							com.mysql.jdbc.Driver
						&lt;/property&gt;
						&lt;property name="hibernate.connection.url"&gt;
							jdbc:mysql://localhost:3306/test
						&lt;/property&gt;
						&lt;property name="hibernate.connection.username"&gt;root&lt;/property&gt;
						&lt;property name="hibernate.connection.password"&gt;root&lt;/property&gt;
						
						&lt;!-- Hibernate Properties --&gt;
						&lt;property name="hibernate.hbm2ddl.auto"&gt;update&lt;/property&gt;
						&lt;property name="hibernate.dialect"&gt;
							org.hibernate.dialect.MySQLDialect
						&lt;/property&gt;
						
						&lt;!-- Resource Mapping --&gt;
						&lt;mapping class="com.codingraja.domain.Customer"/&gt;
						
					&lt;/session-factory&gt;
				&lt;/hibernate-configuration&gt;</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">HibernateUtil.java</h2>
			<pre class="brush: java;">
			package com.codingraja.util;

			import org.hibernate.SessionFactory;
			import org.hibernate.cfg.Configuration;
			
			public class HibernateUtil {
				private static SessionFactory sessionFactory = null;
			
				public static SessionFactory getSessionFactory() {
					if (sessionFactory == null) {
						Configuration configuration = new Configuration();
						configuration.configure("hibernate.cfg.xml");
						sessionFactory = configuration.buildSessionFactory();
					}
			
					return sessionFactory;
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">Customer.java</h2>
			<pre class="brush: java;">
			package com.codingraja.domain;

			import javax.persistence.Column;
			import javax.persistence.Entity;
			import javax.persistence.GeneratedValue;
			import javax.persistence.GenerationType;
			import javax.persistence.Id;
			import javax.persistence.Table;
			
			@Entity
			@Table(name="CUSTOMER_MASTER1")
			public class Customer {
				@Id
				@GeneratedValue(strategy=GenerationType.IDENTITY)
				@Column(name="CUSTOMER_ID")
				private Long id;
				@Column(name="FIRST_NAME")
				private String firstName;
				@Column(name="LAST_NAME")
				private String lastName;
				@Column(name="EMAIL", unique=true)
				private String email;
				@Column(name="MOBILE")
				private String mobile;
			
				public Customer() {
					// Do Nothing
				}
			
				public Customer(String firstName, String lastName, String email, String mobile) {
					this.firstName = firstName;
					this.lastName = lastName;
					this.email = email;
					this.mobile = mobile;
				}
			
				//Getters and Setters
				
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">CustomerDao.java</h2>
			<pre class="brush: java;">
			package com.codingraja.dao;

			import java.util.List;
			
			import com.codingraja.domain.Customer;
			
			public interface CustomerDao {
				long saveCustomer(Customer customer);
			
				void updateCustomer(Customer customer);
			
				void deleteCustomer(Long id);
			
				Customer findCustomerById(Long id);
			
				List&lt;Customer&gt; findAllCustomers();
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">CustomerDaoImpl.java</h2>
			<pre class="brush: java;">
			package com.codingraja.dao.impl;

			import java.util.List;
			
			import org.hibernate.Session;
			import org.hibernate.SessionFactory;
			import org.hibernate.Transaction;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.domain.Customer;
			import com.codingraja.util.HibernateUtil;
			
			public class CustomerDaoImpl implements CustomerDao {
			
				private static CustomerDaoImpl customerDaoImpl = null;
				
				private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
				
				public Long saveCustomer(Customer customer) {
					Session session = this.sessionFactory.openSession();
					Transaction transaction = session.beginTransaction();
					Long id = (Long)session.save(customer);
					transaction.commit();
					session.close();
					
					return id;		
				}
			
				public void updateCustomer(Customer customer) {
					Session session = this.sessionFactory.openSession();
					Transaction transaction = session.beginTransaction();
					session.update(customer);
					transaction.commit();
					session.close();
				}
			
				public void deleteCustomer(Long id) {
					Session session = this.sessionFactory.openSession();
					Transaction transaction = session.beginTransaction();
					Customer customer = session.get(Customer.class, id);
					session.delete(customer);
					transaction.commit();
					session.close();
				}
			
				public Customer findCustomerById(Long id) {
					Session session = this.sessionFactory.openSession();
					Customer customer = session.get(Customer.class, id);
					session.close();
					
					return customer;
				}
			
				@SuppressWarnings("unchecked")
				public List&lt;Customer&gt; findAllCustomers() {
					Session session = this.sessionFactory.openSession();
					List&lt;Customer&gt; customerList = session.createCriteria(Customer.class).list();
					session.close();
					
					return customerList;
				}
				
				public static CustomerDao getInstance() {
					if(customerDaoImpl == null)
						customerDaoImpl = new CustomerDaoImpl();
					
					return customerDaoImpl;
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">HomeController.java</h2>
			<pre class="brush: java;">
			package com.codingraja.controller;

			import java.io.IOException;
			import java.util.List;
			
			import javax.servlet.ServletException;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.dao.impl.CustomerDaoImpl;
			import com.codingraja.domain.Customer;
			
			@WebServlet("/")
			public class HomeController extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				private CustomerDao customerDao = CustomerDaoImpl.getInstance();
				
				public HomeController() {
					// Do Nothing
				}
			
				protected void doGet(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
			
					List&lt;Customer&gt; customers = customerDao.findAllCustomers();
			
					request.setAttribute("customerList", customers);
			
					request.getRequestDispatcher("home.jsp").forward(request, response);
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
					doGet(request, response);
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">CustomerRegistrationController.java</h2>
			<pre class="brush: java;">
			package com.codingraja.controller;

			import java.io.IOException;
			import javax.servlet.ServletException;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.dao.impl.CustomerDaoImpl;
			import com.codingraja.domain.Customer;
			
			@WebServlet("/customer/register")
			public class CustomerRegistrationController extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				private CustomerDao customerDao = CustomerDaoImpl.getInstance();
			
				public CustomerRegistrationController() {
					// Do Nothing
				}
			
				protected void doGet(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
					request.getRequestDispatcher("/").forward(request, response);
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
			
					String custId = request.getParameter("id");
					String firstName = request.getParameter("firstName");
					String lastName = request.getParameter("lastName");
					String email = request.getParameter("email");
					String mobile = request.getParameter("mobile");
			
					Customer customer = new Customer(firstName, lastName, email, mobile);
			
					if (custId == null || custId == "")
						customerDao.saveCustomer(customer);
					else {
						Long id = Long.parseLong(custId);
						customer.setId(id);
						customerDao.updateCustomer(customer);
					}
			
					response.sendRedirect(request.getContextPath() + "/");
				}
			
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">UpdateCustomerController.java</h2>
			<pre class="brush: java;">
			package com.codingraja.controller;

			import java.io.IOException;
			
			import javax.servlet.ServletException;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.dao.impl.CustomerDaoImpl;
			import com.codingraja.domain.Customer;
			
			@WebServlet("/customer/update")
			public class UpdateCustomerController extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				public UpdateCustomerController() {
					// Do Nothing
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
			
					String custId = request.getParameter("custId");
			
					if (custId == "" || custId == null)
						request.getRequestDispatcher("/").forward(request, response);
					else {
						Long id = Long.parseLong(custId);
						CustomerDao customerDao = CustomerDaoImpl.getInstance();
						Customer customer = customerDao.findCustomerById(id);
			
						request.setAttribute("customer", customer);
			
						request.getRequestDispatcher("/").forward(request, response);
					}
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">DeleteCustomerController.java</h2>
			<pre class="brush: java;">
			package com.codingraja.controller;

			import java.io.IOException;
			
			import javax.servlet.ServletException;
			import javax.servlet.annotation.WebServlet;
			import javax.servlet.http.HttpServlet;
			import javax.servlet.http.HttpServletRequest;
			import javax.servlet.http.HttpServletResponse;
			
			import com.codingraja.dao.CustomerDao;
			import com.codingraja.dao.impl.CustomerDaoImpl;
			
			@WebServlet("/customer/delete")
			public class DeleteCustomerController extends HttpServlet {
				private static final long serialVersionUID = 1L;
			
				public DeleteCustomerController() {
					// Do Nothing
				}
			
				protected void doPost(HttpServletRequest request, HttpServletResponse response)
						throws ServletException, IOException {
			
					String custId = request.getParameter("custId");
			
					if (custId == "" || custId == null)
						request.getRequestDispatcher("/").forward(request, response);
					else {
						Long id = Long.parseLong(custId);
						CustomerDao customerDao = CustomerDaoImpl.getInstance();

						customerDao.deleteCustomer(id);
			
						response.sendRedirect(request.getContextPath() + "/");
					}
				}
			}</pre>
		</div> <!-- # End Code -->
		
		<div class="program_code">
			<h2 class="file_name">home.jsp</h2>
			<pre class="brush: xml;">
			&lt;%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
			    pageEncoding="ISO-8859-1"%&gt;
			&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %&gt;
			&lt;!DOCTYPE&gt;
			&lt;html&gt;
			&lt;head&gt;
			&lt;meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"&gt;
			&lt;title&gt;Servlet, JSP and Hibernate CRUD Operations&lt;/title&gt;
			
			&lt;style type="text/css"&gt;
				body{
					text-align: center;
				}
				table {
					margin-left: 15%;
					min-width: 70%; 
					border: 1px solid #CCC;
					border-collapse: collapse; 
				}
				table tr{line-height: 30px;}
				table tr th { background: #000033; color: #FFF;}
				table tr td { border:1px solid #CCC; margin: 5px;}
				input[type=text], input[type=email], input[type=tel]{
					min-width: 60%;
				}
				input[type=submit], a{
					background: green;
					padding: 5px;
					margin: 5px;
					color: #FFF;
				}
				a{
					text-decoration: none;
				}
			&lt;/style&gt;
			&lt;/head&gt;
			&lt;body&gt;
				&lt;h1&gt;Servlet, JSP and Hibernate CRUD Operations&lt;/h1&gt;
				&lt;c:url value="/customer/register" var="registerUrl" /&gt;
				&lt;form action="${'${'}registerUrl}" method="post"&gt;
					&lt;table&gt;
						&lt;c:if test="${'${'}customer.id ne null}"&gt;
							&lt;tr&gt;
							&lt;td&gt;Customer ID:&lt;/td&gt;
								&lt;td&gt;&lt;input type="text" name="id" value="${'${'}customer.id}" readonly="readonly"&gt;&lt;/td&gt;
							&lt;/tr&gt;
						&lt;/c:if&gt;
						&lt;tr&gt;
							&lt;td&gt;First Name:&lt;/td&gt;
							&lt;td&gt;&lt;input type="text" name="firstName" value="${'${'}customer.firstName}" required&gt;&lt;/td&gt;
						&lt;/tr&gt;
						&lt;tr&gt;
							&lt;td&gt;Last Name:&lt;/td&gt;
							&lt;td&gt;&lt;input type="text" name="lastName" value="${'${'}customer.lastName}" required&gt;&lt;/td&gt;
						&lt;/tr&gt;
						&lt;tr&gt;
							&lt;td&gt;Email:&lt;/td&gt;
							&lt;td&gt;&lt;input type="email" name="email" value="${'${'}customer.email}" required&gt;&lt;/td&gt;
						&lt;/tr&gt;
						&lt;tr&gt;
							&lt;td&gt;Mobile:&lt;/td&gt;
							&lt;td&gt;&lt;input type="tel" pattern="[789][0-9]{9}" name="mobile" value="${'${'}customer.mobile}" required&gt;&lt;/td&gt;
						&lt;/tr&gt;
			
						&lt;c:if test="${'${'}customer.id ne null}"&gt;
							&lt;tr&gt;
								&lt;td colspan="2"&gt;&lt;input type="submit" value="Update"&gt;&lt;/td&gt;
							&lt;/tr&gt;
						&lt;/c:if&gt;
						&lt;c:if test="${'${'}customer.id eq null}"&gt;
							&lt;tr&gt;
								&lt;td colspan="2"&gt;&lt;input type="submit" value="Save"&gt;&lt;/td&gt;
							&lt;/tr&gt;
						&lt;/c:if&gt;
					&lt;/table&gt;
				&lt;/form&gt;
				&lt;br&gt;
				&lt;h1&gt;List of Customers&lt;/h1&gt;
				&lt;table&gt;
					&lt;tr&gt;
						&lt;th&gt;ID&lt;/th&gt;
						&lt;th&gt;First Name&lt;/th&gt;
						&lt;th&gt;Last Name&lt;/th&gt;
						&lt;th&gt;Email&lt;/th&gt;
						&lt;th&gt;Mobile&lt;/th&gt;
						&lt;th&gt;Update&lt;/th&gt;
						&lt;th&gt;Delete&lt;/th&gt;
					&lt;/tr&gt;
					&lt;c:forEach items="${'${'}customerList}" var="customer"&gt;
						&lt;tr&gt;
							&lt;td&gt;${'${'}customer.id}&lt;/td&gt;
							&lt;td&gt;${'${'}customer.firstName}&lt;/td&gt;
							&lt;td&gt;${'${'}customer.lastName}&lt;/td&gt;
							&lt;td&gt;${'${'}customer.email}&lt;/td&gt;
							&lt;td&gt;${'${'}customer.mobile}&lt;/td&gt;
							
							&lt;td&gt;
								&lt;form action="&lt;c:url value="/customer/update"/&gt;" method="post"&gt;
									&lt;input type="hidden" name="custId" value="${'${'}customer.id}"&gt;
									&lt;input type="submit" value="Update"&gt;
								&lt;/form&gt;
							&lt;td&gt;
								&lt;form action="&lt;c:url value="/customer/delete"/&gt;" method="post"&gt;
									&lt;input type="hidden" name="custId" value="${'${'}customer.id}"&gt;
									&lt;input style="background: #F00;" type="submit" value="Delete"&gt;
								&lt;/form&gt;
							&lt;/td&gt;
						&lt;/tr&gt;
					&lt;/c:forEach&gt;
				&lt;/table&gt;
			&lt;/body&gt;
			&lt;/html&gt;</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Deploy Application on Server</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="image_frame">
		        	<img alt="CRUD Operations" src="${imageUrl}/servlet/servlet-jsp-hibernate-crud-operations1.jpg">
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="download-box">
			<c:url value="/articles/servlet/src" var="servletSrcUrl" />	
			<a href="${servletSrcUrl}/servlet-jsp-hibernate-crud-operations.zip" 
			   data-download="${servletSrcUrl}/servlet-jsp-hibernate-crud-operations.zip">
				<img alt="Download" src="${imageUrl}/download.jpg">
			</a>
		</div>
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
