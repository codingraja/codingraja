<!doctype html> <!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Servlet and JSP Interview Questions and Answers</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/interviews/interviews-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Servlet &amp; JSP Interview Questions and Answers</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<!-- Servlet Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Servlet Interview Questions
					</h1>
					<ol>
						<li><a href="">What is Client-Server Architecture?</a></li>
						<li><a href="">What is different between web server and application server?</a></li>
						<li><a href="">What is a Servlet and why we use?</a></li>
						<li><a href="">What is a java web project directory structure?</a></li>
						<li><a href="">What are common tasks performed by Servlet Container?</a></li>
						<li><a href="">What is a deployment descriptor and elements of deployment descriptor?</a></li>
						<li><a href="">What are the phases of Servlet life cycle?</a></li>
						<li><a href="">What is difference between GenericServlet and HttpServlet?</a></li>
						<li><a href="">What is ServletRequest object?</a></li>
						<li><a href="">What is ServletResponse object?</a></li>
						<li><a href="">What is ServletConfig object?</a></li>
						<li><a href="">What is ServletContext object?</a></li>
						<li><a href="">What is Query String?</a></li>
						<li><a href="">What is the difference between GET and POST method?</a></li>
						<li><a href="">What is idempotent and non-idempotent? </a></li>
						<li><a href="">What is difference between ServletConfig and ServletContext?</a></li>
						<li><a href="">What is RequestDispatcher?</a></li>
						<li><a href="">What is difference between sendRedirect () and forward() methods?</a></li>
						<li><a href="">How does Cookies work in Servlet and what are the disadvantages?</a></li>
						<li><a href="">What is HttpSession and how to get, set and invalidate Session?</a></li>
						<li><a href="">What is the difference between getSession () and getSession (false)?</a></li>
						<li><a href="">What is the difference between encodeRedirectUrl and encodeURL?</a></li>
						<li><a href="">What is Filter API's?</a></li>
						<li><a href="">What are Servlet listeners?</a></li>
						<li><a href="">Write a Servlet to upload file on server.</a></li>
						<li><a href="">How do we go with database connection in Servlet?</a></li>
						<li><a href="">How to get the IP address of client in Servlet?</a></li>
						<li><a href="">What are important features of Servlet 3.0?</a></li>
						<li><a href="">What is load-on-Startup?</a></li>
						<li><a href="">Can we use load-on-startup with Filters and Listeners</a></li>
					</ol>
				</div> <!-- Ends Servlet Interview Questions  -->
				
				<!-- JSP Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						JSP Interview Questions
					</h1>
					<ol>
						<li><a href="">What is JSP?</a></li>
						<li><a href="">What are advantages of JSP over Servlet?</a></li>
						<li><a href="">Explain lifecycle of a JSP.</a></li>
						<li><a href="">What is JSP translator?</a></li>
						<li><a href="">Explain JSP Scripting Tags?</a></li>
						<li><a href="">How can we avoid direct access of JSP pages from client browser?</a></li>
						<li><a href="">How to define comment in JSP?</a></li>
						<li><a href="">What are JSP implicit objects?</a></li>
						<li><a href="">Explain JSP directives?</a></li>
						<li><a href="">What are JSP actions?</a></li>
						<li><a href="">What is difference between error and errorPage attribute?</a></li>
						<li><a href="">What is the difference between jsp: include and include directive?</a></li>
						<li><a href="">What is the difference between JspWriter and PrintWriter?</a></li>
						<li><a href="">How is Session Management done in JSP?</a></li>
						<li><a href="">Difference between &lt;jsp: forward page = ... > and &lt;c: redirect>?</a></li>
						<li><a href="">What the different types of JSTL tags are?</a></li>
						<li><a href="">What is a JSP custom tag?</a></li>
						<li><a href="">What is JSP Expression Language? </a></li>
						<li><a href="">What are the implicit EL objects in JSP?</a></li>
						<li><a href="">How can we disable java code or scripting in JSP page?</a></li>
						<li><a href="">How can we disable EL?</a></li>
						<li><a href="">How can we handle runtime exception in JSP?</a></li>
						<li><a href="">How to iterate collection object in JSP?</a></li>
					</ol>
				</div> <!-- Ends JSP Interview Questions -->
		    </div>
		</div><!-- /# end post -->
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
