<!doctype html> <!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Spring Interview Questions and Answers</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/interviews/interviews-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Spring Interview Questions and Answers</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<!-- Spring Core Conatiner Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Spring Core Container Interview Questions
					</h1>
					<ol>
						<li><a href="#">What is Spring Framework?</a></li>
						<li><a href="#">What are the advantages and features of Spring Framework?</a></li>
						<li><a href="#">Explain Spring Framework Modules.</a></li>
						<li><a href="#">What are the Maven Dependencies for Spring Framework?</a></li>
						<li><a href="#">What is Spring IoC Container?</a></li>
						<li><a href="#">What is a Spring Bean?</a></li>
						<li><a href="#">What do you understand by Dependency Injection?</a></li>
						<li><a href="#">What are the differences between BeanFactory and ApplicationContext?</a></li>
						<li><a href="#">What is Constructor-based dependency injection </a></li>
						<li><a href="#">What is Setter-based dependency injection </a></li>
						<li><a href="#">What is Dependency resolution process ?</a></li>
						<li><a href="#">How to inject Primitive and String values ?</a></li>
						<li><a href="#">How to inject Null and Empty string values ?</a></li>
						<li><a href="#">How to inject Collections(List, Set, Map) and Properties ?</a></li>
						<li><a href="#">How to inject References of other beans (Collaborators or Dependent Objects) ?</a></li>
						<li><a href="#">What is Autowiring and autowiring modes? </a></li>
						<li><a href="#">What are the  inner beans? And What are  the Compound Properties ?</a></li>
						<li><a href="#">What is the Lazy-initialized bean ?</a></li>
						<li><a href="#">What are the Bean scopes ?</a></li>
						<li><a href="#">What will happened ,when we use Singleton bean with prototype-bean dependencies ?</a></li>
						<li><a href="#">What is the life-cycle of bean? And What is lifecycle callbacks?</a></li>
						<li><a href="#">What is Annotation-based container configuration ?</a></li>
						<li><a href="#">What is Java-based container configuration ?</a></li>
						<li><a href="#">What are different ways to configure a class as Spring Bean?</a></li>
						<li><a href="#">Does Spring Bean provide thread safety?</a></li>
						<li><a href="#">What is circular dependency?</a></li>
					</ol>
				</div> <!-- Ends Spring Core Conatiner Interview Questions -->
				
				<!-- Spring MVC Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Spring MVC Interview Questions
					</h1>
					<ol>
						<li><a href="#">What is Spring MVC and Spring Web MVC?</a></li>
						<li><a href="#">What is the front controller class of Spring MVC?</a></li>
						<li><a href="#">What is DispatcherServlet and ContextLoaderListener ?</a></li>
						<li><a href="#">What is WebApplicationContext ?</a></li>
						<li><a href="#">What is Spring MVC Flow ? Explain.</a></li>
						<li><a href="#">What is Controller ?</a></li>
						<li><a href="#">What is ViewResolver in Spring?</a></li>
						<li><a href="#">What is ResourceBundleMessageSource ?</a></li>
						<li><a href="#">What is a MultipartResolver and when its used?</a></li>
						<li><a href="#">How to handle exceptions in Spring MVC Framework?</a></li>
						<li><a href="#">Can we have multiple Spring configuration files?</a></li>
						<li><a href="#">What are the minimum configurations needed to create Spring MVC application?</a></li>
						<li><a href="#">How would you relate Spring MVC Framework to MVC architecture?</a></li>
						<li><a href="#">Can we send an Object as the response of Controller handler method?</a></li>
						<li><a href="#">How to upload file in Spring MVC Application?</a></li>
						<li><a href="#">How to validate form data in Spring Web MVC Framework?</a></li>
						<li><a href="#">What is the use of Validator Interface in Spring MVC?</a></li>
						<li><a href="#">What is JSR-303 ?</a></li>
						<li><a href="#">What is Spring MVC Interceptor and how to use it? </a></li>
						<li><a href="#">How to achieve localization in Spring MVC applications?</a></li>
						<li><a href="#">How can we use Spring to create Restful Web Service returning JSON response?</a></li>
						<li><a href="#">Difference between &lt;context:annotation-config&gt; vs &lt;context:component-scan&gt;?</a></li>
					</ol>
				</div> <!-- Ends Spring MVC Interview Questions -->
				
				<!-- Spring JDBC, DAO, ORM and Transaction Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Spring JDBC, DAO, ORM and Tx Interview Questions
					</h1>
					<ol>
						<li><a href="#">What is Spring JdbcTemplate class and how to use it?</a></li>
						<li><a href="#">What is DriverManagerDataSource in Spring ?</a></li>
						<li><a href="#">How to use BasicDataSource with spring</a></li>
						<li><a href="#">How to use Tomcat JNDI DataSource in Spring Web Application?</a></li>
						<li><a href="#">How would you achieve Transaction Management in Spring?</a></li>
						<li><a href="#">What is Spring DAO?</a></li>
						<li><a href="#">How to integrate Spring and Hibernate Frameworks?</a></li>
						<li><a href="#">What is @Transactional and why we use this ?</a></li>
					</ol>
				</div> <!-- Spring JDBC, DAO, ORM and Transaction Interview Questions -->
				
				<!-- Spring AOP Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Spring AOP Interview Questions
					</h1>
					<ol>
						<li><a href="#">What is Aspect Oriented Programming and benefits ?</a></li>
						<li><a href="#">What is Aspect, JointPoint, Advice, Pointcut, and Advice Arguments in AOP?</a></li>
						<li><a href="#">What are the types of Advices ?</a></li>
						<li><a href="#">What are the pointcut designators ?</a></li>
						<li><a href="#">What is target object in AOP ?</a></li>
						<li><a href="#">What is AOP Proxy ?</a></li>
						<li><a href="#">What is weaving in AOP ?</a></li>
						<li><a href="#">How to enable AOP in Spring Application ?</a></li>
						<li><a href="#">What is the difference between Spring AOP and AspectJ AOP ?</a></li>
					</ol>
				</div> <!-- Spring AOP Interview Questions -->
				
				<!-- Spring Security Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Spring Security Interview Questions
					</h1>
					<ol>
						<li><a href="#">what is spring security?</a></li>
						<li><a href="#">What are the Spring Security Core modules?</a></li>
						<li><a href="#">what is Authentication and Authorization?</a></li>
						<li><a href="#">Which to enable spring security in Spring Web Application?</a></li>
						<li><a href="#">What is Spring Security Namespace Configuration?</a></li>
						<li><a href="#">Define XML and Java Configuration of Spring Security.</a></li>
						<li><a href="#">How to create custom login page in spring security?</a></li>
						<li><a href="#">How to logout the session in Spring Security</a></li>
						<li><a href="#">What &lt;http> element does in Spring Securiry?</a></li>
						<li><a href="#">What &lt;authentication-manager> element does in Spring Securiry?</a></li>
						<li><a href="#">What &lt;authentication-provider> element does in Spring Securiry?</a></li>
						<li><a href="#">What is &lt;user-service> in Spring Securiry?</a></li>
						<li><a href="#">What is &lt;jdbc-user-service>  in Spring Securiry?</a></li>
						<li><a href="#">What is &lt;ldap-user-service> element does in Spring Securiry?</a></li>
						<li><a href="#">what is CSRF in Spring Security?</a></li>
						<li><a href="#">How to login with database in spring security?</a></li>
						<li><a href="#">How to encode password in spring security?</a></li>
						<li><a href="#">How to achieve "Remember Me" in spring security?</a></li>
						<li><a href="#">What is channel security in spring?</a></li>
						<li><a href="#">How to manage Session in spring security?</a></li>
						<li><a href="#">How to access user role in JSP in spring security?</a></li>
						<li><a href="#">How to access user role in Controller in spring security?</a></li>
						<li><a href="#">How to get user details in spring security?</a></li>
						<li><a href="#">How to create a secure method in spring security?</a></li>
						<li><a href="#">hasRole() VS hasAnyRole() in spring security?</a></li>
					</ol>
				</div> <!-- Spring Security Interview Questions -->
				
				<!-- Spring Misc Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Spring Misc Interview Questions
					</h1>
					<ol>
						<li><a href="#">What's the difference between @Component, @Repository &amp; @Service annotations in Spring?</a></li>
						<li><a href="#">What is the difference between @Configuration, @Configurable  and @Bean  ?</a></li>
						<li><a href="#">Why we  use @PostConstruct, @PreDestroyed and @Resource ? (JSR-250)</a></li>
						<li><a href="#">Name some of the design patterns used in Spring Framework?</a></li>
						<li><a href="#">What are some of the best practices for Spring Framework?</a></li>
					</ol>
				</div> <!-- Ends Spring Misc Interview Questions -->
				
		    </div>
		</div><!-- /# end post -->
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
