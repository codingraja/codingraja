<div class="sidebar_title"><h4 class="caps">Table of Contents</h4></div>
<div class="clearfix"></div>
  <div id="st-accordion-four" class="st-accordion-four">
	<ul>
		<li>
			<a href="${interviewsUrl}/core-java">Core Java Interview Questions<span class="st-arrow">Open or Close</span></a>
			<%-- <div class="st-content">
				<ol class="arrows_list1">		
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Introduction</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Spring Framework Modules</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Spring Installation</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Inversion of Control (IoC)</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> BeanFactory Vs ApplicationContext</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> First Spring Application</a></li>
				</ol>
			</div> --%>
		</li>
		
		<li>
			<a href="${interviewsUrl}/servlet-and-jsp">Servlet &amp; JSP Interview Questions<span class="st-arrow">Open or Close</span></a>
		</li>
		
		<li>
			<a href="${interviewsUrl}/hibernate">Hibernate Interview Questions<span class="st-arrow">Open or Close</span></a>
		</li>
		
		<li>
			<a href="${interviewsUrl}/spring">Spring Interview Questions<span class="st-arrow">Open or Close</span></a>
		</li>
		
		<li>
			<a href="${interviewsUrl}/web-services">Web Services Interview Questions<span class="st-arrow">Open or Close</span></a>
		</li>
		
		<li>
			<a href="${interviewsUrl}/manager-round">Manager Round Interview Questions<span class="st-arrow">Open or Close</span></a>
		</li>
	</ul>
</div>



<!-- #####################   Recent Posts   #################### -->
<div class="recent_posts">
	<%@ include file="/fragments/recent-posts.jsp" %>
</div> <!-- #####################  End Recent Posts   #################### -->