<!doctype html> <!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Hibernate Interview Questions and Answers</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/interviews/interviews-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Hibernate Interview Questions and Answers</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<!-- Hibernate Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Hibernate Interview Questions
					</h1>
					<ol>
						<li><a href="#hibernate-overview">What is Hibernate Framework?</a></li>
						<li><a href="#jpa-overview">What is Java Persistence API (JPA)?</a></li>
						<li><a href="#hibernate-benefits">What are the benefits of using Hibernate Framework?</a></li>
						<li><a href="#hibernate-vs-jdbc">What are the advantages of Hibernate over JDBC?</a></li>
						<li><a href="#hibernate-interfaces">Name some Core interfaces of Hibernate framework</a></li>
						<li><a href="#hibernate-configuration-file">What is hibernate configuration file?</a></li>
						<li><a href="#hibernate-mapping-file">What is hibernate mapping file?</a></li>
						<li><a href="#hibernate-annotations">List some annotations used for Hibernate mapping?</a></li>
						<li><a href="#hibernate-session-factory">What is Hibernate SessionFactory and how to configure it?</a></li>
						<li><a href="#session-factory-thread-safe">Hibernate SessionFactory is thread safe?</a></li>
						<li><a href="#hibernate-session">What is Hibernate Session and how to get it?</a></li>
						<li><a href="#session-thread-safe">Hibernate Session is thread safe?</a></li>
						<li><a href="#openSession-vs-getCurrentSession">What is difference between openSession and getCurrentSession? </a></li>
						<li><a href="#hibernate-get-vs-load">What is difference between Hibernate Session get() and load() method?</a></li>
						<li><a href="#hibernate-caching">What is hibernate caching? Explain Hibernate first level cache?</a></li>
						<li><a href="#hibernate-EHCache">How to configure Hibernate Second Level Cache using EHCache?</a></li>
						<li><a href="#entity-bean-states">What are different states of an entity bean?</a></li>
						<li><a href="#hibernate-merge">What is use of Hibernate Session merge() call?</a></li>
						<li><a href="#hibernate-save-saveOrUpdate-persist">What is difference between Hibernate save(), saveOrUpdate() and persist() methods?</a></li>
						<li><a href="#entity-bean-constructor">What will happen if we don&#8217;t have no-args constructor in Entity bean?</a></li>
						<li><a href="#sorted-vs-ordered-collection">What is difference between sorted collection and ordered collection?</a></li>
						<li><a href="#hibernate-collection-types">What are the collection types in Hibernate?</a></li>
						<li><a href="#hibernate-joins">How to implement Joins in Hibernate?</a></li>
						<li><a href="#entity-final-class">Why we should not make Entity Class final?</a></li>
						<li><a href="#hql-benefits">What is HQL and what are it&#8217;s benefits?</a></li>
						<li><a href="#query-cache">What is Query Cache in Hibernate?</a></li>
						<li><a href="#native-sql-query">Can we execute native sql query in hibernate?</a></li>
						<li><a href="#native-sql-benefits">What is the benefit of native sql query support in hibernate?</a></li>
						<li><a href="#named-sql-query">What is Named SQL Query?</a></li>
						<li><a href="#named-sql-query-benefits">What are the benefits of Named SQL Query?</a></li>
						<li><a href="#hibernate-criteria-benefits">What is the benefit of Hibernate Criteria API?</a></li>
						<li><a href="#hibernate-show-sql">How to log hibernate generated sql queries in log files?</a></li>
						<li><a href="#hibernate-proxy">What is Hibernate Proxy and how it helps in lazy loading?</a></li>
						<li><a href="#hibernate-mappings">How to implement relationships in hibernate?</a></li>
						<li><a href="#hibernate-transaction-management">How transaction management works in Hibernate?</a></li>
						<li><a href="#hibernate-cascading">What is cascading and what are different types of cascading?</a></li>
						<li><a href="#hibernate-log4j">How to integrate log4j logging in hibernate application?</a></li>
						<li><a href="#hibernate-jndi">How to use application server JNDI DataSource with Hibernate framework?</a></li>
						<li><a href="#spring-hibernate">How to integrate Hibernate and Spring frameworks?</a></li>
						<li><a href="#hibernatetemplate-class">What is HibernateTemplate class?</a></li>
						<li><a href="#hibernate-servlet">How to integrate Hibernate with Servlet or Struts2 web applications?</a></li>
						<li><a href="#hibernate-design-patterns">Which design patterns are used in Hibernate framework?</a></li>
						<li><a href="#hibernate-best-practices">What are best practices to follow with Hibernate framework?</a></li>
						<li><a href="#hibernate-validator">What is Hibernate Validator Framework?</a></li>
						<li><a href="#hibernate-tools-eclipse-plugin">What is the benefit of Hibernate Tools Eclipse plugin?</a></li>
					</ol>
				</div> <!-- Ends Hibernate Interview Questions -->
		    </div>
		</div><!-- /# end post -->
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
