<!doctype html> <!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Core Java Interview Questions and Answers</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/interviews/interviews-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Core Java Interview Questions and Answers</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						JDK, JRE and JVM Interview Questions And Answers
					</h1>
					<ol>
						<li><a href="#"> What is the JVM ?</a></li>
						<li><a href="#"> What is the JRE ?</a></li>
						<li><a href="#"> What is the JDK ?</a></li>
						<li><a href="#"> What is "write once, run anywhere" ?</a></li>
						<li><a href="#"> What is JIT ?</a></li>
						<li><a href="#"> What is JAVA Byte Code ?</a></li>
						<li><a href="#"> What is Class Loader ?</a></li>
						<li><a href="#"> Is it possible to create more than one .class file in single java Program ?</a></li>
						<li><a href="#"> What is the purpose of garbage collection in java ?</a></li>
						<li><a href="#"> What is difference between PATH and CLASSPATH ?</a></li>
						<li><a href="#"> How JVM allocate memory for Java Program ?</a></li>
						<li><a href="#"> finalize() Vs gc() Vs shudDownHook().</a></li>
						<li><a href="#"> Serial Vs Throughput garbage collector.</a></li>
						<li><a href="#"> Which algorithm is used in Garbage Collection ?</a></li>
						<li><a href="#"> What is memory leak in Java ?</a></li>
						<li><a href="#"> What is static Vs dynamic class loading in Java ? </a></li>
					</ol>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						Keywords and Methods Interview Questions
					</h1>
					<ol>
						<li><a href="#"> What is static in Java ?</a></li>
						<li><a href="#"> What is final in Java ? </a></li>
						<li><a href="#"> How does Java handle integer underflows and overflows ?.</a></li>
						<li><a href="#"> Is null a keyword?</a></li>
						<li><a href="#"> How many bits are used to represent Unicode, ASCII,UTF-16, and UTF-8 characters?</a></li>
						<li><a href="#"> Which class is the superclass of all classes?</a></li>
						<li><a href="#"> Why Java is not pure Object Oriented language?</a></li>
						<li><a href="#"> Can we overload main method ?</a></li>
						<li><a href="#"> Can we override main method ?</a></li>
						<li><a href="#"> Can we have multiple public classes in a java source file ?</a></li>
					</ol>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						OOP's Concept Interview Questions
					</h1>
					<ol>
						<li><a href=""> What is Object Oriented Programming?</a></li>
						<li><a href=""> What are different oops concepts in java ?</a></li>
						<li><a href=""> What is polymorphism ?</a></li>
						<li><a href=""> What is inheritance ?</a></li>
						<li><a href=""> What is abstraction ?</a></li>
						<li><a href=""> What is Encapsulation ?</a></li>
						<li><a href=""> What is Aggregation ?</a></li>
						<li><a href=""> What is Association ?</a></li>
						<li><a href=""> What is Composition ?</a></li>
						<li><a href=""> What is differences between method and Constructor</a></li>
						<li><a href=""> Method overloading Vs method overriding.</a></li>
						<li><a href=""> Interface Vs Abstract class.</a></li>
						<li><a href=""> Encapsulation Vs Data Binding.</a></li>
						<li><a href=""> Abstraction Vs Data Hiding.</a></li>
					</ol>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						String Handling Interview Questions
					</h1>
					<ol>
						<li><a href="">Why String is immutable in Java ?</a></li>
						<li><a href="">String Class Vs StringBuffer Class Vs StringBuilder Class.</a></li>
						<li><a href="">equals() Vs "==" Operator Vs compareTo().</a></li>
					</ol>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						Exception Handling Interview Questions
					</h1>
					<ol>
						<li><a href="#"> What are checked and unchecked exceptions ?</a></li>
						<li><a href="#"> Difference between Exception and Error.</a></li>
						<li><a href="#"> Difference between Throw and Throws</a></li>
						<li><a href="#"> In which Scenario finally block will not be executed.</a></li>
						<li><a href="#"> Can we write try block without catch block ?</a></li>
						<li><a href="#"> How to create Custom Exception ?</a></li>
						<li><a href="#"> What is printStackTrace ?</a></li>
					</ol>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						Multi Threading Interview Questions
					</h1>
					<ol>
						<li><a href="#"> What is multi-Tasking?</a></li>
						<li><a href="#"> What is difference between thread and process?</a></li>
						<li><a href="#"> What is multi-processing and multi-threating?</a></li>
						<li><a href="#"> How to create thread in Java?</a></li>
						<li><a href="#"> Thread Class Vs Runnable Interface. Which is better to use ?</a></li>
						<li><a href="#"> What is difference between start() and run() methods?</a></li>
						<li><a href="#"> Difference between sleep() and wait() method.</a></li>
						<li><a href="#"> What is Daemon Thread in Java ?</a></li>
						<li><a href="#"> Difference between notify() and notifyAll() methods.</a></li>
						<li><a href="#"> How to use Anonymous class in Multi-Threading ?</a></li>
						<li><a href="#"> What is yield() method ?</a></li>
						<li><a href="#"> stop() Vs suspend() vs destroy().</a></li>
						<li><a href="#"> What is volatile and atomic in Java ?</a></li>
						<li><a href="#"> What is join() ? How do you ensure sequence T1, T2, T3 in Java?</a></li>
						<li><a href="#"> What is synchronization and why is it important?</a></li>
						<li><a href="#"> What is ThreadLocal Class? How to use it ?</a></li>
						<li><a href="#"> What is Dead Lock ?</a></li>
						<li><a href="#"> What is Thread Pool ? Why should be used it ?</a></li>
						<li><a href="#"> What is worker thread?</a></li>
						<li><a href="#"> What is Thread Safe in Java ?</a></li>
						<li><a href="#"> synchronized block Vs synchronized method.</a></li>		
						<li><a href="#"> What is semaphore?</a></li>
						<li><a href="#"> What are monitors?</a></li>
						<li><a href="#"> Why wait(), notify() and notifyAll() are in Object Class?</a></li>
						<li><a href="#"> What is difference CyclicBarrier and CountDownLatch?</a></li>
						<li><a href="#"> What is producer and consumer problem?</a></li>
						<li><a href="#"> What is race condition in Java?</a></li>
						<li><a href="#">What is ThreadPoolExecutor in Java?</a></li>
						<li><a href="#">What is ReadWriteLock in Java?</a></li>
						<li><a href="#">What is busy spin in multi-threading?</a></li>
						<li><a href="#">What is double checked locking of Singleton?</a></li>
						<li><a href="#">What are the best practices in multi-threading? that you should use.</a></li>
						<li><a href="#">What is the fork-join framework in Java?</a></li>
					</ol>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						File I/O Interview Questions
					</h1>
					<ol>
						<li><a href="#"> What is serialization and de-serialization ?</a></li>
						<li><a href="#"> What is transient variable ?</a></li>
						<li><a href="#"> What are marker Interfaces ?</a></li>
						<li><a href="#"> How static works in serialization ?</a></li>
						<li><a href="#"> serialization Vs externalization ?</a></li>
						<li><a href="#"> What is serialVersionUID ? Why is it used ?</a></li>
						<li><a href="#"> Does Constructor call during de-serialization process ?</a></li>
						<li><a href="#"> What is cloning ?</a></li>
						<li><a href="#"> shallow cloning Vs deep cloning.</a></li>
					</ol>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						Collections Framework Interview Questions
					</h1>
					<ol>
						<li><a href="#"> What are the core Collection  interfaces ?</a></li>
						<li><a href="#"> What is the difference between Collection and Collections ?</a></li>
						<li><a href="#"> What is an iterator ?</a></li>
						<li><a href="#"> What is the difference between Iterator and ListIterator ?</a></li>
						<li><a href="#"> What is the difference between Array and ArrayList in Java ?</a></li>
						<li><a href="#"> What is EnumSet in Java ?</a></li>
						<li><a href="#"> What is ArrayList vs LinkedList ?</a></li>
						<li><a href="#"> What is ArrayList vs Vector ?</a></li>
						<li><a href="#"> What is HashMap vs ConcurrentHashMap ?</a></li>
						<li><a href="#"> What is HashMap vs Hashtable ?</a></li>
						<li><a href="#"> What is Comparator vs Comparable ?</a></li>
						<li><a href="#"> What is HashSet vs TreeSet ? </a></li>
						<li><a href="#"> What is Fail Fast Iterator vs Fail Safe Iterator ?</a></li>
						<li><a href="#"> What is Iterator vs Enumeration ?</a></li>
						<li><a href="#"> What is EntrySet vs KeySet ?</a></li>
					</ol>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						JDBC Interview Questions
					</h1>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						Core Java Misc Interview Questions
					</h1>
				</div>
		    </div>
		</div><!-- /# end post -->
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
