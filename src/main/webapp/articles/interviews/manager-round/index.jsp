<!doctype html> <!-- www.codingraja.com -->
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Project Manager Round Interview Questions and Answers</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/interviews/interviews-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>PM Round Interview Questions and Answers</h1>
		        
		        <div class="blog_content">
					In this round, Project manager will evaluate the candidate in various aspects like-
				</div>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<!-- Technical Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Technical Interview Questions
					</h1>
					<ol>
						<li><a href="#">What is your Project Architecture? </a></li>
						<li><a href="#">What are your roles and responsibilities in the project? </a></li>
						<li><a href="#">What are the technologies you have worked on? </a></li>
						<li><a href="#">What are the tools that are used in your projects? </a></li>
						<li><a href="#">Which IDE you have used for the development? </a></li>
						<li><a href="#">What are the database's you worked on? </a></li>
						<li><a href="#">What are the server's / platform's you worked on? </a></li>
						<li><a href="#">Which operating system / environment you are working on? </a></li>
						<li><a href="#">Have you ever been involved into Design? </a></li>
						<li><a href="#">What is the reporting tool you are using in your project? </a></li>
						<li><a href="#">What is the CIS/build management tool used in your project? </a></li>
						<li><a href="#">What is the volume of data against which your application runs? </a></li>
						<li><a href="#">How do you manage security like managing user credentials / data encryption etc?</a></li> 
						<li><a href="#">How do you write Unit Test Cases?</a></li>
					</ol>
				</div> <!-- Ends Technical Interview Questions -->
				
				<!-- Nature of Work Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Nature of Work Interview Questions
					</h1>
					<ol>
						<li><a href="#">Have you ever participated in release call? </a></li>
						<li><a href="#">Have you ever been involved into Testing? </a></li>
						<li><a href="#">What is your Team size? </a></li>
						<li><a href="#">What is the corporate training you have attended? </a></li>
						<li><a href="#">Do you have experience in production Support and what is the hierarchy in Production Support? </a></li>
					</ol>
				</div> <!-- Ends Nature of Work Interview Questions -->
				
				<!-- Leadership Qualities Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Leadership Qualities Interview Questions
					</h1>
					<ol>
						<li><a href="#">Tell and explain to us about a specific situation during the project where you have to cope with a less experienced candidate in your team and what are the measures you took to meet your deadlines?</a></li> 
						<li><a href="#">How do you handle non-productive team members?</a></li>
					</ol>
				</div> <!-- Ends Leadership Qualities Interview Questions -->
				
				<!-- Behavioral and commitment aspects Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Behavioral and Commitment Aspects Interview Questions
					</h1>
					<ol>
						<li><a href="#">Tell us some of your extraordinary skills which place you in the leading position over the other technical associates?</a></li>
						<li><a href="#">What were the criticism / difficulties you got during your projects, how did you handle it and what are the measures you look to avoid repetition of the same?</a></li>
					</ol>
				</div> <!-- Ends Behavioral and commitment aspects Interview Questions -->
				
				<!-- Source Control Management Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Source Control Management Interview Questions
					</h1>
					<ol>
						<li><a href="#">What is SCM tool?</a></li>
						<li><a href="#">What are the measures you take to ensure there are minimal code conflicts with other peers in the team?</a></li>
					</ol>
				</div> <!-- Ends Source Control Management Interview Questions -->
				
				<!-- Process Oriented Interview Questions -->
				<div class="interview">
					<h1 class="interview-question-cat">
						Process Oriented Interview Questions
					</h1>
					<ol>
						<li><a href="#">What are the Different phases in delivering the project during development and during maintenance?</a></li>
						<li><a href="#">How do you perform unit testing and make sure you covered all the aspects of your code?</a></li>
						<li><a href="#">What is the document that should be consulted to know about your project, the activities you do, to know about your project?</a></li>
						<li><a href="#">How do you come up with estimates for a requirement?</a></li>
					</ol>
				</div> <!-- Ends Process Oriented Interview Questions -->
				
		    </div>
		</div><!-- /# end post -->
		
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
