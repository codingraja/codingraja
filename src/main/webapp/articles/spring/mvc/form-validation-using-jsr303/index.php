<!DOCTYPE HTML>
<html lang="en-us">
<head>
<title>Spring MVC First Application</title>
<!-- Bootstrap -->
<?php include "$_SERVER[DOCUMENT_ROOT]/links.php"; ?>
</head>
<body>

<!--This is Header  -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/header.php"; ?>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/mvc/mvc-menu.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddRight.php"; ?>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddTop.php"; ?>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<!--  ########################### Main Content Start  #############################  -->

<h1>Spring MVC Form Validation Using Jsr-303</h1>

   <p>Validating data is a common task that occurs throughout any application, from the presentation layer to the persistence layer. 
	   Often the same validation logic is implemented in each layer, proving time consuming and error-prone.
	   To avoid duplication of these validations in each layer, developers often bundle validation logic directly
	   into the domain model with validation code which is really metadata about the class itself.</p>
   
   <p> <strong>JSR 303 - Bean Validation - </strong>defines a metadata model and API for entity validation. 
	   The default metadata source is annotations, with the ability to override and extend the meta-data through the use of XML.
	   The API is not tied to a specific application tier or programming model. It is specifically not tied to either the web tier 
	   or the persistence tier, and is available for both <strong>server-side</strong> application programming, as well as rich client Swing application developers.</p>
   
	
	<h4 id="sub-heading">@Controller Annotation</h4>
	 <div class="mlist">
		 <ol>
			<li>The @Controller annotation indicates that a particular class serves the role of a controller. Spring does not require you to extend any controller base class.</li>
			<li>The @Controller annotation acts as a stereotype for the annotated class, indicating its role. The dispatcher scans such annotated classes for mapped methods and detects @RequestMapping annotations.</li>
		</ol>
	</div>

	 <h4 id="sub-heading">Project Structure</h4><br/>
	<img class="img-responsive" src="../../images/fifth-application.png" /><br/>

	<div class="program-file">
		<div class="file-name">pom.xml</div>
		<pre class="brush: xml">
		 &lt;dependencies&gt;
			&lt;dependency&gt;
				&lt;groupId&gt;org.springframework&lt;/groupId&gt;
				&lt;artifactId&gt;spring-webmvc&lt;/artifactId&gt;
				&lt;version&gt;4.1.0.RELEASE&lt;/version&gt;
			&lt;/dependency&gt;
			 &lt;dependency&gt;
				&lt;groupId&gt;commons-logging&lt;/groupId&gt;
				&lt;artifactId&gt;commons-logging&lt;/artifactId&gt;
				&lt;version&gt;1.2&lt;/version&gt;
			&lt;/dependency&gt;
			&lt;dependency&gt;
				&lt;groupId&gt;javax.validation&lt;/groupId&gt;
				&lt;artifactId&gt;validation-api&lt;/artifactId&gt;
				&lt;version&gt;1.1.0.Final&lt;/version&gt;
			&lt;/dependency&gt;
			&lt;dependency&gt;
				&lt;groupId&gt;org.hibernate&lt;/groupId&gt;
				&lt;artifactId&gt;hibernate-validator&lt;/artifactId&gt;
				&lt;version&gt;5.1.0.Final&lt;/version&gt;
			&lt;/dependency&gt;
		  &lt;/dependencies&gt;
		</pre>
	</div>
	
	<div class="program-file">
		<div class="file-name">welcome.jsp</div>
		<pre class="brush: xml">
			 &lt;%@ page language="java" contentType="text/html; charset=ISO-8859-1"
					pageEncoding="ISO-8859-1" %&gt;
					
				&lt;%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %&gt;
				&lt;!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"&gt;
				&lt;html&gt;
				&lt;head&gt;
				&lt;meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"&gt;
				&lt;title&gt;Spring's Form Tags&lt;/title&gt;
				&lt;style type="text/css"&gt;
					.error{
						color:#FF0000;
						font-style: italic;
					}
				&lt;/style&gt;
				&lt;/head&gt;
				&lt;body&gt;
					&lt;h1&gt;Spring Form Validation&lt;/h1&gt;
					
					&lt;form:form method="POST" modelAttribute="user" action="register"&gt;
						&lt;table&gt;
							&lt;tr&gt;
								&lt;td&gt;User Name:&lt;/td&gt;
								&lt;td&gt;&lt;form:input path="userName"/&gt;&lt;/td&gt;
								&lt;td&gt;&lt;form:errors path="userName" cssClass="error" /&gt;&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Gender:&lt;/td&gt;
								&lt;td&gt;
									&lt;form:radiobutton path="gender" value="Male"/&gt;Male 
									&lt;form:radiobutton path="gender" value="Female"/&gt;Female
								&lt;/td&gt;
								&lt;td&gt;&lt;form:errors path="gender" cssClass="error"/&gt;&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Hobbies&lt;/td&gt;
								&lt;td&gt;
									&lt;form:checkbox path="hobbies" value="Coding"/&gt;Coding
									&lt;form:checkbox path="hobbies" value="Designing"/&gt;Designing
									&lt;form:checkbox path="hobbies" value="Testing"/&gt;Testing
								&lt;/td&gt;
								&lt;td&gt;&lt;form:errors path="hobbies" cssClass="error"/&gt;&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Skills:&lt;/td&gt;
								&lt;td&gt;
									&lt;form:select multiple="true" path="skills"&gt;
										&lt;option value="J2EE"&gt;J2EE&lt;/option&gt;
										&lt;option value="Spring"&gt;Spring&lt;/option&gt;
										&lt;option value="Hibernate"&gt;Hibernate&lt;/option&gt;
									&lt;/form:select&gt;
								&lt;/td&gt;
								&lt;td&gt;&lt;form:errors path="skills" cssClass="error"/&gt;&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Password:&lt;/td&gt;
								&lt;td&gt;
									&lt;form:password path="password"/&gt;
								&lt;/td&gt;
								&lt;td&gt;&lt;form:errors path="password" cssClass="error" /&gt;&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Address:&lt;/td&gt;
								&lt;td&gt;&lt;form:textarea path="address" rows="3" cols="30"/&gt;&lt;/td&gt;
							&lt;td&gt;&lt;form:errors path="address" cssClass="error" /&gt;&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td colspan="2"&gt;
									&lt;input type="submit" value="Submit"&gt;
								&lt;/td&gt;
							&lt;/tr&gt;
						&lt;/table&gt;
					&lt;/form:form&gt;
					
				&lt;/body&gt;
				&lt;/html&gt;
		</pre>
	</div>

	<div class="program-file">
		<div class="file-name">web.xml</div>
		<pre class="brush: xml">
		&lt;/?xml version="1.0" encoding="UTF-8"?&gt;
		&lt;/web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				 xmlns="http://xmlns.jcp.org/xml/ns/javaee" 
				 xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
				 id="WebApp_ID" version="3.1"&gt;
			  &lt;/welcome-file-list&gt;
				&lt;/welcome-file&gt;/&lt;/welcome-file&gt;
			  &lt;/welcome-file-list&gt;
			  
			  &lt;/servlet&gt;
				&lt;/servlet-name&gt;dispatcher&lt;/servlet-name&gt;
				&lt;/servlet-class&gt;org.springframework.web.servlet.DispatcherServlet&lt;/servlet-class&gt;
				&lt;/load-on-startup&gt;1&lt;/load-on-startup&gt;
			  &lt;/servlet&gt;
			  &lt;/servlet-mapping&gt;
				&lt;/servlet-name&gt;dispatcher&lt;/servlet-name&gt;
				&lt;/url-pattern&gt;/&lt;/url-pattern&gt;
			  &lt;/servlet-mapping&gt;
		&lt;/web-app&gt;
		</pre>
	</div>

	<div class="program-file">
		<div class="file-name">dispatcher-Servlet.xml</div>
		<pre class="brush: xml">
		&lt;?xml version="1.0" encoding="UTF-8"?&gt;
		&lt;beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			   xmlns:p="http://www.springframework.org/schema/p"
			   xmlns:context="http://www.springframework.org/schema/context"
			   xmlns:mvc="http://www.springframework.org/schema/mvc"
			   xmlns="http://www.springframework.org/schema/beans"
			   xsi:schemaLocation="http://www.springframework.org/schema/mvc
				http://www.springframework.org/schema/mvc/spring-mvc.xsd
				http://www.springframework.org/schema/beans
				http://www.springframework.org/schema/beans/spring-beans.xsd
				http://www.springframework.org/schema/context
				http://www.springframework.org/schema/context/spring-context.xsd"&gt;
				
			&lt;mvc:annotation-driven /&gt;  
			&lt;context:component-scan base-package="com.codingraja.spring.*" /&gt;
				
			&lt;bean id="viewResolver" 	class="org.springframework.web.servlet.view.InternalResourceViewResolver"&gt;
				&lt;property name="prefix" value="/WEB-INF/views/" /&gt;
				&lt;property name="suffix" value=".jsp" /&gt;
			&lt;/bean&gt;
			
		&lt;/beans&gt;
		</pre>
	</div>
	

	<div class="program-file">
		<div class="file-name">User.java</div>
		<pre class="brush: java">
		package com.codingraja.spring.bean;

		import javax.validation.constraints.Size;
		import org.hibernate.validator.constraints.NotEmpty;
		import org.springframework.stereotype.Component;

		@Component("user")
		public class User {
			@NotEmpty(message="Required Field")
			private String userName;
			@NotEmpty(message="Required Field")
			private String gender;
			@NotEmpty(message="Required Field")
			private String[] hobbies;
			@NotEmpty(message="Required Field")
			private String[] skills;
			@NotEmpty(message="Required Field")
			@Size(min=6,max=15)
			private String password;
			@NotEmpty(message="Required Field")
			private String address;
			
			public User() { }

			//Getters and Setters
		}
		</pre>
	</div>

	<div class="program-file">
		<div class="file-name">UserController.java</div>
		<pre class="brush: java">
			package com.codingraja.spring.controller;

			import org.springframework.stereotype.Controller;
			import org.springframework.ui.Model;
			import org.springframework.validation.BindingResult;
			import org.springframework.validation.annotation.Validated;
			import org.springframework.web.bind.annotation.ModelAttribute;
			import org.springframework.web.bind.annotation.RequestMapping;
			import org.springframework.web.servlet.ModelAndView;

			import com.codingraja.spring.bean.User;

			@Controller
			public class UserController {
				
				@RequestMapping("/")
				public String userForm(Model model) {
					User user = new User();
					model.addAttribute("user", user);
					return "welcome";
				}
				
				@RequestMapping("/register")
				public ModelAndView registerUser(@ModelAttribute @Validated User user, BindingResult result){
					if(result.hasErrors())
						return new ModelAndView("welcome");
					else
						return new ModelAndView("regSuccess","user",user);
				}
			}
		</pre>
	</div>
	
	<div class="program-file">
		<div class="file-name">regSuccess.jsp</div>
		<pre class="brush: xml">
			&lt;%@ page language="java" contentType="text/html; charset=ISO-8859-1"
				pageEncoding="ISO-8859-1" %&gt;
			&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %&gt;
			&lt;!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"&gt;
			&lt;html&gt;
			&lt;head&gt;
			&lt;title&gt;Show User Details&lt;/title&gt;
			&lt;/head&gt;
			&lt;body&gt;
				&lt;h1&gt;User details&lt;/h1&gt;
				&lt;p&gt;User Name: ${user.userName}&lt;/p&gt;
				&lt;p&gt;Gender: ${user.gender}&lt;/p&gt;
				&lt;p&gt;Hobbies: 
					&lt;c:forEach begin="0" items="${user.hobbies}" var="hob"&gt;
						&lt;c:out value="${hob}" /&gt;
					&lt;/c:forEach&gt;
				&lt;/p&gt;
				&lt;p&gt;Skills: 
					&lt;c:forEach begin="0" items="${user.skills}" var="skill"&gt;
						&lt;c:out value="${skill}" /&gt;
					&lt;/c:forEach&gt;
				&lt;/p&gt;
				&lt;p&gt;Password: ${user.password}&lt;/p&gt;
				&lt;p&gt;Address: ${user.address}&lt;/p&gt;
			&lt;/body&gt;
			&lt;/html&gt;
		</pre>
	</div>
	
	<h4 id="sub-heading">After Register it Redirect to Success.jsp page and Display the Data</h4>
	<img class="img-responsive" src="../../images/output10.png" /><br/>
	
	<h4 id="sub-heading">If field is emplty then it will display error messages</h4>
	<img class="img-responsive" src="../../images/output11.png" /><br/>

<!--  ########################### Main Content End    #############################  -->
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddBottom.php"; ?>
  </div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/footer.php"; ?>
</div>
</body>
</html>