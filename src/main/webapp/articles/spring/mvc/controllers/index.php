<!DOCTYPE HTML>
<html lang="en-us">
<head>
<title>Spring MVC Controllers</title>
<!-- Bootstrap -->
<?php include "$_SERVER[DOCUMENT_ROOT]/links.php"; ?>
</head>
<body>

<!--This is Header  -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/header.php"; ?>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/mvc/mvc-menu.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddRight.php"; ?>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddTop.php"; ?>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<!--  ########################### Main Content Start  #############################  -->

	<h1>Spring MVC Controllers</h1>
   
	 <div class="mlist">
		 <ol>
			<li>Controllers interpret user input and transform it into a model that is represented to the user by the view.</li>
			<li>Spring 2.5 introduced many annotations for the MVC Controller these are</li>
			<li><b>@RequestMapping, @RequestParam, @ModelAttribute etc.</b></li>

		</ol>
	</div>
	
	
	<div class="program-file">
		<div class="file-name">HelloSpringController.java</div>
		<pre class="brush: java">
			@Controller
			public class HelloSpringController {
			 
				@RequestMapping("/helloSpringMvc")
				public String helloSpringMvc(Model model) {
					model.addAttribute("message", "Hello Spring MVC!");
					return "helloSpringMvc";
				}
			}
		</pre>
	</div>
	
	<p>The <strong>@Controller</strong> and <strong>@RequestMapping</strong> annotations allow flexible method names and signatures. 
	In this particular example the method accepts a Model and returns a view name as a String.</p>
	
	<h4 id="sub-heading">@Controller Annotation</h4>
	 <div class="mlist">
		 <ol>
			<li>The @Controller annotation indicates that a particular class serves the role of a controller. Spring does not require you to extend any controller base class.</li>
			<li>The @Controller annotation acts as a stereotype for the annotated class, indicating its role. The dispatcher scans such annotated classes for mapped methods and detects @RequestMapping annotations.</li>
		</ol>
	</div>
	
	
	<h4 id="sub-heading">To enable autodetection of such annotated controllers, Use the spring-context schema as shown in the following XML snippet</h4>
	<div class="program-file">
		<div class="file-name">application.xml</div>
			<pre class="brush: xml">
				&lt;--?xml version="1.0" encoding="UTF-8"?--/&gt;
				&lt;beans xmlns="http://www.springframework.org/schema/beans"
					xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
					xmlns:p="http://www.springframework.org/schema/p"
					xmlns:context="http://www.springframework.org/schema/context"
					xsi:schemaLocation="
						http://www.springframework.org/schema/beans
						http://www.springframework.org/schema/beans/spring-beans.xsd
						http://www.springframework.org/schema/context
						http://www.springframework.org/schema/context/spring-context.xsd"&gt;
				 
					&lt;context:component-scan base-package="com.codingraja.controller"/&gt;
				   &lt;!-- ... --&gt;
				 
				&lt;/beans&gt;
			</pre>
	</div>

<!--  ########################### Main Content End    #############################  -->
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddBottom.php"; ?>
  </div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/footer.php"; ?>
</div>
</body>
</html>