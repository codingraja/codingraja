<!DOCTYPE HTML>
<html lang="en-us">
<head>
<title>Form Tags in Spring MVC</title>
<!-- Bootstrap -->
<?php include "$_SERVER[DOCUMENT_ROOT]/links.php"; ?>
</head>
<body>

<!--This is Header  -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/header.php"; ?>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/mvc/mvc-menu.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddRight.php"; ?>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddTop.php"; ?>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<!--  ########################### Main Content Start  #############################  -->

<h1>Spring MVC Form Tags</h1>
   
	<h4 id="sub-heading">View Resolver</h4>
	 <div class="mlist">
		<p>We can integrate any view technology with Spring, for this spring defines different-different kinds of view resolvers. 
		Here we are using JSP, for JSPs you’ll need a view resolver that will resolve your views.
		The most commonly used view resolvers when developing with JSPs are the <strong>InternalResourceViewResolver</strong> and the <strong>ResourceBundleViewResolver.</strong>
		Both are declared in the WebApplicationContext.</p>
	</div>
	
	
	<div class="program-file">
		<div class="file-name">dispatcher-Servlet.xml</div>
		<pre class="brush: xml">
		&lt;bean id="viewResolver" class="org.springframework.web.servlet.view.InternalResourceViewResolver"&gt;
			&lt;property name="viewClass" value="org.springframework.web.servlet.view.JstlView"/&gt;
			&lt;property name="prefix" value="/WEB-INF/view/"/&gt;
			&lt;property name="suffix" value=".jsp"/&gt;
		&lt;/bean&gt;
		</pre>
	</div>
	
	<h4 id="sub-heading">Remember</h4>
	 <div class="mlist">
		<ol>
			<li>Configure this view resolver in WebApplicationContext</li>
			<li>Place your <strong>JSP</strong> files in a directory under the <strong>'WEB-INF'</strong> directory, so there can be no direct access by clients.</li>
			<li>When using the JSTL you must use a special view class, the JstlView, because JSTL requires some preparation before things such as the I18N features will work</li>
		</ol>
	</div>
	
	<h4 id="sub-heading">Using Spring’s form tag library</h4>
	 <div class="mlist">
		<ol>
			<li>As of <strong>version 2.0,</strong> Spring provides a comprehensive set of data binding-aware tags for handling form elements when using JSP and Spring Web MVC.</li> 
			<li>Each tag provides support for the set of attributes of its corresponding HTML tag.</li>
			<li>The tag-generated HTML is HTML 4.01/XHTML 1.0 compliant.</li>
			<li>The form tags make JSPs easier to develop, read and maintain.</li>
		</ol>
	</div>
	
	
	<h4 id="sub-heading">Configuration of Spring's Form Tags</h4>
	 <div class="mlist">
		<ol>
			<li>The form tag library comes bundled in spring-webmvc.jar. The library descriptor is called spring-form.tld.</li>
			<li>To use the tags from this library, add the following directive to the top of your JSP page:</li>
		</ol>
	</div>
	<div class="program-file">
		<div class="file-name">.jsp</div>
		<pre class="brush: xml">
			&lt;%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %&gt;
		</pre>
	</div>
	<p>Here <strong>form</strong> is the tag name prefix you want to use for the tags from this library.</p>
	
	
<h1>Table of Spring's Form tags</h1>
<div class="table-responsive">	
	<table class="table bordered"> 
		<tr>
			<th><p><b>SNo.</b></p></th>
			<th><p><b>Tags</b></p></th>
			<th><p><b>Description</b></p></th>
		</tr>

		<tr>
			<td><p><b>1</b></p></td>
			<td><p><b>&lt;form:form method="POST" modelAttribute=""&gt;</b></p></td>
			<td><p>Root Element of Spring's form tags</p></td>
		</tr>

		<tr>
			<td><p><b>2</b></p></td> 
			<td><p><b>&lt;form:input path="firstName"/&gt;</b></p></td>
			<td><p>Convert into HTML type="text"</p></td>
		</tr>

		<tr>
			<td><p><b>3</b></p></td>
			<td><p><b>&lt;form:checkbox path="" value=""/&gt;</b></p></td>
			<td><p>Convert into type="checkbox"</p></td>
		</tr>

		<tr>
			<td><p><b>4</b></p></td>
			<td><p><b>&lt;form:checkboxes path="" items="${}"/&gt;</b></p></td>
			<td><p>Multiple Checkboxes</p></td>
		</tr>

		<tr>
			<td><p><b>5</b></p></td>
			<td><p><b>&lt;form:radiobutton path="sex" value="M"/&gt;
					 &lt;form:radiobutton path="sex" value="F"/&gt; </b></p></td>
			<td><p>Type="radio" to create radio button's group</p></td>
		</tr>

		<tr>
			<td><p><b>6</b></p></td>
			<td><p><b>&lt;form:radiobuttons path="sex" items="${}"/&gt;</b></p></td>
			<td><p>Group og radio buttons</p></td>
		</tr>

		<tr>
			<td><p><b>7</b></p></td>
			<td><p><b>&lt;form:password path=" "/&gt;</b></p></td>
			<td><p>Input type is password</p></td>
		</tr>

		<tr>
			<td><p><b>8</b></p></td>
			<td><p><b>&lt;form:select path="skills" items="${skills}"/&gt;

					&lt;select name="skills" multiple="true"&gt;
						&lt;option value=""&gt;J2EE&lt;/option&gt;
						&lt;option value=""&gt;Spring&lt;/option&gt;
						&lt;option value=""&gt;Hibernate&lt;/option&gt;
					&lt;/select&gt;</b></p></td>
			<td><p>Selection list and Multi select list</p></td>
		</tr>

		<tr>
			<td><p><b>9</b></p></td>
			<td><p><b>&lt;form:select path="skill"&gt;
						&lt;form:option value="J2EE"/&gt;
						&lt;form:option value="Spring"/&gt;
						&lt;form:option value="Hibernate"/&gt;
					&lt;/form:select&gt;</b></p></td>
			<td><p>Selection list</p></td>
		</tr>

		<tr>
			<td><p><b>10</b></p></td>
			<td><p><b>&lt;form:options items="${}" itemValue="" itemLabel=""/&gt;</b></p></td>
			<td><p>Sub-element of list</p></td>
		</tr>

		<tr>
			<td><p><b>11</b></p></td>
			<td><p><b>&lt;form:textarea path="notes" rows="3" cols="20"/&gt;</b></p></td>
			<td><p>Type="textarea" no. of rows and colums</p></td>
		</tr>

		<tr>
			<td><p><b>12</b></p></td>
			<td><p><b>&lt;form:errors path="notes"/&gt;</b></p></td>
			<td><p>To display the error message</p></td>
		</tr>

		<tr>
			<td><p><b>13</b></p></td>
			<td><p><b>&lt;form:hidden path=""/&gt;</b></p></td>
			<td><p>Type="hidden" - hidden form field</p></td>
		</tr>
	</table>
</div>


<h1>Spring Form Application</h1>

	 <h4 id="sub-heading">Project Structure</h4><br/>
	<img class="img-responsive" src="../../images/forth-application.png" /><br/>

	<div class="program-file">
		<div class="file-name">pom.xml</div>
		<pre class="brush: xml">
		 &lt;dependencies&gt;
			&lt;dependency&gt;
				&lt;groupId&gt;org.springframework&lt;/groupId&gt;
				&lt;artifactId&gt;spring-webmvc&lt;/artifactId&gt;
				&lt;version&gt;4.1.0.RELEASE&lt;/version&gt;
			&lt;/dependency&gt;
			 &lt;dependency&gt;
				&lt;groupId&gt;commons-logging&lt;/groupId&gt;
				&lt;artifactId&gt;commons-logging&lt;/artifactId&gt;
				&lt;version&gt;1.2&lt;/version&gt;
			&lt;/dependency&gt;
			&lt;dependency&gt;
				&lt;groupId&gt;jstl&lt;/groupId&gt;
				&lt;artifactId&gt;jstl&lt;/artifactId&gt;
				&lt;version&gt;1.2&lt;/version&gt;
			&lt;/dependency&gt;
		  &lt;/dependencies&gt;
		</pre>
	</div>
	
	
	<div class="program-file">
		<div class="file-name">welcome.jsp</div>
		<pre class="brush: xml">
			 &lt;%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %&gt;
				&lt;html&gt;
				&lt;head&gt;
				&lt;title&gt;Spring's Form Tags&lt;/title&gt;
				&lt;/head&gt;
				&lt;body&gt;
					&lt;h1&gt;All Spring's Form Tags&lt;/h1&gt;
					&lt;form:form method="POST" modelAttribute="user" action="register"&gt;
						&lt;table&gt;
							&lt;tr&gt;
								&lt;td&gt;User Name:&lt;/td&gt;
								&lt;td&gt;&lt;form:input path="userName"/&gt;&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Gender:&lt;/td&gt;
								&lt;td&gt;
									&lt;form:radiobutton path="gender" value="Male"/&gt;Male 
									&lt;form:radiobutton path="gender" value="Female"/&gt;Female
								&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Hobbies&lt;/td&gt;
								&lt;td&gt;
									&lt;form:checkbox path="hobbies" value="Coding"/&gt;Coding
									&lt;form:checkbox path="hobbies" value="Designing"/&gt;Designing
									&lt;form:checkbox path="hobbies" value="Testing"/&gt;Testing
								&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Skills:&lt;/td&gt;
								&lt;td&gt;
									&lt;form:select multiple="true" path="skills"&gt;
										&lt;option value="J2EE"&gt;J2EE&lt;/option&gt;
										&lt;option value="Spring"&gt;Spring&lt;/option&gt;
										&lt;option value="Hibernate"&gt;Hibernate&lt;/option&gt;
									&lt;/form:select&gt;
								&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Password:&lt;/td&gt;
								&lt;td&gt;
									&lt;form:password path="password"/&gt;
								&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td&gt;Address:&lt;/td&gt;
								&lt;td&gt;&lt;form:textarea path="address" rows="3" cols="20"/&gt;&lt;/td&gt;
							&lt;/tr&gt;
							&lt;tr&gt;
								&lt;td colspan="2"&gt;
									&lt;input type="submit" value="Submit"&gt;
								&lt;/td&gt;
							&lt;/tr&gt;
						&lt;/table&gt;
					&lt;/form:form&gt;
				&lt;/body&gt;
				&lt;/html&gt;
		</pre>
	</div>

	<div class="program-file">
		<div class="file-name">web.xml</div>
		<pre class="brush: xml">
		&lt;/?xml version="1.0" encoding="UTF-8"?&gt;
		&lt;/web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				 xmlns="http://xmlns.jcp.org/xml/ns/javaee" 
				 xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
				 id="WebApp_ID" version="3.1"&gt;
			  &lt;/display-name&gt;spring-sorm-tags&lt;/display-name&gt;
			  &lt;/welcome-file-list&gt;
				&lt;/welcome-file&gt;/&lt;/welcome-file&gt;
			  &lt;/welcome-file-list&gt;
			  
			  &lt;/servlet&gt;
				&lt;/servlet-name&gt;dispatcher&lt;/servlet-name&gt;
				&lt;/servlet-class&gt;org.springframework.web.servlet.DispatcherServlet&lt;/servlet-class&gt;
				&lt;/load-on-startup&gt;1&lt;/load-on-startup&gt;
			  &lt;/servlet&gt;
			  &lt;/servlet-mapping&gt;
				&lt;/servlet-name&gt;dispatcher&lt;/servlet-name&gt;
				&lt;/url-pattern&gt;/&lt;/url-pattern&gt;
			  &lt;/servlet-mapping&gt;
		&lt;/web-app&gt;
		</pre>
	</div>

	<div class="program-file">
		<div class="file-name">dispatcher-Servlet.xml</div>
		<pre class="brush: xml">
		&lt;beans xmlns="http://www.springframework.org/schema/beans"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns:context="http://www.springframework.org/schema/context"
			xsi:schemaLocation="http://www.springframework.org/schema/beans
				http://www.springframework.org/schema/beans/spring-beans.xsd
				http://www.springframework.org/schema/context
				http://www.springframework.org/schema/context/spring-context.xsd"&gt;
				
			 &lt;context:component-scan base-package="com.codingraja.spring.*" /&gt;
				
			 &lt;bean id="viewResolver" class="org.springframework.web.servlet.view.InternalResourceViewResolver"&gt;
				&lt;property name="prefix" value="/WEB-INF/view/" /&gt;
				&lt;property name="suffix" value=".jsp" /&gt;
			 &lt;/bean&gt;
		&lt;/beans&gt;
		</pre>
	</div>

	<div class="program-file">
		<div class="file-name">User.java</div>
		<pre class="brush: java">
			package com.codingraja.spring.bean;

			import org.springframework.stereotype.Component;

			@Component("user")
			public class User {
				private String userName;
				private String gender;
				private String[] hobbies;
				private String[] skills;
				private String password;
				private String address;
				
				public User() { System.out.println("User Object is created");}
				
				//Getters and Setters
			}

		</pre>
	</div>
	
	<div class="program-file">
		<div class="file-name">UserController.java</div>
		<pre class="brush: java">
			package com.codingraja.spring.controller;

			import org.springframework.stereotype.Controller;
			import org.springframework.ui.Model;
			import org.springframework.web.bind.annotation.ModelAttribute;
			import org.springframework.web.bind.annotation.RequestMapping;
			import org.springframework.web.servlet.ModelAndView;

			import com.codingraja.spring.bean.User;

			@Controller
			public class UserController {
				
				@RequestMapping("/")
				public String userForm(Model model) {
					User user = new User();
					model.addAttribute("user", user);
					return "welcome";
				}
				
				@RequestMapping("/register")
				public ModelAndView registerUser(@ModelAttribute User user) {
					return new ModelAndView("regSuccess","user",user);
				}
			}
		</pre>
	</div>
	
	<div class="program-file">
		<div class="file-name">regSuccess.jsp</div>
		<pre class="brush: xml">
			&lt;%@ page language="java" contentType="text/html; charset=ISO-8859-1"
				pageEncoding="ISO-8859-1" %&gt;
			&lt;%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %&gt;
			&lt;!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"&gt;
			&lt;html&gt;
			&lt;head&gt;
			&lt;title&gt;Show User Details&lt;/title&gt;
			&lt;/head&gt;
			&lt;body&gt;
				&lt;h1&gt;User details&lt;/h1&gt;
				&lt;p&gt;User Name: ${user.userName}&lt;/p&gt;
				&lt;p&gt;Gender: ${user.gender}&lt;/p&gt;
				&lt;p&gt;Hobbies: 
					&lt;c:forEach begin="0" items="${user.hobbies}" var="hob"&gt;
						&lt;c:out value="${hob}" /&gt;
					&lt;/c:forEach&gt;
				&lt;/p&gt;
				&lt;p&gt;Skills: 
					&lt;c:forEach begin="0" items="${user.skills}" var="skill"&gt;
						&lt;c:out value="${skill}" /&gt;
					&lt;/c:forEach&gt;
				&lt;/p&gt;
				&lt;p&gt;Password: ${user.password}&lt;/p&gt;
				&lt;p&gt;Address: ${user.address}&lt;/p&gt;
			&lt;/body&gt;
			&lt;/html&gt;
		</pre>
	</div>
	
	<h4 id="sub-heading">After Register it Redirect to Success.jsp page and Display the Data</h4>
	<img class="img-responsive" src="../../images/output8.png" /><br/>
	
	<h4 id="sub-heading">Getting all the Details of in success.jsp file</h4>
	<img class="img-responsive" src="../../images/output9.png" /><br/>
	


<!--  ########################### Main Content End    #############################  -->
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddBottom.php"; ?>
  </div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/footer.php"; ?>
</div>
</body>
</html>