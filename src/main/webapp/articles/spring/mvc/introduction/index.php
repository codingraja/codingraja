<!DOCTYPE HTML>
<html lang="en-us">
<head>
<title>Introduction to Spring MVC</title>
<!-- Bootstrap -->
<?php include "$_SERVER[DOCUMENT_ROOT]/links.php"; ?>
</head>
<body>

<!--This is Header  -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/header.php"; ?>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/mvc/mvc-menu.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddRight.php"; ?>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddTop.php"; ?>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<!--  ########################### Main Content Start  #############################  -->
	<h1>Introduction To Spring MVC</h1>
	
	<h4 id="sub-heading">Spring MVC</h4>
	<div class="mlist">
		 <ol>
			<li>The Spring Web model-view-controller (MVC) framework is designed around a <strong>DispatcherServlet</strong> that dispatches requests to handlers.</li>
			<li>The default handler is based on the <strong>@Controller</strong> and <strong>@RequestMapping</strong> annotations, offering a wide range of flexible handling methods.</li>
			<li>With the introduction of Spring 3.0, the @Controller mechanism also allows you to create RESTful Web sites and applications, through the @PathVariable annotation and other features.</li>
			<li>Some methods in the core classes of Spring Web MVC are marked final. As a developer you cannot override these methods to supply your own behaviour.</li>
			<li>You cannot add advice to final methods when you use Spring MVC.</li>
		</ol>
	</div>
	
	<h4 id="sub-heading">Features Spring MVC</h4>
	<div class="mlist">
		 <ol>
			<li>Clear separation of roles.</li>
			<li>Powerful and straightforward configuration of both framework and application classes as JavaBeans.</li>
			<li>Define any controller method signature you need, using annotations.</li>
			<li>Customizable binding and validation.</li>
			<li>Reusable business code, no need for duplication.</li>
			<li>Flexible Mode Transfer. Model transfer with a name/value Map supports easy integration with any view-technologies.</li>
			<li>Support for JSPs with or without Spring Tags, support JSTL and Velocity.</li>
			<li>Provide Spring Tag library that support features as data binding and theme.</li>
		</ol>
	</div>


<!--  ########################### Main Content End    #############################  -->
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddBottom.php"; ?>
  </div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/footer.php"; ?>
</div>
</body>
</html>