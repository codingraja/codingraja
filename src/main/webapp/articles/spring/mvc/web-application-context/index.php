<!DOCTYPE HTML>
<html lang="en-us">
<head>
<title>WebApplicationContext in Spring MVC</title>
<!-- Bootstrap -->
<?php include "$_SERVER[DOCUMENT_ROOT]/links.php"; ?>
</head>
<body>

<!--This is Header  -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/header.php"; ?>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/mvc/mvc-menu.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddRight.php"; ?>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddTop.php"; ?>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<!--  ########################### Main Content Start  #############################  -->
	<h1>WebApplicationContext</h1>
   
	<p>The WebApplicationContext is an extension of the plain ApplicationContext that has some extra features necessary for web applications. 
	It differs from a normal ApplicationContext in that it is capable of resolving themes and that it knows which Servlet it is associated with</p>
	
	 <div class="mlist">
		 <ol>
			<li>Apart from ApplicationContext, there can be multiple WebApplicationContext in a single web application.</li>
			<li>Each DispatcherServlet associated with single WebApplicationContext.</li>
			<li><b>[Servlet Name]-servlet.xml </b>file is specific to the DispatcherServlet and a web application can have more than one DispatcherServlet configured to handle the requests.</li>
			<li>But in this case each DispatcherServlet would have a separate [Servlet Name]-servlet.xml configured. But, applicationContext.xml will be common for all the Servlet configuration files.</li>
			<li>Spring will by default load file named <b>“[Servlet name]-servlet.xml” </b>from your webapps WEB-INF folder where [Servlet name] that you passed inside the web.xml.</li>
			<li>If we want to change the name of that file name or change the location, add <b>initi-param </b>with contextConfigLocation as param name</li>

		</ol>
	</div>



<!--  ########################### Main Content End    #############################  -->
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddBottom.php"; ?>
  </div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/footer.php"; ?>
</div>
</body>
</html>