<!DOCTYPE HTML>
<html lang="en-us">
<head>
<title>Spring MVC First Application</title>
<!-- Bootstrap -->
<?php include "$_SERVER[DOCUMENT_ROOT]/links.php"; ?>
</head>
<body>

<!--This is Header  -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/header.php"; ?>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/mvc/mvc-menu.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddRight.php"; ?>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddTop.php"; ?>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<!--  ########################### Main Content Start  #############################  -->

<h1>Spring MVC Form Handling</h1>

	 <h4 id="sub-heading">Project Structure</h4><br/>
	<img class="img-responsive" src="../../images/third-application.png" /><br/>

	<div class="program-file">
		<div class="file-name">pom.xml</div>
		<pre class="brush: xml">
		 &lt;dependencies&gt;
			&lt;dependency&gt;
				&lt;groupId&gt;org.springframework&lt;/groupId&gt;
				&lt;artifactId&gt;spring-webmvc&lt;/artifactId&gt;
				&lt;version&gt;4.1.0.RELEASE&lt;/version&gt;
			&lt;/dependency&gt;
			 &lt;dependency&gt;
				&lt;groupId&gt;commons-logging&lt;/groupId&gt;
				&lt;artifactId&gt;commons-logging&lt;/artifactId&gt;
				&lt;version&gt;1.2&lt;/version&gt;
			&lt;/dependency&gt;
		  &lt;/dependencies&gt;
		</pre>
	</div>
	
	<div class="program-file">
		<div class="file-name">index.jsp</div>
		<pre class="brush: xml">
			 &lt;h1&gt;Spring Form Handling&lt;/h1&gt;
			 &lt;a href="customer/register">Register&lt;/a&gt;
		</pre>
	</div>
	
	<div class="program-file">
		<div class="file-name">register.jsp</div>
		<pre class="brush: xml">
			 &lt;form action="new"&gt;
				&lt;table&gt;
					&lt;tr&gt;
						&lt;td&gt;Name:&lt;/td&gt;
						&lt;td&gt;&lt;input type="text" name="custName"&gt;&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr&gt;
						&lt;td&gt;Email:&lt;/td&gt;
						&lt;td&gt;&lt;input type="text" name="email"&gt;&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr&gt;
						&lt;td&gt;Mobile:&lt;/td&gt;
						&lt;td&gt;&lt;input type="text" name="mobile"&gt;&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr&gt;
						&lt;td&gt;Gender:&lt;/td&gt;
						&lt;td&gt;
							&lt;input type="radio" name="gender" value="male"&gt;Male
							&lt;input type="radio" name="gender" value="female"&gt;Female
						&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr&gt;
						&lt;td&gt;&lt;input type="submit" value="Register"&gt;&lt;/td&gt;
						&lt;td&gt;&lt;/td&gt;
					&lt;/tr&gt;
				&lt;/table&gt;
			&lt;/form&gt;
		</pre>
	</div>

	<div class="program-file">
		<div class="file-name">web.xml</div>
		<pre class="brush: xml">
		&lt;/?xml version="1.0" encoding="UTF-8"?&gt;
		&lt;/web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				 xmlns="http://xmlns.jcp.org/xml/ns/javaee" 
				 xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
				 id="WebApp_ID" version="3.1"&gt;
			  &lt;/display-name&gt;spring-webmvc-form-handling&lt;/display-name&gt;
			  &lt;/welcome-file-list&gt;
				&lt;/welcome-file&gt;/WEB-INF/view/index.jsp&lt;/welcome-file&gt;
			  &lt;/welcome-file-list&gt;
			  
			  &lt;/servlet&gt;
				&lt;/servlet-name&gt;dispatcher&lt;/servlet-name&gt;
				&lt;/servlet-class&gt;org.springframework.web.servlet.DispatcherServlet&lt;/servlet-class&gt;
				&lt;/init-param&gt;
					&lt;/param-name&gt;contextConfigLocation&lt;/param-name&gt;
					&lt;/param-value&gt;/WEB-INF/spring.xml&lt;/param-value&gt;
				&lt;/init-param&gt;
				&lt;/load-on-startup&gt;1&lt;/load-on-startup&gt;
			  &lt;/servlet&gt;
			  &lt;/servlet-mapping&gt;
				&lt;/servlet-name&gt;dispatcher&lt;/servlet-name&gt;
				&lt;/url-pattern&gt;/&lt;/url-pattern&gt;
			  &lt;/servlet-mapping&gt;
		&lt;/web-app&gt;
		</pre>
	</div>

	<div class="program-file">
		<div class="file-name">spring.xml</div>
		<pre class="brush: xml">
		&lt;beans xmlns="http://www.springframework.org/schema/beans"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns:context="http://www.springframework.org/schema/context"
			xsi:schemaLocation="http://www.springframework.org/schema/beans
				http://www.springframework.org/schema/beans/spring-beans.xsd
				http://www.springframework.org/schema/context
				http://www.springframework.org/schema/context/spring-context.xsd"&gt;
				
			 &lt;context:component-scan base-package="com.codingraja.spring.controller" /&gt;
				
			 &lt;bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"&gt;
				&lt;property name="prefix" value="/WEB-INF/view/" /&gt;
				&lt;property name="suffix" value=".jsp" /&gt;
			 &lt;/bean&gt;
			 
			 &lt;bean id="customer" class="com.codingraja.spring.bean.Customer" /&gt;
			 
		&lt;/beans&gt;
		</pre>
	</div>

	<div class="program-file">
		<div class="file-name">Customer.java</div>
		<pre class="brush: java">
		package com.codingraja.spring.bean;

		public class Customer {
			private long custId;
			private String custName;
			private String email;
			private String mobile;
			private String gender;
			
			public Customer() {}
			public long getCustId() {
				return custId;
			}
			// Add corresponding Setters and Getters Methods
		}
		</pre>
	</div>
	
	<div class="program-file">
		<div class="file-name">CustomerController.java</div>
		<pre class="brush: java">
		package com.codingraja.spring.controller;

		import org.springframework.stereotype.Controller;
		import org.springframework.web.bind.annotation.ModelAttribute;
		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.servlet.ModelAndView;

		import com.codingraja.spring.bean.Customer;

		@Controller
		@RequestMapping("/customer")
		public class CustomerController {
			
			@RequestMapping("/register")
			public String regCustomer() {
				return "register";
			}
			
			@RequestMapping("/new")
			public ModelAndView newCustomer(@ModelAttribute Customer cust) {
				return new ModelAndView("success","customer",cust);
			}
		}
		</pre>
	</div>
	
	<div class="program-file">
		<div class="file-name">success.jsp</div>
		<pre class="brush: xml">
			&lt;h1&gt;Customer Details&lt;/h1&gt;
			&lt;h4&gt;Cust ID: ${customer.custId}&lt;/h4&gt;
			&lt;h4&gt;Name: ${customer.custName}&lt;/h4&gt;
			&lt;h4&gt;Email: ${customer.email}&lt;/h4&gt;
			&lt;h4&gt;Mobile: ${customer.mobile}&lt;/h4&gt;
			&lt;h4&gt;Gender: ${customer.gender}&lt;/h4&gt;
			
		</pre>
	</div>

	<h4 id="sub-heading">After Deployment Output and After Click on Hyperlink it will redirect to register.jsp form</h4><br/>
	<img class="img-responsive" src="../../images/output5.png" /><br/>
	
	<h4 id="sub-heading">After Register it Redirect to Success.jsp page and Display the Data</h4>
	<img class="img-responsive" src="../../images/output6.png" /><br/>
	
	<h4 id="sub-heading">Getting all the Details of in success.jsp file</h4>
	<img class="img-responsive" src="../../images/output7.png" /><br/>

<!--  ########################### Main Content End    #############################  -->
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddBottom.php"; ?>
  </div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/footer.php"; ?>
</div>
</body>
</html>