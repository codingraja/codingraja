<!DOCTYPE HTML>
<html lang="en-us">
<head>
<title>Spring MVC Flow</title>
<!-- Bootstrap -->
<?php include "$_SERVER[DOCUMENT_ROOT]/links.php"; ?>
</head>
<body>

<!--This is Header  -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/header.php"; ?>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/mvc/mvc-menu.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddRight.php"; ?>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddTop.php"; ?>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<!--  ########################### Main Content Start  #############################  -->

	<h1>Spring MVC Flow</h1>
   
	<p>To understand the working of Spring MVC Framework see the diagram below</p>
	<img alt="Spring-mvc-flow" src="../../images/spring-mvc-flow.png">
	
	<h4 id="sub-heading">When a request is sent to the server the following sequence of events happen</h4>
	 <div class="mlist">
		 <ol>
			<li>First, the request is received by the DispatcherServlet.</li>
			<li>The DispatcherServlet consults with the HandlerMapping and invokes the Controller associated with the request.</li>
			<li>The Controller processes the request by calling the appropriate methods and returns a ModelAndView object to the DispatcherServlet. The ModeAndView object contains the model data and the view name.</li>
			<li>The DispatcherServlet sends the view name to a ViewResolver to find the actual View and return back to DispatcherServlet.</li>
			<li>Now the DispatcherServlet will send the model object to the View to render the result.</li>
			<li>The View will render the model data and send result back to the user.</li>
		</ol>
	</div>

	<h1>HandlerMapping</h1>
	<h4 id="sub-heading">HandlerMapping ?</h4>
	<div class="mlist">
		 <ol>
			<li>The DispatcherServlet consults with the HandlerMapping and invokes the Controller associated with the request.</li>
			<li>When no handler mapping is explicitly specified in configuation, BeanNameUrlHandlerMapping is created and used by default.</li>

			<li><strong>"By default the DispatcherServlet uses the BeanNameUrlHandlerMapping</strong> to map the incoming request.</li> 
			<li>The BeanNameUrlHandlerMapping uses the bean name as the URL pattern. Since BeanNameUrlHandlerMapping is used by default, 
			you need not do any seperate configuration for this." </li>
		</ol>
	</div>

<!--  ########################### Main Content End    #############################  -->
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddBottom.php"; ?>
  </div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/footer.php"; ?>
</div>
</body>
</html>