<!DOCTYPE HTML>
<html lang="en-us">
<head>
<title>DispatcherServlet in Spring MVC</title>
<!-- Bootstrap -->
<?php include "$_SERVER[DOCUMENT_ROOT]/links.php"; ?>
</head>
<body>

<!--This is Header  -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/header.php"; ?>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/mvc/mvc-menu.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddRight.php"; ?>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddTop.php"; ?>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<!--  ########################### Main Content Start  #############################  -->

	<h1>DispatcherServlet</h1>
	
	<h4 id="sub-heading">DispatcherServlet (Front Controller)</h4>
	<div class="mlist">
		 <ol>
			<li>The DispatcherServlet is an actual Servlet (it inherits from the HttpServlet base class), and as such is declared in the web.xml of your web application.</li>
			<li>You need to map requests that you want the DispatcherServlet to handle, by using a URL mapping in the same web.xml file.</li>
			<li>In Spring MVC all incoming requests go through a single Servlet.</li> 
			<li>This Servlet - DispatcherServlet - is the front controller.</li> 
			<li>Front controller is a typical design pattern in the web applications development. In this case, a single Servlet receives all requests and transfers them to all other components of the application.</li>
		</ol>
	</div>

	<h4 id="sub-heading">The following example shows such a DispatcherServlet declaration and mapping:</h4>
	<div class="program-file">
		<div class="file-name">application.xml</div>
			<pre class="brush: xml">
				&lt;web-app&gt;
					&lt;servlet&gt;
						&lt;servlet-name&gt;example&lt;/servlet-name&gt;
						&lt;servlet-class&gt;org.springframework.web.servlet.DispatcherServlet&lt;/servlet-class&gt;
						&lt;load-on-startup&gt;1&lt;/load-on-startup&gt;
					&lt;/servlet&gt;

					&lt;servlet-mapping&gt;
						&lt;servlet-name&gt;example&lt;/servlet-name&gt;
						&lt;url-pattern>/example/*&lt;/url-pattern&gt;
					&lt;/servlet-mapping&gt;
				&lt;/web-app&gt;
			</pre>
	</div>
	
<!--  ########################### Main Content End    #############################  -->
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="home">&lt;&lt;--Prev</a></li>
		</ul>
	</div>
	<div id="next-top">
		<ul id="MenuBar1" class="MenuBarHorizontal">
			<li><a href="spring-modules">Next--&gt;&gt;</a></li>
		</ul>
	</div>
</div>

<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddBottom.php"; ?>
  </div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/footer.php"; ?>
</div>
</body>
</html>