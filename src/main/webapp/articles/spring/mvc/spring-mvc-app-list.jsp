<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:url value="/articles/spring/mvc/applications" var="springMvcAppUrl" />

<div class="interview">
	<h1 class="interview-question-cat">
		Useful Spring Web MVC Applications
	</h1>
	<ol>
		<li><a href="${springMvcAppUrl}/first-spring-webmvc-app">First Spring Web MVC Application</a></li>
	</ol>
</div>