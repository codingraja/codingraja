<!DOCTYPE HTML>
<html>
<head>
<title>Spring Framework Tutorials</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<%@ include file="/articles/spring/spring-menu.jsp" %>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="#"> &lt;&lt;--Prev </a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="#"> Next--&gt;&gt; </a>
	</div>
</div>
    
<h1>Spring Introduction</h1>
<ul class="listClassUL">
	<li>The Spring Framework is a Java Platform that is used for developing Java applications.</li>
	<li>It is developed by Rod Johnson in 2003. It's initial name was interface21(2002).</li>
	<li>Spring Framework is a product of Pivotal Software, Inc.</li>
	<li>We can develop standalone application , Web application and Enterprise Applications using Spring Framework.</li>
	<li>Spring Framework is an open source and light-weight framework.</li>
	<li>It is a non-invasive framework. Non-invasive means spring framework does not force to implement any interface
		 or extend any class for developing any application.</li>
	<li>Spring Framework is a alternative to EJB.</li>
	<li>It uses POJO classes (Plain Old Java Object).</li>
</ul>

<h1>Features of Spring Framework</h1>
<ul class="listClassUL">
	<li>Dependency Injection and loose coupling</li>
	<li>Aspect-Oriented Programming including Spring's declarative transaction management</li>
	<li>Spring MVC web application and RESTful web service framework</li>
	<li>Foundational support  for JDBC, JPA, JMS</li>
</ul>
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="#"> &lt;&lt;--Prev </a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="#"> Next--&gt;&gt; </a>
	</div>
</div>


<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddBottom.jsp" %>
  </div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>