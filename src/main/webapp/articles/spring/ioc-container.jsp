<!DOCTYPE HTML>
<html>
<head>
<title>Dependency Injection(DI)</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
<link type="text/css" rel="stylesheet" href="${resourceURL}/syntaxhighlighter/styles/shCoreEclipse.css"/>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<%@ include file="/articles/spring/spring-menu.jsp" %>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>
<h1>IOC Container (Inversion of Control)</h1>
<ul class="listClassUL">
	<li>In spring, an object is created, instantiated, assembled, and managed by a Spring IoC container.</li>
	<li>IoC gets the entry from the spring configuration file and works accordingly.</li>
	<li>This Configuration file may be XML or Java file or Annotations.</li>
	<li>IoC Container is also known as Dependency Injection (DI). </li>
	<li>We can say DI is a process where objects define their dependencies.</li>
	<li>The <code>org.springframework.beans</code> and <code>org.springframework.context packages</code> are the basis for Spring IoC container.</li> 
	<li><code>BeanFactory</code> and <code>ApplicationContext</code> interfaces provide the functionality of IoC container.</li>
</ul>

<p>Spring Framework has mainly three IoC containers-</p>
<ol class="listSubTopic">
	<li>BeanFactory</li>
	<li>ApplicationContext</li>
	<li>WebApplicationContext</li>
</ol>

<h1>BeanFactory IoC Container</h1>
<ul class="listClassUL">
	<li>BeanFactory is a root interface of IoC container.</li> 
	<li>The implementation is provided by org.springframework.beans.factory.xml.XmlBeanFactory.</li>
	<li>Normally a BeanFactory will load bean definitions defined in a configuration file 
	(such as an XML file), and use the org.springframework.beans package to configure the beans.</li>
	<li>However, an implementation could simply return Java objects it creates as necessary
	 directly in Java code. 
	<li>There are no constraints on how the definitions could be stored: LDAP, RDBMS, XML,
	 properties file, etc. Implementations are encouraged to support references amongst beans (Dependency Injection).</li>
</ul>

<h1>ApplicationContext IoC Container</h1>

<h1>Dependencies Resolution Process</h1>
<img class="img-responsive" alt="IoC Container" src="${springURL}/images/ioc-container.PNG">


<!-- ***************************END OF CONTENTS********************************** -->	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>
    

<!------------------- User Suggestion--------------------->
<div> 
	<%@ include file="/fragments/googleAddBottom.jsp" %>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>