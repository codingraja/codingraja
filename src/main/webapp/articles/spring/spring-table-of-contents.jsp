<div class="sidebar_title"><h4 class="caps">Table of Contents</h4></div>
<div class="clearfix"></div>
      <div id="st-accordion-four" class="st-accordion-four">
	<ul>
	<%-- 	<li>
			<a href="${resourceUrl}/#">Spring Framework Introduction<span class="st-arrow">Open or Close</span></a>
			<div class="st-content">
				<ol class="arrows_list1">		
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Introduction</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Spring Framework Modules</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Spring Installation</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Inversion of Control (IoC)</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> BeanFactory Vs ApplicationContext</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> First Spring Application</a></li>
				</ol>
			</div>
		</li>
		
		<li>
			<a href="${resourceUrl}/#">Spring Core Container<span class="st-arrow">Open or Close</span></a>
			<div class="st-content">
				<ol class="arrows_list1">		
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Dependency Injection (DI)</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Dependency resolution process</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Primitive and String values</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Null and Empty string values</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Collections(List, Set, Map)</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Properties Object</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Other Dependent Objects</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Autowiring collaborators</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Defining Inner beans</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Defining Compound Property Names</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Lazy-initialized beans</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Spring Bean Scopes</a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Singleton Bean with Prototype-Bean </a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Lifecycle of Spring Bean </a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Annotation-Based Configuration </a></li>
					<li><a href="${resourceUrl}/#"><i class="fa fa-angle-right"></i> Java-Based Container Configuration </a></li>
				</ol>
			</div>
		</li> --%>
		
		<li>
			<a href="${resourceUrl}/#">Spring Web MVC<span class="st-arrow">Open or Close</span></a>
			<div class="st-content">
				<ol class="arrows_list1">		
					<li><a href="${springUrl}/mvc/applications"><i class="fa fa-angle-right"></i>Spring Web MVC Applications</a></li>
				</ol>
			</div>
		</li>
		
	</ul>
</div>



<!-- #####################   Recent Posts   #################### -->
<div class="recent_posts">
	<%@ include file="/fragments/recent-posts.jsp" %>
</div> <!-- #####################  End Recent Posts   #################### -->