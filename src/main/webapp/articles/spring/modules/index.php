<!DOCTYPE HTML>
<html>
<head>
<title>Spring Modules</title>
<!-- Bootstrap -->
<?php include "$_SERVER[DOCUMENT_ROOT]/links.php"; ?>
</head>
<body>

<!--This is Header  -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/header.php"; ?>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/spring-menu-sub.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddRight.php"; ?>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddTop.php"; ?>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="">&lt;&lt--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="">Next--&gt;&gt;</a>
	</div>
</div>

<!-- *****************************START CONTENTS********************************* -->
	
<h1>Spring Modules</h1>
    
<p>The Spring Framework consists about 20 modules. These modules are grouped into six modules. These are -</p>
 <div class="mlist">
 <ol>
 	<b><li>Core Container</li>
	<li>AOP(Aspect Oriented Programming) And Instrumentation</li>
	<li>Messaging</li>
	<li>Data Access/Integration</li>
	<li>Web</li>
	<li>Test</li></b>
</ol>
</div>

<img alt="Spring Modules" src="../images/spring-overview.png">
 
<h4 id="sub-heading">1- Core Container</h4>
<p>The Core Container consists of <b>spring-core</b>, <b>spring-beans</b>, <b>spring-context</b> and <b>spring-expression(Spring Expression Language)</b>.</p>
<div class="mlist">
 <ol>
	<li>The <b>spring-core</b> and <b>spring-beans</b> modules provides the fundamental part of the framework, like <b>IoC Container</b> and <b>Dependency Injection</b> features.</li>
	<li>The <b>spring-context</b> module builds on the top of the <b>spring-core</b> and <b>spring-beans</b> modules. The <b>spring-context</b> module inherits its feature from <b>spring-beans</b> module. The <b>ApplicationContext</b> Interface is the Central point of the <b>spring-context</b> module.</li>
	<li>The <b>spring-expression</b> module provides <b>Expression Language</b> for querying and manipulating object graph at Runtime. It is just like <b>JSP Expression Language</b>. </li>
</ol>
</div>

<h4 id="sub-heading">2- AOP(Aspect Oriented Programming) And Instrumentation</h4>
<div class="mlist">
 <ol>
	<li>The <b>spring-aop</b> module provides an <b>aspect-oriented programming</b> implementation that allowing you to define <b>method interceptors</b> and <b>pointcuts</b> to decouple code that implements functionality that should be separated.</li>
	<li>The separate <b>spring-aspects</b> module provides integration with <b>AspectJ</b>.</li>
	<li>The <b>spring-instrument</b>  module provide class instrumentation support and <b>classloader</b>  implementations to be used in certain Application Server.</li>
</ol>
</div>

<h4>3- Messaging</h4>
<p>The Spring Framework 4 include a <b>spring-messaging</b> module  for developing <b>messaging-based</b> applications. It contains a set of <b>annotations</b> for mapping messages to methods.</p>

<h4>4- Data Access/Integration</h4>
<p>The Data Access/Integration layer consists  JDBC, ORM, OXM, JMS and Transaction modules.</p>
<div class="mlist">
 <ol>
	<li>The <b>spring-jdbc</b> module provides a <b>JDBC-abstraction</b> layer to remove slow JDBC coding and parsing of database-vendors specific error codes.</li>
	<li>The <b>spring-tx</b> module supports programmatic and declarative transaction management.</li>
	<li>The <b>spring-orm</b> module provides a integration layer with different-different <b>ORM Tools</b> like- <b>Hibernate, JPA, JDO</b> etc. </li>
	<li>The <b>spring-oxm</b> module provides an abstraction layer that supports <b>Object/XML</b> Mapping implementations with <b>JAXB, XMLBeans, JiBX, XStream</b> and <b>Castor</b>.</li>
	<li>The <b>spring-jms</b> (Java Messaging Services) module supports for producing and consuming messages. It provides integration with <b>spring-messaging</b> module.</li>
</ol>
</div>


<h4 id="sub-heading">5- Web</h4>
<p>The Spring Web layer consists of <b>spring-web, spring-webmvc, spring-websocket</b> and <b>spring-webmvc-portlet</b>.</p>
<div class="mlist">
 <ol>
	<li>The <b>spring-web</b> module provides basic web-oriented features like- <b>multipart</b> file upload, initialization of IoC container using servlet listeners and web-oriented application context.</li>
	<li>The <b>spring-webmvc</b>(also known as Web-Servlet module) module provides Spring's Web MVC(Model-View-Controller) implementation for web application.</li>
	<li>The <b>spring-webmvc-portlet</b>(also known as Web-Portlet module ) provides MVC implementation to be used in <b>Portlet</b> environment.</li>
</ol>
</div>

<h4 id="sub-heading">6- Test</h4> 
<p>The <b>spring-test</b> module supports <b>Unit Testing</b> and <b>Integration Testing</b> of the spring components with <b>JUnit</b> and <b>TestNG</b>. </p>
        

<!-- ***************************END OF CONTENTS********************************** -->	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="">&lt;&lt--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="">Next--&gt;&gt;</a>
	</div>
</div>
    

<!------------------- User Suggestion--------------------->
<div> 
	<?php include "$_SERVER[DOCUMENT_ROOT]/googleAddBottom.php"; ?>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<?php include "$_SERVER[DOCUMENT_ROOT]/footer.php"; ?>
</div>
</body>
</html>