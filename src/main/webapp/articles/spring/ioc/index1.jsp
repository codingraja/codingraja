<!DOCTYPE HTML>
<html>
<head>
<title>IOC Container</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<?php include "$_SERVER[DOCUMENT_ROOT]/articles/spring/ioc/ioc-menu.php"; ?>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<!-- *****************************START CONTENTS********************************* -->
	    
<fieldset class="main-topis">
	<legend>Spring Core Applications</legend>
	<ul>
		<li>Introduction to IoC Container</li>
		<li>BeanFactory IoC Container</li>
		<li>ApplicationContext IoC Container</li>
		<li>Introduction to Dependency Injection(DI)</li>
		<li>Constructor-based dependency injection</li>
		<li>Setter-based dependency injection</li>
		<li>Dependency resolution process</li>
		<li>Straight values (primitives, Strings)</li>
		<li>Null and empty string values</li>
		<li>Collections(List, Set, Map) and Properties</li>
		<li>References to other beans (collaborators)</li>
		<li>Inner beans</li>
	 	<li>Compound property names</li>
	 	<li>Using depends-on</li>
	 	<li>Lazy-initialized beans</li>
 		<li>Autowiring collaborators</li>
	 	<li>Bean scopes</li>
	 	<li>Annotation-based container configuration</li> 
	 	<li>Java-based container configuration</li>
	</ul>
</fieldset>

    

<!-- ***************************END OF CONTENTS********************************** -->	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>
    

<!------------------- User Suggestion--------------------->
<div> 
	<%@ include file="/fragments/googleAddBottom.jsp" %>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>