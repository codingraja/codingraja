<!DOCTYPE HTML>
<html>
<head>
<title>Injecting null and empty String Values</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
<link type="text/css" rel="stylesheet" href="${resourceURL}/syntaxhighlighter/styles/shCoreEclipse.css"/>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<%@ include file="/articles/spring/spring-menu.jsp" %>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<h1>Injecting null and empty String Values</h1>
    
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<div> 
	<%@ include file="/fragments/googleAddBottom.jsp" %>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>