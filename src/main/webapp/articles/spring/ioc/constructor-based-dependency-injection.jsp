<!DOCTYPE HTML>
<html>
<head>
<title>Constructor Based Dependency Injection(DI)</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
<link type="text/css" rel="stylesheet" href="${resourceURL}/syntaxhighlighter/styles/shCoreEclipse.css"/>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<%@ include file="/articles/spring/spring-menu.jsp" %>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<h1>Constructor-based Dependency Injection(DI)</h1>


<h1>Creating Application</h1>

<h3>Final Project Structure</h3>
<img class="img-responsive" alt="Constructor Based DI" src="${springURL}/images/constructor-based-di.jpg">

<h3><span>Step-I:</span> Create New Maven Project</h3>
<p>First of all, open your Eclipse and create New Maven Project. Click on Next than select archetype <strong>Maven-archetype-quickstart 1.1</strong></p>
<img class="img-responsive" alt="Create New Maven Project" src="${springURL}/images/constructor-based-di1.jpg">
<p>Enter <strong>Group ID: </strong> com.codingraja.spring</p>
<p>Enter <strong>Artifact ID: </strong> constructor-based-di</p>

<h3><span>Step-II:</span> Add Dependencies in <span>pom.xml</span></h3>
<div class="program-file">
<div class="file-name">pom.xml</div>
<pre class="brush: xml">
&lt;project xmlns="http://maven.apache.org/POM/4.0.0" 
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
  		 http://maven.apache.org/xsd/maven-4.0.0.xsd"&gt;
  &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;

  &lt;groupId&gt;com.codingraja.spring&lt;/groupId&gt;
  &lt;artifactId&gt;constructor-based-dependency-injection&lt;/artifactId&gt;
  &lt;version&gt;0.0.1-SNAPSHOT&lt;/version&gt;
  &lt;packaging&gt;jar&lt;/packaging&gt;

  &lt;name&gt;constructor-based-dependency-injection&lt;/name&gt;
  &lt;url&gt;http://maven.apache.org&lt;/url&gt;

  &lt;properties&gt;
    &lt;project.build.sourceEncoding&gt;UTF-8&lt;/project.build.sourceEncoding&gt;
  &lt;/properties&gt;

  &lt;dependencies&gt;
    &lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-beans&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
  &lt;/dependencies&gt;
&lt;/project&gt;
</pre>
</div>

<h3><span>Step-III:</span> Create JavaBean Class</h3>
<div class="program-file">
<div class="file-name">Customer.java</div>
<pre class="brush: java">
package com.codingraja.spring.domain;

public class Customer {
	private long id;
	private String name;
	private String email;
	private long mobile;
	
	public Customer(long id, String name, String email, long mobile) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return "ID: "+id
				+"\nName: "+name
				+"\nEmail: "+email
				+"\nMobile: "+mobile;
	}
}
</pre>
</div>

<h3><span>Step-IV:</span> Create Spring Configuration File <span>application.xml</span></h3>
<div class="program-file">
<div class="file-name">application.xml</div>
<pre class="brush: xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;

&lt;!DOCTYPE beans PUBLIC "-//SPRING//DTD BEAN 2.0//EN"
			"http://www.springframework.org/dtd/spring-beans-2.0.dtd"&gt;
			
&lt;beans&gt;
	&lt;bean id="customer" class="com.codingraja.spring.domain.Customer"&gt;
		&lt;constructor-arg value="1001" /&gt;
		&lt;constructor-arg value="CodingRAJA" /&gt;
		&lt;constructor-arg value="info@codingraja.com" /&gt;
		&lt;constructor-arg value="9742900696" /&gt;
	&lt;/bean&gt;
&lt;/beans&gt;
</pre>
</div>

<h3><span>Step-V:</span> Create Test Class</h3>
<div class="program-file">
<div class="file-name">TestCustomer.java</div>
<pre class="brush: java">
package com.codingraja.spring.test;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.codingraja.spring.domain.Customer;

public class TestCustomer {

	public static void main(String[] args) {
		//Load the application.xml file using Resource
		Resource resource = new ClassPathResource("application.xml");
		
		//Now pass the resource to BeanFactory IoC Container
		BeanFactory factory = new XmlBeanFactory(resource);
		
		//Getting bean object created by IoC Container
		//Using Bean ID (Non-Generic getBean() method)
		Customer customer1 = (Customer)factory.getBean("customer");
		
		//Using Class Type (Generic getBean() method)
		Customer customer2 = factory.getBean(Customer.class);
		
		System.out.println(customer1);
	}

}
</pre>
</div>

<h3><span>Step-VI:</span> Run Project</h3>
<div class="program-file">
<div class="file-name">Output</div>
<pre class="brush: java">
Mar 29, 2017 2:09:22 PM org.springframework.beans.factory.xml.XmlBeanDefinitionReader 
loadBeanDefinitions
INFO: Loading XML bean definitions from class path resource [application.xml]
ID: 1001
Name: CodingRAJA
Email: info@codingraja.com
Mobile: 9742900696
</pre>
</div>
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<div> 
	<%@ include file="/fragments/googleAddBottom.jsp" %>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>