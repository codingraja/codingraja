<!DOCTYPE HTML>
<html>
<head>
<title>Dependency Injection(DI)</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
<link type="text/css" rel="stylesheet" href="${resourceURL}/syntaxhighlighter/styles/shCoreEclipse.css"/>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<%@ include file="/articles/spring/spring-menu.jsp" %>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<h1>Introduction to Dependency Injection (DI)</h1>
<ol class="listClassOL">
	<li>Dependency injection (DI) is a process whereby objects define their dependencies.</li>
	<li>These objects are constructed by IoC container  or returned by factory methods.</li>
	<li>The container then injects those dependencies when it creates the bean.</li>
	<li>This process is fundamentally the inverse, hence the name Inversion of Control (IoC).</li>
</ol>

<p>There are two ways to define the Dependency injection (DI)-</p>
<ol class="listSubTopic">
	<li>Constructor-based dependency injection</li>
	<li>Setter-based dependency injection.</li>
</ol>

<h1>Constructor-based Dependency Injection</h1>
<ol class="listClassOL">
	<li>Constructor-based DI is accomplished by the container by invoking a constructor with a number of arguments.</li>
	<li>Constructor-based DI does not allow partial DI, passing values and no. of parameters must be same.</li>
	<li>It does not support to inject the dependencies again( can't re-inject).</li>
	<li>Parameters type order and arguments type order must be same.</li>
	<li>If both orders are different then, you can use index value (start from 0 to no. of parameters).</li>
</ol>

<h1>Setter-based Dependency Injection</h1>
<ol class="listClassOL">
	<li>Setter-based DI is accomplished by the container by calling setter methods, after invoking .
		a no-argument constructor or no-argument static factory method to instantiate your bean.</li>
	<li>It also supports partial dependencies Injection.</li>
	<li>It supports setter-based DI after some dependencies have already been injected through the setter approach.</li>
	<li>It supports setter-based DI after some dependencies have already been injected through the constructor approach.</li>
	<li><code>@Required</code> annotation on a setter method can be used to make the property a required dependency.</li>
</ol>

<fieldset class="note">
	<legend>Note:</legend>
	<p>you can mix constructor-based and setter-based DI, it is a good rule of thumb to  use 
		constructors for mandatory dependencies and setter methods or configuration methods for 
		optional dependencies.</p>
</fieldset>


<h1>Dependency resolution process</h1>
<p>The IoC container performs bean dependency resolution as follows:</p>
<ol class="listClassOL">
	<li>First, the ApplicationContext is created and initialized with configuration metadata that 
		describes all the beans. </li>
	<li>Configuration metadata can be specified via XML, annotations, or Java code.</li>
	<li>All the dependencies are expressed in the form of properties and constructor arguments.</li>
	<li>Each property or constructor argument is an actual definition of the value to set, 
		or a reference to another bean in the container.</li>
	<li>Each property or constructor argument which is a value is converted from its specified 
  format to the actual type of that property or constructor argument. </li>
	<li>By default Spring can convert a value supplied in string format to all built-in types, such as int, 
		long, String, boolean, etc.</li>
</ol>
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<div> 
	<%@ include file="/fragments/googleAddBottom.jsp" %>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>