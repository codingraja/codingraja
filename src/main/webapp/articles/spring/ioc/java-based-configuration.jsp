<!DOCTYPE HTML>
<html>
<head>
<title>Spring Java-Based Configuration</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
<link type="text/css" rel="stylesheet" href="http://www.codingraja.com/syntaxhighlighter/styles/shCoreEclipse.css"/>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<%@ include file="/articles/spring/spring-menu.jsp" %>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<h1>Spring Java Based Configuration</h1>
<p>Java configuration typically uses <b>@Bean</b> annotated methods within a 
<b>@Configuration</b> class.</p> 
<p>The <b>@Bean</b> annotation is used to indicate that a method instantiates, 
configures and initializes a new object to be managed by the Spring IoC container.</p>

<div class="program-file">
<div class="file-name">JavaConfiguration.java</div>
<pre class="brush: java;">
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.codingraja.model.Employee;

@Configuration
public class JavaConfiguration {
	
	@Bean
	public Employee employee()
	{
		return new Employee();
	} 
}
</pre>
</div>

<h4>Equivalent XML-Based Configuration</h4>
<div class="program-file">
<div class="file-name">applicationContext.xml</div>
<pre class="brush: xml;">
&lt;beans&gt;
	&lt;bean id="employee" class="com.codingraja.model.Employee"&gt;
	&lt;/bean&gt;
&lt;/beans&gt;
</pre>
</div>

<h1>Java-Based Configuration Example</h1>

<h4>Required Files</h4>
<div class="mlist">
	<ol>
		<li>Employee.java</li>
		<li>JavaConfiguration.java</li>
		<li>TestEmployee.java</li>
	</ol>
</div>

<div class="program-file">
<div class="file-name">Employee.java</div>
<pre class="brush: java;">
package com.codingraja.beans;

public class Employee {

	private int empId;
	private String empName;
	private float salary;
	
	//Getter and Setter Methods
	
	public String toString()
	{
		return "ID:"+empId+" Name:"+empName+" Salary:"+salary;
	}
}
</pre>
</div>

<div class="program-file">
<div class="file-name">JavaConfiguration.java</div>
<pre class="brush: java;">
package com.codingraja.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.codingraja.beans.Employee;

@Configuration
public class JavaConfiguration {
	
	@Bean
	public Employee employee()
	{
		return new Employee();
	}

}
</pre>
</div>

<div class="program-file">
<div class="file-name">TestEmployee.java</div>
<pre class="brush: java;">
package com.codingraja.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.codingraja.config.JavaConfiguration;
import com.codingraja.beans.Employee;

public class TestEmployee {
	
	public static void main(String[] args)
	{
		ApplicationContext ctx = new AnnotationConfigApplicationContext(JavaConfiguration.class);
		Employee emp = (Employee)ctx.getBean("employee");
		emp.setEmpId(1001);
		emp.setEmpName("CL Verma");
		emp.setSalary(55000);
		
		System.out.println(emp);
	}

}
</pre>
</div>
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<div> 
	<%@ include file="/fragments/googleAddBottom.jsp" %>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>