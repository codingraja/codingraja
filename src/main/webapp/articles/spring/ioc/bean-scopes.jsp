<!DOCTYPE HTML>
<html>
<head>
<title>Spring Bean Scopes</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<%@ include file="/articles/spring/spring-menu.jsp" %>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<h1>Spring Bean Scope</h1>
    
<div class="table-responsive">
	<table class="table bordered">
		<tr>
			<th><p>Sn.</p></th>
			<th><p>Scope</p></th>
			<th><p>Description</p></th>
		</tr>
		<tr>
			<td><p><b>1</b></p></td>
			<td><p><b>singleton</b></p></td>
			<td><p>Singleton is a default Scope of Spring Bean. It creates a single bean definition to a single object instance per Spring IoC container.</p></td>
		</tr>
		<tr>
			<td><p><b>2</b></p></td>
			<td><p><b>prototype</b></p></td>
			<td><p>Scopes a single bean definition to any number of object instances.</p></td>
		</tr>
		<tr>
			<td><p><b>3</b></p></td>
			<td><p><b>request</b></p></td>
			<td><p>Scopes a single bean definition to the lifecycle of a single HTTP request; that is, each HTTP request has its own instance of a bean created off the back of a single bean definition. Only valid in the context of a web-aware Spring ApplicationContext.</p></td>
		</tr>
		<tr>
			<td><p><b>4</b></p></td>
			<td><p><b>session</b></p></td>
			<td><p>Scopes a single bean definition to the lifecycle of an HTTP Session. Only valid in the context of a web-aware Spring ApplicationContext.</p></td>
		</tr>
		<tr>
			<td><p><b>5</b></p></td>
			<td><p><b>application</b></p></td>
			<td><p>Scopes a single bean definition to the lifecycle of a ServletContext. Only valid in the context of a web-aware Spring ApplicationContext.</p></td>
		</tr>
		<tr>
			<td><p><b>6</b></p></td>
			<td><p><b>globalSession</b></p></td>
			<td><p>Scopes a single bean definition to the lifecycle of a global HTTP Session. Typically only valid when used in a portlet context. Only valid in the context of a web-aware Spring ApplicationContext.</p></td>
		</tr>
	</table>
</div>   

<h1>Singleton and prototype Scope Example</h1>
<p></p>

<h4>Required Files</h4>
<div class="mlist">
	<ol>
		<li>Employee.java</li>
		<li>Address.java</li>
		<li>SpringCoreTest.java</li>
		<li>applicationContext.xml</li>
	</ol>
</div>

<h4>Required Spring Jar's</h4>
<div class="img-exam"><img src="jsp/spring/ioc/images/spring-core-lib.png" alt="Spring Core Lib" /></div>

<span class="hl-filename">Employee.java</span>
<pre class="brush: java;">
package com.javacoderz.bean;

public class Employee {
	
	private String empId;
	private String empName;
	private float salary;
	private String dept;
	
	//setter and getter methods

	public String toString()
	{
		return "Employee ID: "+empId+"\n"+
				"Employe Name: "+empName+"\n"+
				"Salary: "+salary+"\n"+
				"Dept.: "+dept;
	}
}
</pre>

<span class="hl-filename">Address.java</span>
<pre class="brush: java;">
package com.javacoderz.bean;

public class Address {
	
	private String street;
	private String city;
	private String state;
	private int pin;
	
	//setter and getter methods
	
	public String toString() {
		
		return "Street: "+street+"\n"+
				"City: "+city+"\n"+
				"State: "+state+"\n"+
				"Pin: "+pin;
	}
}
</pre>

<span class="hl-filename">applicationContext.xml</span>
<pre class="brush: xml;">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;

&lt;beans
	xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:p="http://www.springframework.org/schema/p"
	xsi:schemaLocation="http://www.springframework.org/schema/beans 
	http://www.springframework.org/schema/beans/spring-beans-3.1.xsd"&gt;
	
	&lt;bean id="employee" class="com.javacoderz.bean.Employee"&gt;&lt;/bean&gt;
	
	&lt;bean id="address" class="com.javacoderz.bean.Address" scope="prototype"&gt;&lt;/bean&gt;
&lt;/beans&gt;
</pre>

<span class="hl-filename">SpringCoreTest.java</span>
<pre class="brush: java;">
package com.javacoderz.bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringCoreTest {

	public static void main(String[] args) {

		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//Here emp is a Singleton Object
		Employee emp = (Employee)ctx.getBean("employee");
		emp.setEmpId("JC1001");
		emp.setEmpName("CL Verma");
		emp.setSalary(55000);
		emp.setDept("IT");	
		System.out.println(emp+"\n");
		
		Employee emp1 = (Employee)ctx.getBean("employee");
		System.out.println(emp1+"\n");
		
		//Here add is a Prototype Object
		Address add = (Address)ctx.getBean("address");
		add.setStreet("Nagla Bhim");
		add.setCity("Kasganj");
		add.setState("UP");
		add.setPin(207123);
		System.out.println(add+"\n");
		
		Address add1 = (Address)ctx.getBean("address");
		System.out.println(add1);
	}
}
</pre>

<h4>Output</h4>
<pre class="output">
Employee ID: JC1001
Employe Name: CL Verma
Salary: 55000.0
Dept.: IT

Employee ID: JC1001
Employe Name: CL Verma
Salary: 55000.0
Dept.: IT

Street: Nagla Bhim
City: Kasganj
State: UP
Pin: 207123

Street: null
City: null
State: null
Pin: 0
</pre>

	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>


<!------------------- User Suggestion--------------------->
<div> 
	<%@ include file="/fragments/googleAddBottom.jsp" %>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>