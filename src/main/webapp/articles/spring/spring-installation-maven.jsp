<!DOCTYPE HTML>
<html>
<head>
<title>Spring Installation- Maven Dependencies</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
<link type="text/css" rel="stylesheet" href="${resourceURL}/syntaxhighlighter/styles/shCoreEclipse.css"/>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<%@ include file="/articles/spring/spring-menu.jsp" %>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>
<h1>Spring Installation</h1>
<p>To Use Spring Framework, there are two ways- </p>
<ol>
	<li>Download all spring jars and add these jars in your project using build path.</li>
	<li>You can use any project management tool (Maven, Gradle, Ivy, etc.)</li>
 </ol>
 
 <h1>Maven</h1>
 <p>Maven is a project management and comprehension tool that can manage whole project in a single 
 file that is pom.xml (project object model). To know more <a href="#">Click Here</a></p>
<p>It defines the source directory, target directory, properties and project dependencies.</p>

<fieldset class="note">
	<legend>Note:</legend>
	<p>If you are using Maven first time than internet connection will be required. So when will add
	 the dependencies, maven will download all <strong>jar</strong> from the remote repositories and 
	 store these jars in local repositories.</p>
	 <p>You can check these in <strong>c:/users/user-name/.m2/repositories/</strong></p>
</fieldset>

<h1>Spring Framework Maven Dependencies</h1>
<p>To add dependency in <strong>pom.xml</strong> file, you must know three things- <strong>groupId</strong>, 
<strong>artifactId</strong> and <strong>version</strong>. You can add Spring, Hibernate etc. dependencies-</p>

<p><strong>groupId</strong>- org.springframework (common for all spring modules)</p>
<p><strong>artifactId</strong>- spring-core, spring-beans, spring-context, spring-context etc.</p>
<p><strong>version</strong>- 4.3.5.RELEASE (common for all spring modules)</p>

<div class="program-file">
<div class="file-name">pom.xml</div>
<pre class="brush: xml">
&lt;project xmlns="http://maven.apache.org/POM/4.0.0" 
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 
				http://maven.apache.org/xsd/maven-4.0.0.xsd"&gt;
  &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;

  &lt;groupId&gt;com.codigraja.spring&lt;/groupId&gt;
  &lt;artifactId&gt;application-name&lt;/artifactId&gt;
  &lt;version&gt;0.0.1-SNAPSHOT&lt;/version&gt;
  &lt;packaging&gt;jar&lt;/packaging&gt;

  &lt;name&gt;application-name&lt;/name&gt;
  &lt;url&gt;http://maven.apache.org&lt;/url&gt;

  &lt;properties&gt;
    &lt;project.build.sourceEncoding&gt;UTF-8&lt;/project.build.sourceEncoding&gt;
  &lt;/properties&gt;

  &lt;dependencies&gt;
	
	&lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-core&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
	
	&lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-beans&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
  
    &lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-context&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
	
	&lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-aop&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
	
	&lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-expression&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
	
	&lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-web&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
	
	&lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-webmvc&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
	
	&lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-orm&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
	
	&lt;dependency&gt;
      &lt;groupId&gt;org.springframework&lt;/groupId&gt;
      &lt;artifactId&gt;spring-tx&lt;/artifactId&gt;
      &lt;version&gt;4.3.5.RELEASE&lt;/version&gt;
    &lt;/dependency&gt;
	
	&lt;!-- Add some other Dependencies --&gt;
	
  &lt;/dependencies&gt;
&lt;/project&gt;
</pre>
</div> 


<!-- ***************************END OF CONTENTS********************************** -->	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>
    

<!------------------- User Suggestion--------------------->
<div> 
	<%@ include file="/fragments/googleAddBottom.jsp" %>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>