<!DOCTYPE HTML>
<html>
<head>
<title>BeanFactory IoC Container</title>
<!-- Bootstrap -->
<%@ include file="/fragments/links.jsp" %>
<link type="text/css" rel="stylesheet" href="${resourceURL}/syntaxhighlighter/styles/shCoreEclipse.css"/>
</head>
<body>

<!--This is Header  -->
	<%@ include file="/fragments/header.jsp" %>
<!--End of the Header   -->


<!--Starting of Middle Contents  -->
<div class="thrColElsHdr">
<div id="container">

<div id="sidebar1">
<div>
	<%@ include file="/articles/spring/spring-menu.jsp" %>
</div>
	  
	<div> <!--paste add code Here-->
	 </div>    
</div>


<div id="sidebar2">
	<div>
		<%@ include file="/fragments/googleAddRight.jsp" %>
	</div>

<!-- end #sidebar2 --></div>

<!--This is main Content put your code here--> 
<div id="mainContent">
<div>
		<!--paste add code Here--> 
		<%@ include file="/fragments/googleAddTop.jsp" %>
  </div>

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<h1>BeanFactory</h1>
    
<p>BeanFactory is a root interface of IoC container. The implementation is provided 
by <b>org.springframework.beans.factory.xml.XmlBeanFactory.</b></p>

<p>Normally a BeanFactory will load bean definitions defined in a configuration
 file (such as an XML file), and use the <code>org.springframework.beans</code>
  package to configure the beans. However, an implementation could simply return
  Java objects it creates as necessary directly in Java code. There are no
  constraints on how the definitions could be stored: LDAP, RDBMS, XML,
  properties file, etc. Implementations are encouraged to support references
  amongst beans (Dependency Injection).</p>
  
<img class="img-responsive" src="${springURL}/images/bean-factory1.png" />

<img class="img-responsive" src="${springURL}/images/bean-factory2.png" />

<img class="img-responsive" src="${springURL}/images/bean-factory3.png" />

<img class="img-responsive" src="${springURL}/images/bean-factory4.png" />

<img class="img-responsive" src="${springURL}/images/bean-factory5.png" />

<img class="img-responsive" src="${springURL}/images/bean-factory6.png" />

<div class="program-file">
<div class="file-name">Employee.java</div>
<pre class="brush: java">
package com.codingraja.spring.beans;

public class Employee {
	
	private int empId;
	private String empName;
	private String email;
	private float salary;
	
	public Employee(){}
		
	public Employee(int empId, String empName, String email, float salary) {
		this.empId = empId;
		this.empName = empName;
		this.email = email;
		this.salary = salary;
	}
	
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
}
</pre>
</div>

<div class="program-file">
<div class="file-name">application.xml</div>
<pre class="brush: xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd"&gt;

    &lt;bean id="employee" class="com.codingraja.spring.beans.Employee"&gt;
    	&lt;property name="empId" value="1001" /&gt;
    	&lt;property name="empName" value="CL Verma" /&gt;
    	&lt;property name="email" value="info@codingraja.com" /&gt;
    	&lt;property name="salary" value="95000.00" /&gt;
    &lt;/bean&gt;

&lt;/beans&gt;
</pre>
</div>

<div class="program-file">
<div class="file-name">TestEmployee.java</div>
<pre class="brush: java">
package com.codingraja.spring.test;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.codingraja.spring.beans.Employee;

public class TestEmployee {

	public static void main(String[] args) {
		
		Resource resource = 
		new ClassPathResource("com/codingraja/spring/config/application.xml");
		BeanFactory factory = new XmlBeanFactory(resource);
		
		Employee emp = (Employee)factory.getBean("employee");
		
		System.out.println("Employe ID: "+emp.getEmpId());
		System.out.println("Employe Name: "+emp.getEmpName());
		System.out.println("Employe Email: "+emp.getEmail());
		System.out.println("Employe Salary: "+emp.getSalary());
	}
}
</pre>
</div>
	
<hr/>	

<div id="pre-next">
	<div id="pre-top">
		<a class="btn btn-danger" href="" >&lt;&lt;--Prev</a>
	</div>
	<div id="next-top">
		<a class="btn btn-danger" href="" >Next--&gt;&gt;</a>
	</div>
</div>

<div> 
	<%@ include file="/fragments/googleAddBottom.jsp" %>
</div>

<!-- end #mainContent --></div>


<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />

<!-- end #container --></div>
</div>


<div class="footer_bg"><!-- start footer -->
	<%@ include file="/fragments/footer.jsp" %>
</div>
</body>
</html>