<div class="sidebar_title"><h4 class="caps">Table of Contents</h4></div>
<div class="clearfix"></div>
<div id="st-accordion-four" class="st-accordion-four">
	<ul>
		<li>
			<a href="${logicalCodingUrl}/#">Logical Coding<span class="st-arrow">Open or Close</span></a>
			<div class="st-content">
				<ol class="arrows_list1">	
					<li><a href="${logicalCodingUrl}/number-pattern"><i class="fa fa-angle-right"></i> Number Pattern</a></li>
					<li><a href="${logicalCodingUrl}/star-pattern"><i class="fa fa-angle-right"></i> Star Pattern</a></li>
					<li><a href="${logicalCodingUrl}/alphabet-pattern"><i class="fa fa-angle-right"></i> Alphabet Pattern</a></li>
					<li><a href="${logicalCodingUrl}/array"><i class="fa fa-angle-right"></i> Array Manipulation</a></li>
				</ol>
			</div>
		</li>
		
	</ul>
</div>



<!-- #####################   Recent Posts   #################### -->
<div class="recent_posts">
	<%@ include file="/fragments/recent-posts.jsp" %>
</div> <!-- #####################  End Recent Posts   #################### -->