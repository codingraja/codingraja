<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Array Logical Coding in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/logical-coding/logical-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Array Logical Coding</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="interview">
					<h1 class="interview-question-cat">
						All Possible Programs in Array
					</h1>
					<ol>
						<li><a href="find-max-element-in-array">Program to find Max element in an Array.</a></li>
					    <li><a href="find-min-element-in-array">Program to find Min element in an Array.</a></li>
						
					    <li><a href="find-second-max-element-in-array">Program to find Second Max element in an Array.</a></li>
					    <li><a href="find-second-min-element-in-array">Program to find Second Min element in an Array.</a></li>	
						
					    <li><a href="search-an-element-in-array">Program to search an element in an Array.</a></li>
					    <li><a href="addition-of-array-elements">Program to find Addition of elements of an Array.</a></li>
						
						<li><a href="addition-left-and-right-half-elements">Program to add first half and second half elements in Array.</a></li>
						<li><a href="addition-of-odd-and-even-indexes">Program to add Odd index and Even index elements in Array.</a></li>
						
						<li><a href="reverse-an-array">Program to Reverse an Array.</a></li>
						<li><a href="add-element-at-specified-location-in-array">Program to add an Element in Array at specified location.</a></li>
						<li><a href="delete-element-from-specified-location-in-array">Program to Delete an Element from an Array.</a></li>
						<li><a href="remove-duplicate-elements-from-array">Program to remove duplicate elements from Array.</a></li>
						
						
					<!--	<li><a href="program-array-left-shift-one-element">Program to shift left one element in Array.</a></li>
						<li><a href="program-array-left-shift-two-element">Program to shift left two element in Array.</a></li>
						<li><a href="program-array-right-shift-one-element">Program to shift right one element in Array.</a></li>
						<li><a href="program-array-right-shift-two-element">Program to shift right two element in Array.</a></li>
					-->	
					</ol>
				</div>
				
				<div class="interview">
					<h1 class="interview-question-cat">
						Programming on Matrix
					</h1>
					<ol>
						<li><a href="addition-of-two-matrix">Program to find Addition of two Matrix.</a></li>
					    <li><a href="subtraction-of-two-matrix">Program to find Subtraction of two Matrix.</a></li>
					    <li><a href="multiplication-of-two-matrix">Program to find Multiplication of two Matrix.</a></li>
					    <li><a href="transpose-of-matrix">Program to find Transpose of a Matrix.</a></li>
					</ol>
				</div>
			
		    </div>
		</div>
		        
		<!-- <div class="blog_note">
			<h3>Note:</h3>
		  	<p>
		  		All the basics and advanced applications in this tutorial are created 
		  		as a <em>Maven Project</em> and used <em>webapp-javaee7</em> 
		  		<strong>archetype</strong>. If you don't know, how to create <em>Maven</em> 
		  		project than first read this article- 
		  		<a href="#">Creating Maven Project</a>
		  	</p>
		</div> -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        
		        <%@ include file="../logical-program-list.jsp" %>
			
		    </div>
		</div><!-- /# end post -->
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
