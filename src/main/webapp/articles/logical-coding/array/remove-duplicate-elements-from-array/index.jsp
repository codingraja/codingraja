<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Program to remove duplicate elements from Array.</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/logical-coding/logical-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Program to remove duplicate elements from Array.</h1>
				<div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">ArrayRemoveDuplicate.java</h2>
			<pre class="brush: java;">
			import java.util.Scanner;

			public class ArrayRemoveDuplicate {

				public static void main(String args[]) {
					
					int num;
			        Scanner s = new Scanner(System.in);
			        System.out.print("Enter number of elements in the array:");
			        num = s.nextInt();
			        int array[] = new int[num+1];
			        
			        
			        System.out.println("Enter elements of array:");
			        for(int i = 0; i &lt; num; i++)
			            array[i] = s.nextInt();
					
			        for(int i=0; i &lt; num; i++)
			        {
			            for(int j=i+1; j &lt; num; j++)
			            {
			                if(array[i] == array[j])
			                {
			                    for(int k=j; k &lt; num; k++)
			                        array[k] = array[k+1];
			                        
			                    num -= 1;
			                    j--;
			                }
			            }
			        }
			     
			        System.out.println("Array with unique Elements : ");
			        for(int i=0 ; i &lt; num; i++)
			            System.out.print(array[i] + " ");
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Command Prompt</h2>
			<pre class="brush: xml;">
				E:\src>javac ArrayRemoveDuplicate.java
				E:\src>java ArrayRemoveDuplicate
				
				Enter number of elements in the array:7
				Enter elements of array:
				10
				30
				10
				35
				30
				10
				20
				Array with unique Elements : 
				10 30 35 20</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../logical-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="../delete-element-from-specified-location-in-array" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		
		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
