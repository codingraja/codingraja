<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Find Addition of two Matrix using Java.</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/logical-coding/logical-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Find Addition of two Matrix using Java.</h1>
				<div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">AddTwoMatrix.java</h2>
			<pre class="brush: java;">
			//Program to find addition of two Matrix

			import java.util.Scanner;
			
			public class AddTwoMatrix {
			
				public static void main(String[] args) {
			
					Scanner scanner = new Scanner(System.in);
					System.out.print("Enter the number of rows: ");
					int rows = scanner.nextInt();
			
					System.out.print("Enter the number of columns: ");
					int columns = scanner.nextInt();
			
					int[][] matrix1 = new int[rows][columns];
					int[][] matrix2 = new int[rows][columns];
			
					System.out.println("Enter the Elements of First Matrix");
					for (int i = 0; i &lt; rows; i++) {
						for (int j = 0; j &lt; columns; j++) {
							matrix1[i][j] = scanner.nextInt(); // elements of first matrix.
						}
					}
					System.out.println("Enter the Element of Second Matrix");
					for (int i = 0; i &lt; rows; i++) {
						for (int j = 0; j &lt; columns; j++) {
							matrix2[i][j] = scanner.nextInt(); // elements of second matrix.
						}
					}
					int[][] matrix3 = new int[rows][columns];
					for (int i = 0; i &lt; rows; i++) {
						for (int j = 0; j &lt; columns; j++) {
							matrix3[i][j] = matrix1[i][j] + matrix2[i][j];
						}
					}
					System.out.println("The Sum of Two Matrices is");
					for (int i = 0; i &lt; rows; i++) {
						for (int j = 0; j &lt; columns; j++) {
							System.out.print(matrix3[i][j] + " ");
						}
						System.out.println();
					}
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Command Prompt</h2>
			<pre class="brush: xml;">
				E:\src>javac AddTwoMatrix.java
				E:\src>java AddTwoMatrix
				
				Enter the number of rows: 3
				Enter the number of columns: 3
				Enter the Elements of First Matrix
				1 2 3 4 5 6 7 8 9
				Enter the Element of Second Matrix
				1 2 3 4 5 6 7 8 9
				The Sum of Two Matrices is
				2	 4	 	6 
				8 	 10	 	12 
				14 	 16 	18</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../logical-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="../remove-duplicate-elements-from-array" class="navlinks">&lt; Previous</a>
	        <a href="../substraction-of-two-matrix" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		
		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
