<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Logical Number Patterns</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/logical-coding/logical-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Logical Number Patterns</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="blog_content">
					
				</div>
				
				<div class="row">
				    
					<div class="col-md-3">
						<a href="1"><img width="200" class="img-responsive" src="img/1.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="2"><img width="200" class="img-responsive" src="img/2.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="3"><img width="200" class="img-responsive" src="img/3.jpg" ></a>
					</div>
					<div class="col-md-3">
						<a href="4"><img width="200" class="img-responsive img-thumbnail" src="img/4.jpg" ></a>
					</div>					
					<div class="col-md-3">
						<a href="5"><img width="200" class="img-responsive img-thumbnail" src="img/5.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="6"><img width="200" class="img-responsive img-thumbnail" src="img/6.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="7"><img width="200" class="img-responsive img-thumbnail" src="img/7.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="8"><img width="200" class="img-responsive img-thumbnail" src="img/8.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="9"><img width="200" class="img-responsive img-thumbnail" src="img/9.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="10"><img width="200" class="img-responsive img-thumbnail" src="img/10.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="11"><img width="200" class="img-responsive img-thumbnail" src="img/11.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="12"><img width="200" class="img-responsive img-thumbnail" src="img/12.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="13"><img width="200" class="img-responsive img-thumbnail" src="img/13.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="14"><img width="200" class="img-responsive img-thumbnail" src="img/14.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="15"><img width="200" class="img-responsive img-thumbnail" src="img/15.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="16"><img width="200" class="img-responsive img-thumbnail" src="img/16.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="17"><img width="200" class="img-responsive img-thumbnail" src="img/17.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="18"><img width="200" class="img-responsive img-thumbnail" src="img/18.jpg" ></a>
					</div>
					
					<div class="col-md-3">
						<a href="19"><img width="200" class="img-responsive img-thumbnail" src="img/19.jpg" ></a>
					</div>
				</div>
			
		    </div>
		</div>
		        
		<!-- <div class="blog_note">
			<h3>Note:</h3>
		  	<p>
		  		All the basics and advanced applications in this tutorial are created 
		  		as a <em>Maven Project</em> and used <em>webapp-javaee7</em> 
		  		<strong>archetype</strong>. If you don't know, how to create <em>Maven</em> 
		  		project than first read this article- 
		  		<a href="#">Creating Maven Project</a>
		  	</p>
		</div> -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        
		        <%@ include file="../logical-program-list.jsp" %>
			
		    </div>
		</div><!-- /# end post -->
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
