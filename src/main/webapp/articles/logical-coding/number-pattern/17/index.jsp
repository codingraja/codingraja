<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Number Pattern using Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/logical-coding/logical-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Number Pattern - 17</h1>
				
				<div class="row">
					<div class="col-md-3">
						<img width="200" alt="Number Patter" src="../img/17.jpg">
					</div>
					<div class="col-md-9">
						<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
					</div>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">NumberPattern17.java</h2>
			<pre class="brush: java;">
			import java.util.Scanner;

			public class NumberPattern17 
			{
				public static void main(String[] args) 
				{
					System.out.print("Enter the No. of Rows :");
					int number = new Scanner(System.in).nextInt();
					
					for(int i=number,k=1; i>1; i--,k++)   //Outer Loop for number of rows
				    {
				        for(int j=1; j&lt;=i; j++)   	//First Pyramid
				            System.out.print(j);
				 
				        for(int j=1; j&lt;k; j++)    	//For second Empty Pyramid
				            System.out.print(" ");
				 
				        for(int j=1; j&lt;k-1; j++)   	//For Third Empty Pyramid
				        	System.out.print(" ");
				 
				        for(int j=i; j>=1; j--)   		//For forth pyramid
				        {
				            if(j==number)
				            	System.out.print("\b"); //To skip middle element
				            System.out.print(j);
				        }
				        System.out.println();        // for new line
				    }
				 
				 
				    for(int i=1,k=number; i&lt;=number; i++,k--)   //Outer Loop for number of rows
				    {
				        for(int j=1; j&lt;=i; j++)   //First Pyramid
				        	System.out.print(j);
				 
				        for(int j=1; j&lt;k; j++)   //For second Empty Pyramid
				        	System.out.print(" ");
				 
				        for(int j=1; j&lt;k-1; j++)   //For Third Empty Pyramid
				        	System.out.print(" ");
				 
				        for(int j=i; j>=1; j--)   //For forth pyramid
				        {
				            if(j==number)
				            	System.out.print("\b"); //To skip middle element
				            System.out.print(j);
				        }
				        System.out.println();        // for new line
				    }	
				}
			}</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../logical-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="../16" class="navlinks">&lt; Previous</a>
	        <a href="../18" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
