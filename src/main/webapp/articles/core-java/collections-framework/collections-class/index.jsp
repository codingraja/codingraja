<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Collections Class in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Collections Class</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<h1>Collections Class</h1>
				<p>The <code>Collections</code> class is a utility class of the Java Collections Framework. It consists
				 static methods that operate on collection object and return that collection object.
				</p>
				
				<p>All methods of this class throw a <code>NullPointerException</code> if,  the passing collections or class 
				objects are <code>null</code>.</p>
				
				<h1>Mostly Used Methods of Collections Class</h1>
				
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>Methods</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>static &lt;T&gt;void	sort(List&lt;T&gt; list)</code></td>
							<td>Sorts the specified list into ascending order, according to the natural ordering of its elements.</td>
						</tr>
						<tr>
							<td><code>static &lt;T&gt; void	sort(List&lt;T&gt; list, Comparator&lt;? super T&gt; c)</code></td>
							<td>Sorts the specified list according to the order induced by the specified comparator.</td>
						</tr>
						<tr>
							<td><code>static &lt;T&gt; Collection&lt;T&gt;	synchronizedCollection(Collection&lt;T&gt; c)</code></td>
							<td>Returns a synchronized (thread-safe) collection backed by the specified collection.</td>
						</tr>
						<tr>
							<td><code>static &lt;T&gt; List&lt;T&gt;	synchronizedList(List&lt;T&gt; list)</code></td>
							<td>Returns a synchronized (thread-safe) list backed by the specified list.</td>
						</tr>
						<tr>
							<td><code>static &lt;T&gt; Set&lt;T&gt;	synchronizedSet(Set&lt;T&gt; s)</code></td>
							<td>Returns a synchronized (thread-safe) set backed by the specified set.</td>
						</tr>
						<tr>
							<td><code>static &lt;T&gt; SortedSet&lt;T&gt;	synchronizedSortedSet(SortedSet&lt;T&gt; s)</code></td>
							<td>Returns a synchronized (thread-safe) sorted set backed by the specified sorted set.</td>
						</tr>
						<tr>
							<td><code>static &lt;K,V&gt; Map&lt;K,V&gt;	synchronizedMap(Map&lt;K,V&gt; m)</code></td>
							<td>Returns a synchronized (thread-safe) map backed by the specified map.</td>
						</tr>
						<tr>
							<td><code>static &lt;K,V&gt; SortedMap&lt;K,V&gt;	synchronizedSortedMap(SortedMap&lt;K,V&gt; m)</code></td>
							<td>Returns a synchronized (thread-safe) sorted map backed by the specified sorted map.</td>
						</tr>
						<tr>
							<td><code>static &lt;T&gt; List&lt;T&gt;	singletonList(T o)</code></td>
							<td>Returns an immutable list containing only the specified object.</td>
						</tr>
						<tr>
							<td><code>static &lt;T&gt; Set&lt;T&gt;	singleton(T o)</code></td>
							<td>Returns an immutable set containing only the specified object.</td>
						</tr>
						<tr>
							<td><code>static &lt;K,V&gt; Map&lt;K,V&gt;	singletonMap(K key, V value)</code></td>
							<td>Returns an immutable map, mapping only the specified key to the specified value.</td>
						</tr>
						<tr>
							<td><code>static void	reverse(List&lt;?&gt; list)</code></td>
							<td>Reverses the order of the elements in the specified list.</td>
						</tr>
					</table>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
