<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Map  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Map Interface</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<ol class="topic-list">
					<li><code>Map</code> is an Object that consists <em>key-value</em> pair. </li>
					<li>A map cannot contain <em>duplicate</em> keys.</li>
					<li>Each key can map to at most one value.</li>
					<li>It  allows one <code>null</code> key and multiple <code>null</code> values.</li>
				</ol>

				<h1>Mostly Used  Methods</h1>
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>Methods</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>V	put(K key, V value)</code></td>
							<td>Associates the specified value with the specified key in this map</td>
						</tr>
						<tr>
							<td><code>void	putAll(Map&lt;? extends K,? extends V&gt; m)</code></td>
							<td>Copies all of the entries from the specified map to this map</td>
						</tr>
						<tr>
							<td><code>V	get(Object key)</code></td>
							<td>Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.
							</td>
						</tr>
						<tr>
							<td><code>boolean	containsKey(Object key)</code></td>
							<td>Returns true if this map contains a mapping for the specified key.</td>
						</tr>
						<tr>
							<td><code>boolean	containsValue(Object value)</code></td>
							<td>Returns true if this map maps one or more keys to the specified value.</td>
						</tr>
						<tr>
							<td><code>V	remove(Object key)</code></td>
							<td>Removes the mapping for a key from this map if it is present</td>
						</tr>
						<tr>
							<td><code>void	clear()</code></td>
							<td>Removes all of the mappings from this map</td>
						</tr>
						<tr>
							<td><code>int	size()</code></td>
							<td>Returns the number of key-value mappings in this map.</td>
						</tr>
						<tr>
							<td><code>Set&lt;Map.Entry&lt;K,V&gt;&gt;	entrySet()</code></td>
							<td>Returns a Set view of the mappings contained in this map.</td>
						</tr>
						<tr>
							<td><code>Set&lt;K&gt;	keySet()</code></td>
							<td>Returns a Set view of the keys contained in this map.</td>
						</tr>
						<tr>
							<td><code>Collection&lt;V&gt;	values()</code></td>
							<td>Returns a Collection view of the values contained in this map.</td>
						</tr>
						<tr>
							<td><code>boolean	isEmpty()</code></td>
							<td>Returns true if this map contains no key-value mappings.</td>
						</tr>
					</table>
				</div>
				
				<h1>Map Implementations</h1>
				<ol class="list-impl">
					<li><a href="../hash-map">HashMap</a></li>
					<li><a href="../linked-hash-map">LinkedHashMap</a></li>
					<li><a href="../tree-map">TreeMap</a></li>
					<li><a href="../identity-hash-map">IdentityHashMap</a></li>
					<li><a href="../weak-hash-map">WeakHashMap</a></li>
					<li><a href="../concurrent-hash-map">ConcurrentHashMap</a></li>
				</ol>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
