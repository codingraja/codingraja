<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Comparable Interface  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Comparable Interface</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<p><code>Comparable</code> Interface is implemented to sorting the 
					elements of the <code>List</code>.
				</p>
				<p>If we want to sort the elements of the <code>List</code> then we write 
					<code>Collections.sort(list)</code> but the <code>Collections.sort(list)</code> 
					will sort only those objects which implements <code>Comparable</code> Interface only.
				</p>
				
				<p>If a class implements <code>Comparable</code> interface then objects of that class will 
					be sorted automatically in natural ordering by calling <code>Collections.sort(list)</code>.
				</p>
				
				<h1>Classes that Implements Comparable Interface</h1>
				<div class="table-responsive">
					<table class="table bordered">
						<tr>
							<th>Class</th>
							<th>Natural Ordering</th>
						</tr>
						<tr>
							<td><code>Byte</code></td>
							<td>Signed numerical</td>
						</tr>
						<tr>
							<td><code>Character</code></td>
							<td>Unsigned numerical</td>
						</tr>
						<tr>
							<td><code>Long</code></td>
							<td>Signed numerical</td>
						</tr>
						<tr>
							<td><code>Integer</code></td>
							<td>Signed numerical</td>
						</tr>
						<tr>
							<td><code>Short</code></td>
							<td>Signed numerical</td>
						</tr>
						<tr>
							<td><code>Double</code></td>
							<td>Signed numerical</td>
						</tr>
						<tr>
							<td><code>Float</code></td>
							<td>Signed numerical</td>
						</tr>
						<tr>
							<td><code>BigInteger</code></td>
							<td>Signed numerical</td>
						</tr>
						<tr>
							<td><code>BigDecimal</code></td>
							<td>Signed numerical</td>
						</tr>
						<tr>
							<td><code>Boolean</code></td>
							<td><b>Boolean.FALSE &lt; Boolean.TRUE</b></td>
						</tr>
						<tr>
							<td><code>File</code></td>
							<td>System-dependent lexicographic on path name</td>
						</tr>
						<tr>
							<td><code>String</code></td>
							<td>Lexicographic</td>
						</tr>
						<tr>
							<td><code>Date</code></td>
							<td>Chronological</td>
						</tr>
						<tr>
							<td><code>CollationKey</code></td>
							<td>Locale-specific lexicographic</td>
						</tr>
					</table>
				</div>
				
				<p>If the <code>List</code> consists of <code>String</code> elements, it will be sorted into 
					<b>alphabetical</b> order. If it consists of <code>Date</code> elements, it will be sorted 
					into <b>chronological</b> order.
				</p>
				
				<div class="program_code">
				<div class="file_name">Comparable Interface</div>
				<pre class="brush: java;">
				public interface Comparable&lt;T> {
				    public int compareTo(T o);
				}
				</pre>
				</div>
				
				<p>The <code>compareTo()</code> method compare <code>this</code> object and <b>specified</b> object 
					and <b>returns</b> :
				</p>
				
				<p><b>this.id &gt; o.id = Positive Integer</b></p>
				<p><b>this.id &lt; o.id = Negative Integer</b></p>
				<p><b>this.id == o.id = 0</b></p>
				
				<p>If either of the arguments has an <b>inappropriate</b> type for the <b>Comparator</b>, the compare 
					method throws a <code>ClassCastException</code>.
				</p>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">ComparableDemo.java</h2>
			<pre class="brush: java;">
			import java.util.*;

			class Employee implements Comparable&lt;Employee&gt;{
				int empId;
				String empName;
				int age;
				float salary;
				
				Employee(int empId, String empName, int age, float salary) {
					this.empId = empId;
					this.empName = empName;
					this.age = age;
					this.salary = salary;
				}
				
				@Override
				public String toString() {
					return "Emp ID:"+empId+" Emp Name:"+empName+" Age:"+age+" Salary:"+salary;
				}
				
				@Override
				public int compareTo(Employee emp) {
					return this.empId - emp.empId;
				}
			}
			
			public class ComparableDemo {
				public static void main(String[] args) {
					List&lt;Employee&gt; empList = new ArrayList&lt;&gt;();
					empList.add(new Employee(5,"XYZ",20,20000));
					empList.add(new Employee(1,"abc",30,30000));
					empList.add(new Employee(4,"xyz",25,25000));
					empList.add(new Employee(3,"ABC",35,29000));
					empList.add(new Employee(2,"mnp",40,50000));
					
					Collections.sort(empList);
					
					Iterator itr = empList.iterator();
					while(itr.hasNext()) {
						System.out.println(itr.next());
					}
				}
			}</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
