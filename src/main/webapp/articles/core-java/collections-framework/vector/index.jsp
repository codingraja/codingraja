<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Vector  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Vector</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<ol class="topic-list">
					<li><code>Vector</code> implements <code>List</code> interface and extends <code>AbstractList</code> class.</li>
					<li>It is a <em>Synchronized</em> form of <code>ArrayList</code>.</li>
					<li>Initial default capacity is <em>10</em>.</li>
					<li>Vector grows by double of its size when resized.</li>
					<li>It allows random access because array works an the index basis.</li>
					<li>It is  <em>Synchronized</em>.</li>
					<li>It maintains <em>insertion order</em> of the elements.</li>
					<li>It allows <em>duplicate</em> elements.</li>
					<li>It manipulates slowly because more shifting is required. </li>
				</ol>
				
				
				<h1>Vector Constructors</h1>
				<p>Vector has Four Constructors-</p>
				<div class="table-responsive">
					<table class="table bordered">
						<tr>
							<th>Constructor</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>Vector()</code></td>
							<td>Create an empty array with an initial capacity of ten.</td>
						</tr>
						<tr>
							<td><code>Vector(int initialCapacity)</code></td>
							<td>Create an empty array with the specified initial capacity.</td>
						</tr>
						<tr>
							<td><code>Vector(Collection&lt;? extends E> c)</code></td>
							<td>Create a array containing the elements of the specified collection.</td>
						</tr>
						<tr>
							<td><code>Vector(int initialCapacity,<br> int capacityIncrement)</code></td>
							<td>Create an empty vector with the specified initial capacity and capacity increment</td>
						</tr>
					</table>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">VectorDemo.java</h2>
			<pre class="brush: java;">
			package com.codingraja;

			import java.util.List;
			import java.util.Vector;
			import java.util.Iterator;
			
			public class VectorDemo {
			
				public static void main(String[] args) {
					
					List&lt;String> list = new Vector&lt;String>();
					list.add("CL Verma");
					list.add("Suraj Tiwari");
					list.add("Pradeep Singh");
					list.add("Manvir Singh");
					list.add("Rajendra Singh Bisht");
					
					Iterator&lt;String> itr = list.iterator();
					while(itr.hasNext())
					{
						System.out.println(""+itr.next());
					}
					
					//hasCode
					System.out.println("HasCode="+list.hashCode());
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Output</h2>
			<pre class="brush: xml; output">
			E:\src>javac -d . VectorDemo.java

			E:\src>java com.codingraja.VectorDemo
			CL Verma
			Suraj Tiwari
			Pradeep Singh
			Manvir Singh
			Rajendra Singh Bisht
			HasCode=1747981032</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
