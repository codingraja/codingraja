<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>LinkedList  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>LinkedList</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<ol class="topic-list">
					<li><code>LinkedList</code> implements <code>List</code> and <code>Deque</code> interface.</li>
					<li>It is an implementation of<em> Doubly-linked list</em>.</li>
					<li>It does not allow random access.</li>
					<li>It is not <em>Synchronized</em>.</li>
					<li>It maintains <em>insertion order</em> of the elements.</li>
					<li>It allows <em>duplicate</em> elements.</li>
					<li>It manipulates(insertion/deletion) fast because  shifting is not required. </li>
				</ol>
				
				
				<h1>LinkedList Constructors</h1>
				<p>LinkedList has Two Constructors-</p>
				<div class="table-responsive">
					<table class="table bordered">
						<tr>
							<th>Constructor</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>LinkedList()</code></td>
							<td>Create an empty list.</td>
						</tr>
						<tr>
							<td><code>LinkedList(Collection&lt;? extends E> c)</code></td>
							<td>Create a list containing the elements of the specified collection.</td>
						</tr>
					</table>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">LinkedListDemo.java</h2>
			<pre class="brush: java;">
			package com.codingraja;

			import java.util.List;
			import java.util.LinkedList;
			import java.util.ListIterator;
			
			public class LinkedListExample {
			
				public static void main(String[] args) {
						
					List&lt;String> list = new LinkedList&lt;String>();
					list.add("CL Verma");
					list.add("Suraj Tiwari");
					list.add("Pradeep Singh");
					list.add("Manvir Singh");
					list.add("Rajendra Singh Bisht");
					
					ListIterator&lt;String> litr = list.listIterator();
					//Traverse in forward direction
					System.out.println("Traverse in forward direction");
					while(litr.hasNext())
					{
						System.out.println(""+litr.next());
					}
					
					//Traverse in backward direction
					System.out.println("Traverse in backward direction");
					while(litr.hasPrevious())
					{
						System.out.println(""+litr.previous());
					}
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Output</h2>
			<pre class="brush: xml; output">
			E:\src>javac -d . LinkedListDemo.java

			E:\src>java com.codingraja.LinkedListDemo
			Traverse in forward direction
			CL Verma
			Suraj Tiwari
			Pradeep Singh
			Manvir Singh
			Rajendra Singh Bisht
			Traverse in backward direction
			Rajendra Singh Bisht
			Manvir Singh
			Pradeep Singh
			Suraj Tiwari
			CL Verma</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
