<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>LinkedHashMap  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>LinkedHashMap</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<ol class="topic-list">
					<li><code>LinkedHashMap</code> extends <code>HashMap</code> class and implements 
						<code>Map</code> interface. 
					</li>
					<li>Initial default capacity is <em>16</em> and Load Factor <em>0.75</em>.</li>
					<li>It does not allow <em>duplicate</em> keys, contains only <em>unique</em> keys.</li>
					<li>It maintains <em>insertion order</em> of the keys.</li>
					<li>It  allows one <code>null</code> key and multiple <code>null</code> values.</li>
					<li>It is not <em>synchronized</em>.</li>
				</ol>
				
				
				<h1>LinkedHashMap Constructors</h1>
				<p>LinkedHashMap has Five Constructor- </p>
				<div class="table-responsive">
					<table class="table bordered">
						<tr>
							<th>Constructor</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>LinkedHashMap()</code></td>
							<td>Creates an empty LinkedHashMap instance with the default initial capacity (16) and load factor (0.75).</td>
						</tr>
						<tr>
							<td><code>LinkedHashMap(int initialCapacity)</code></td>
							<td>Creates an empty LinkedHashMap instance with the specified initial capacity and a default load factor (0.75).</td>
						</tr>
						<tr>
							<td><code>LinkedHashMap(int initialCapacity,<br /> float loadFactor)</code></td>
							<td>Creates an empty LinkedHashMap instance with the specified initial capacity and load factor.</td>
						</tr>
						<tr>
							<td><code>LinkedHashMap(int initialCapacity,<br /> float loadFactor, boolean accessOrder)</code></td>
							<td>Creates an empty LinkedHashMap instance with the specified initial capacity, load factor and ordering mode.</td>
						</tr>
						<tr>
							<td><code>LinkedHashMap(Map&lt;? extends K,? extends V> m)</code></td>
							<td>Creates an LinkedHashMap instance with the same mappings as the specified map</td>
						</tr>
					</table>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">LinkedHashMapDemo.java</h2>
			<pre class="brush: java;">
			package com.codingraja;

			import java.util.LinkedHashMap;
			import java.util.Iterator;
			import java.util.Map;
			
			public class LinkedHashMapDemo {
			
				public static void main(String[] args) {
					
					LinkedHashMap&lt;String,String> map = new LinkedHashMap&lt;String,String>();
					map.put("dev", "CL Verma");
					map.put("hr", "Suraj Tiwari");
					map.put("ceo", "Pradeep Singh");
					map.put("designer", "Manvir Singh");
					map.put("owner", "Rajendra Singh Bisht");
					
					map.put("tester", "CL Verma");
					map.put(null, "Hitesh kumar");
					
					//Traverse LinkedHashmap
					Iterator itr = map.entrySet().iterator();
					while(itr.hasNext())
					{
						Map.Entry entry = (Map.Entry)itr.next();
						System.out.println("Key :"+entry.getKey()+", Value :"+entry.getValue());
					}
					
					//get value by key
					System.out.println("Name of owner :"+map.get("owner"));
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Output</h2>
			<pre class="brush: xml; output">
			E:\src>javac -d . LinkedHashMapDemo.java

			E:\src>java com.codingraja.LinkedHashMapDemo
			Key :dev, Value :CL Verma
			Key :hr, Value :Suraj Tiwari
			Key :ceo, Value :Pradeep Singh
			Key :designer, Value :Manvir Singh
			Key :owner, Value :Rajendra Singh Bisht
			Key :tester, Value :CL Verma
			Key :null, Value :Hitesh kumar
			Name of owner :Rajendra Singh Bisht</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
