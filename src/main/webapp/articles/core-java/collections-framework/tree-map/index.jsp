<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>TreeMap  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>TreeMap</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<ol class="topic-list">
					<li>The <code>TreeMap</code> class extends <code>AbstractMap</code> and implements 
						<code>NavigableMap</code> interface that extends the <code>SortedMap</code> interface.
					</li>
					<li>It does not allow <em>duplicate keys</em>, contains only <em>unique</em> keys.</li>
					<li>TreeMap is <em>sorted</em> form of Map, it contains keys in <em>ascending order</em>.</li>
					<li>It does not allow <code>null</code> keys, gives the exception at runtime.</li>
					<li>It is not <em>synchronized</em>.</li>
				</ol>
				
				
				<h1>TreeMap Constructors</h1>
				<p>TreeMap has Four Constructor- </p>
				<div class="table-responsive">
					<table class="table bordered">
						<tr>
							<th>Constructor</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>TreeMap()</code></td>
							<td>Creates a new, empty tree map, using the natural ordering of its keys.</td>
						</tr>
						<tr>
							<td><code>TreeMap(Comparator&lt;? super K> comparator)</code></td>
							<td>Creates a new, empty tree map, ordered according to the given comparator.</td>
						</tr>
						<tr>
							<td><code>TreeMap(Map&lt;? extends K,? extends V> m)</code></td>
							<td>Creates a new tree map containing the same mappings as the given map, ordered according to the natural ordering of its keys.</td>
						</tr>
						<tr>
							<td><code>TreeMap(SortedMap&lt;K,? extends V> m)</code></td>
							<td>Creates a new tree map containing the same mappings and using the same ordering as the specified sorted map</td>
						</tr>
					</table>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">TreeMapDemo.java</h2>
			<pre class="brush: java;">
			package com.codingraja;

			import java.util.Map;
			import java.util.TreeMap;
			import java.util.Iterator;
			
			public class TreeMapDemo {
			
				public static void main(String[] args) {
					
					TreeMap&lt;String, String> map = new TreeMap&lt;String, String>();
					map.put("dev", "CL Verma");
					map.put("hr", "Suraj Tiwari");
					map.put("ceo", "Pradeep Singh");
					map.put("designer", "Manvir Singh");
					map.put("owner", "Rajendra Singh Bisht");
					
					//Map can contain multiple duplicate values
					map.put("tester", "CL Verma");
					
					//Traverse TreeMap
					Iterator itr = map.entrySet().iterator();
					while(itr.hasNext())
					{
						Map.Entry entry = (Map.Entry)itr.next();
						System.out.println("Key :"+entry.getKey()+", Value :"+entry.getValue());
					}
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Output</h2>
			<pre class="brush: xml; output">
			E:\src>javac -d . TreeMapDemo.java

			E:\src>java com.codingraja.TreeMapDemo
			Key :ceo, Value :Pradeep Singh
			Key :designer, Value :Manvir Singh
			Key :dev, Value :CL Verma
			Key :hr, Value :Suraj Tiwari
			Key :owner, Value :Rajendra Singh Bisht
			Key :tester, Value :CL Verma</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
