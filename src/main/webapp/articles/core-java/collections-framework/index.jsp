<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Core Java Tutorials</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Collections Framework</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
		        
		        <div class="blog_content">
					In this tutorial, we will cover all the basic and advanced topics of <em>Core Java</em> 
					and all the interview logical programs.
				</div>
				
				<div class="blog_content">
					A collection is an <em>object</em> that contains multiple elements into a 
					<em>single unit</em>. Collections are used to <em>store, retrieve, manipulate,</em> 
					and <em>communicate</em> aggregate data. They represent a <em>natural</em> group, 
					like- a collection of <em>letters</em> and mapping of names to phone numbers etc.
				</div>
				
				<div class="blog_content">
					A <em>collections framework</em> is a organized architecture for <em>representing</em> 
					and <em>manipulating</em> collections.
				</div>
				
				<div class="blog_content">
					<strong>All collections framework contain the following:</strong>
				</div>
				
				<div class="blog_content">
					<strong>Interfaces:</strong>  Interfaces allow collections to be manipulated independently 
					of the details of their representation.
				</div>
				 
				<div class="blog_content">
					<strong>Implementations:</strong> These are the concrete classes that provides the 
					implementation of the collection interfaces. They are reusable data structures.
				</div>
				
				<div class="blog_content">
					<strong>Algorithms:</strong>These are the methods that perform useful operations, such 
					as searching and sorting, on objects that implement collection interfaces. 
				</div>
				 
				<h1>Core Collection Interfaces</h1>
				<div class="blog_content">
					Core collection interfaces are the foundation of the Java <em>Collections Framework</em>.
					These interfaces allow collections to be manipulated independently of the details of 
					their representation.
				</div>
				
				<img class="img-responsive" src="${imageUrl}/core-java/collections.PNG" alt="Core Collection Interfaces" />
				
				<h4>List Interface</h4>
				<div class="blog_content">
					A <em>List</em> is an <em>ordered</em> Collection, sometimes called a <em>sequence</em> 
					Collection. It allows operation based on the <em>indexes</em> , means We can access, 
					search and  insert an element using index value.
				</div>
				
				<div class="blog_content">
					<em>List</em> provides an another iterator called <em>ListIterator</em>, it provides 
					traversing in both direction(left or right).
				</div>
				
				<div class="blog_content">
					<strong>There are three implementations of List Interface</strong>
				</div>
				
				<ol class="type-list">
					<li><a href="array-list">ArrayList</a></li>
					<li><a href="linked-list">LinkedList</a></li>
					<li><a href="vector">Vector</a></li>
				</ol>
				
				
				<h4>Queue Interface</h4>
				<div class="blog_content">
					A <em>Queue</em> is a collection that provides additional <em>insertion, removal</em>, 
					and <em>inspection</em> operations. The <em>Queue</em> interface follows.
				</div>
				
				<div class="program-code">
				<div class="file-name">Queue Interface</div>
				<pre class="brush: java;">
				public interface Queue&lt;E> extends Collection&lt;E> {
				    E element();
				    boolean offer(E e);
				    E peek();
				    E poll();
				    E remove();
				}
				</pre>
				</div>
				
				<div class="blog_content">
					<em>Implemented class of <em>Queue</em> Interface</em>
				</div>
				
				<ol class="type-list">
					<li><a href="array-queue">ArrayQueue</a></li>
				</ol>
				
				
				<h4>Set Interface</h4>
				<div class="blog_content">
					A <em>Set</em> is a <em>Collection</em> that does not allow <em>duplicate</em> elements. 
					It contains only methods inherited from <em>Collection Interface</em> and prohibit the 
					duplicate elements.
				</div>
				
				<div class="blog_content">
					Two Set instances are equal if they contain the same elements.
				</div>
				<div class="blog_content">
					<strong>There are three implementations of <em>Set</em> Interface</strong>
				</div>
				
				<ol class="type-list">
					<li><a href="hash-set">HashSet</a></li>
					<li><a href="linked-hash-set">LinkedHashSet</a></li>
					<li><a href="tree-set">TreeSet</a></li>
				</ol>
				
				<h4>SortedSet Interface</h4>
				<div class="blog_content">
					A <em>SortedSet</em> is Set that maintains its elements in ascending order. It 
					sorted according to the elements' natural ordering, means alphabetically, according 
					to numbers etc.
				</div>
				
				<h4>Map Interface</h4>
				<div class="blog_content">
					A <em>Map</em> is an object that contains pairs of <em>keys</em> to <em>values</em>. A 
					map does not contain duplicate keys and Each key can map to at most one value
				</div>
				
				<div class="blog_content">
					<strong>There are three implementations of <em>Map</em> Interface</strong>
				</div>
				
				<ol class="type-list">
					<li><a href="hash-map">HashMap</a></li>
					<li><a href="linked-hash-map">LinkedHashMap</a></li>
					<li><a href="tree-map">TreeMap</a></li>
				</ol>
				
				
				<h4>SortedMap Interface</h4>
				<div class="blog_content">
					A <em>SortedMap</em> is a Map that maintains its entries in ascending order, It 
					sorted according to the keys' natural ordering. means alphabetically, according to 
					numbers etc. 
				</div>
			
		    </div>
		</div>
		        
		<!-- <div class="blog_note">
			<h3>Note:</h3>
		  	<p>
		  		All the basics and advanced applications in this tutorial are created 
		  		as a <em>Maven Project</em> and used <em>webapp-javaee7</em> 
		  		<strong>archetype</strong>. If you don't know, how to create <em>Maven</em> 
		  		project than first read this article- 
		  		<a href="#">Creating Maven Project</a>
		  	</p>
		</div> -->
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        
		        <%@ include file="../core-java-program-list.jsp" %>
			
		    </div>
		</div><!-- /# end post -->
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
