<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Comparator Interface  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Comparator Interface</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<p>If you want to sort some objects in an order other then their natural ordering Or 
					if you want to sort some objects that don't implement <code>Comparable</code> interface. 
					Then you'll need to make <code>Comparator</code> by implementing <code>Comparator</code> 
					Interface.
				</p>
				
				<p>The <code>Comparator</code> interface consists of a single method.</p>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<div class="file_name">Comparator Interface</div>
			<pre class="brush: java;">
			public interface Comparator&lt;T> {
			    int compare(T o1, T o2);
			}
			</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<p>The <code>compare</code> method compares its two arguments <b>o1</b> and <b>o2</b> 
	        		and <code>return</code> :
	        	</p>
				<p><b>o1 &gt; o2 = Positive Integer</b></p>
				<p><b>o1 &lt; o2 = Negative Integer</b></p>
				<p><b>o1 == o2 = 0</b></p>
				
				<p>If either of the arguments has an <b>inappropriate</b> type for the <code>Comparator</code>,
				 	the compare method throws a <code>ClassCastException</code>.
				</p>
	        </div>
	    </div>
		
		<div class="program_code">
			<h2 class="file_name">ComparatorDemo.java</h2>
			<pre class="brush: java;">
			import java.util.*;

			class Employee {
				int empId;
				String empName;
				int age;
				float salary;
				
				Employee(int empId, String empName, int age, float salary) {
					this.empId = empId;
					this.empName = empName;
					this.age = age;
					this.salary = salary;
				}
				
				@Override
				public String toString() {
					return "Emp ID:"+empId+" Emp Name:"+empName+" Age:"+age+" Salary:"+salary;
				}
			}
			
			class IdComparator implements Comparator&lt;Employee&gt; {
				@Override
				public int compare(Employee emp1, Employee emp2) {
					return emp1.empId - emp2.empId;
				}
			}
			
			class NameComparator implements Comparator&lt;Employee&gt; {
				@Override
				public int compare(Employee emp1, Employee emp2) {
					return emp1.empName.compareTo(emp2.empName);
				}
			}
			
			class AgeComparator implements Comparator&lt;Employee&gt; {
				@Override
				public int compare(Employee emp1, Employee emp2) {
					return emp1.age - emp2.age;
				}
			}
			
			class SalaryComparator implements Comparator&lt;Employee&gt; {
				@Override
				public int compare(Employee emp1, Employee emp2) {
					return (int)(emp1.salary - emp2.salary);
				}
			}
			
			public class ComparatorDemo {
				public static void main(String[] args) {
					List&lt;Employee&gt; empList = new ArrayList&lt;&gt;();
					empList.add(new Employee(5,"XYZ",20,20000));
					empList.add(new Employee(1,"abc",30,30000));
					empList.add(new Employee(4,"xyz",25,25000));
					empList.add(new Employee(3,"ABC",35,29000));
					empList.add(new Employee(2,"mnp",40,50000));
					
					Collections.sort(empList,new AgeComparator());
					
					Iterator itr = empList.iterator();
					while(itr.hasNext()) {
						System.out.println(itr.next());
					}
				}
			}</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
