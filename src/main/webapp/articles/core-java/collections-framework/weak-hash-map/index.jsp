<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>WeakHashMap  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>WeakHashMap</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<p><code>WeakHashMap</code> is a <code>Hashtable</code> based implementation of the <code>Map</code> 
				interface, with <em>weak keys</em>.</p>
				
				<p>An entry in a <code>WeakHashMap</code> will automatically be removed when its key is 
				no longer in use. If key is no longer in use then that is made <em>finalizable</em>, 
				<em>finalized</em>, and then <em>reclaimed</em> by the garbage collector.</p>
				
				<p>When a key has been discarded its entry is effectively removed from the map.</p>
				<p>Once a key is discarded it can never be recreated.</p>
				
				<ol class="topic-list">
					<li><code>WeakHashMap</code> extends <code>AbstractMap</code> class and implements <code>Map</code> interface</li>
					<li>The default initial capacity -- MUST be a power of two.</li>
					<li>Once a key is discarded it can never be recreated.</li>
					<li><code>WeakHashMap</code> is designed for use only in the rare cases wherein <em>reference-equality</em> is required.</li>
					<li><code>WeakHashMap</code> provides constant-time performance for the basic operations (<em>get</em> and <em>put</em>)</li>
					<li>It does not maintain <em>insertion order</em> of the keys.</li>
					<li>It is not <em>synchronized</em>.</li>
				</ol>
				
				
				<h1>WeakHashMap Constructors</h1>
				<p>WeakHashMap has Four Constructor- </p>
				<div class="table-responsive">
					<table class="table bordered">
						<tr>
							<th>Constructor</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>WeakHashMap()</code></td>
							<td>Creates an empty WeakHashMap instance with the default initial capacity (16) and load factor (0.75).</td>
						</tr>
						<tr>
							<td><code>WeakHashMap(int initialCapacity)</code></td>
							<td>Creates an empty WeakHashMap instance with the specified initial capacity and a default load factor (0.75).</td>
						</tr>
						<tr>
							<td><code>WeakHashMap(int initialCapacity,<br /> float loadFactor)</code></td>
							<td>Creates an empty WeakHashMap instance with the specified initial capacity and load factor.</td>
						</tr>
						<tr>
							<td><code>WeakHashMap(Map&lt;? extends K,? extends V> m)</code></td>
							<td>Creates an WeakHashMap instance with the same mappings as the specified map</td>
						</tr>
					</table>
				</div>
				
				<p> Each key object in a <code>WeakHashMap</code> is stored indirectly as
				  the referent of a weak reference.  Therefore a key will automatically be
				  removed only after the weak references to it, both inside and outside of the
				  map, have been cleared by the garbage collector.
				</p>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
