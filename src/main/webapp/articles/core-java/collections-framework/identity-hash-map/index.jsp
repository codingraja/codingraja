<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>IdentityHashMap  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>IdentityHashMap</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				<p>It is mainly used in serialization or deep-copying etc.</p>
				<ol class="topic-list">
					<li><code>IdentityHashMap</code> extends <code>AbstractMap</code> class and implements <code>Map</code> interface.</li>
					<li>It uses <em>reference-equality</em> in place of <em>object-equality</em> when comparing <em>keys</em> (and <em>values</em>).</li>
					<li>In an <code>IdentityHashMap</code>, two keys <em>k1</em> and <em>k2</em> are considered equal if and only if (<em>k1==k2</em>).</li>
					<li><code>IdentityHashMap</code> is designed for use only in the rare cases wherein <em>reference-equality</em> is required.</li>
					<li><code>IdentityHashMap</code> provides <em>constant-time</em> performance for the basic operations (<em>get</em> and <em>put</em>)</li>
					<li>It does not maintain <em>insertion order</em> of the keys.</li>
					<li>It is not <em>synchronized</em>.</li>
				</ol>
				
				
				<h1>IdentityHashMap Constructors</h1>
				<p>IdentityHashMap has Three Constructor- </p>
				
				<div class="table-responsive">
					<table class="table bordered">
						<tr>
							<th>Constructor</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>IdentityHashMap()</code></td>
							<td>Creates an empty IdentityHashMap instance with the default initial capacity (21).</td>
						</tr>
						<tr>
							<td><code>IdentityHashMap(int initialCapacity)</code></td>
							<td>Creates an empty IdentityHashMap instance with the specified initial capacity.</td>
						</tr>
						<tr>
							<td><code>IdentityHashMap(Map&lt;? extends K,? extends V> m)</code></td>
							<td>Creates an IdentityHashMap instance with the same mappings as the specified map.</td>
						</tr>
					</table>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">IdentityHashMapDemo.java</h2>
			<pre class="brush: java;">
			package com.codingraja;

			import java.util.HashMap; 
			import java.util.IdentityHashMap; 
			import java.util.Map;
			
			public class IdentityHashMapDemo {
			
				public static void main(String[] args) 
				{ 
					Map ihm = new IdentityHashMap&lt;String,String>(); 
					Map hashMap = new HashMap(); 
					
					ihm.put("java", "Platform"); 
					ihm.put(new String("java"), "Language"); 
					ihm.put("java", "Everything");
					
					hashMap.put("java", "Platform"); 
					hashMap.put(new String("java"), "Language"); 
					hashMap.put("java", "Everything");
					
					System.out.println("Identity Map Size:" +  ihm.keySet().size()); 
					System.out.println("Identity Map" + ihm);
					
					System.out.println("Hash Map Size:" + hashMap.keySet().size()); 
					System.out.println("Hash Map" + hashMap); 		 
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Output</h2>
			<pre class="brush: xml; output">
			Identity Map Size:2
			Identity Map{java=Language, java=Everything}
			Hash Map Size:1
			Hash Map{java=Everything}</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<p>The Size of <code>IdentityHashMap</code> is <b>2</b> because here <b>"java"</b> and 
				<code>new String("java")</code> are considered two different Object. The comparison is 
				done using <b>==</b> operator.</p>
				
				<p>The Size of <code>HashMap</code> is <b>1</b> because <code>K1.equals(K2)</code> returns 
				<b>true</b> for all three Keys and every time will remove the old value and updating 
				it with the new value.</p>
	        </div>
	    </div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
