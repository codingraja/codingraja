<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>HashMap  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>HashMap</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<ol class="topic-list">
					<li><code>HashMap</code> extends <code>AbstractMap</code> class and implements <code>Map</code> interface. </li>
					<li>Initial default capacity is <em>16</em> and Load Factor <em>0.75</em>.</li>
					<li>It does not allow <em>duplicate</em> keys, contains only <em>unique</em> keys.</li>
					<li>It does not maintain <em>insertion order</em> of the keys.</li>
					<li>It  allows one <em>null</em> key and multiple <em>null</em> values.</li>
					<li>It is not <em>synchronized</em>.</li>
				</ol>
				
				
				<h1>HashMap Constructors</h1>
				<p>HashMap has Four Constructor-</p>
				<div class="table-responsive">
					<table class="table bordered">
						<tr>
							<th>Constructor</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>HashMap()</code></td>
							<td>Creates an empty HashMap instance with the default initial capacity (16) and load factor (0.75).</td>
						</tr>
						<tr>
							<td><code>HashMap(int initialCapacity)</code></td>
							<td>Creates an empty HashMap instance with the specified initial capacity and a default load factor (0.75).</td>
						</tr>
						<tr>
							<td><code>HashMap(int initialCapacity,<br /> float loadFactor)</code></td>
							<td>Creates an empty HashMap instance with the specified initial capacity and load factor.</td>
						</tr>
						<tr>
							<td><code>HashMap(Map&lt;? extends K,? extends V> m)</code></td>
							<td>Creates an HashMap instance with the same mappings as the specified map</td>
						</tr>
					</table>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">HashMapDemo.java</h2>
			<pre class="brush: java;">
			package com.codingraja;

			import java.util.HashMap;
			import java.util.Iterator;
			import java.util.Map;
			
			public class HashMapDemo {
			
				public static void main(String[] args) {
					
					//HashMap contains key-value pair and does not contains duplicate key
					HashMap&lt;String,String> map = new HashMap&lt;String,String>();  
					
					//HashMap does not maintain insertion order
					map.put("dev", "CL Verma");    
					map.put("hr", "Suraj Tiwari"); //HashMap allow multiple duplicate value
					map.put("ceo", "Pradeep Singh"); // HashMap allow only one null key
					map.put("designer", "Man Vir Singh");
					map.put("owner", "Rajendra Singh Bisht");
					
					map.put("dev", "CL Verma R"); // It will override previous value
					
					map.put("tester", "CL Verma");
					
					map.put(null, "Hitesh Kumar");
					
					//Traverse HashMap
					Iterator itr = map.entrySet().iterator();
					while(itr.hasNext())
					{
						Map.Entry entry = (Map.Entry)itr.next();
						System.out.println("Key :"+entry.getKey()+", Value :"+entry.getValue());
					}
					
					//Find value by key
					System.out.println("Name of dev :"+map.get("dev")); 
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Output</h2>
			<pre class="brush: xml; output">
			E:\src>javac -d . HashMapDemo.java

			E:\src>java com.codingraja.HashMapDemo
			Key :owner, Value :Rajendra Singh Bisht
			Key :null, Value :Hitesh Kumar
			Key :dev, Value :CL Verma R
			Key :tester, Value :CL Verma
			Key :hr, Value :Suraj Tiwari
			Key :designer, Value :Man Vir Singh
			Key :ceo, Value :Pradeep Singh
			Name of dev :CL Verma R</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
