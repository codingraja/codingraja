<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>ArrayList  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>ArrayList</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<ol class="topic-list">
					<li><code>ArrayList</code> implements <code>List</code> Interface and extends <code>AbstractList</code>class.</li>
					<li>It is a <em>dynamic array</em> implementation of the <em>List</em>.</li>
					<li>Initial default capacity is <em>10</em>.</li>
					<li><code>ArrayList</code> grows by <em>half</em> of its size when resized.</li>
					<li>It allows <em>random access</em> because array works an the index basis.</li>
					<li>It is not <em>Synchronized</em>.</li>
					<li>It maintains <em>insertion order</em> of the elements.</li>
					<li>It allows <em>duplicate</em> elements.</li>
					<li>It manipulates(insertion/deletion) <em>slowly</em> because more shifting is required. </li>
				</ol>
				
				<h1>ArrayList Constructors</h1>
				<p><code>ArrayList</code> has Three Constructors-</p>
				<div class="table-responsive">
					<table class="table bordered">
						<tr>
							<th>Constructor</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>ArrayList()</code></td>
							<td>Create an empty list with an initial capacity of ten.</td>
						</tr>
						<tr>
							<td><code>ArrayList(int initialCapacity)</code></td>
							<td>Create an empty list with the specified initial capacity.</td>
						</tr>
						<tr>
							<td><code>ArrayList(Collection&lt;? extends E> c)</code></td>
							<td>Create a list containing the elements of the specified collection.</td>
						</tr>
					</table>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="program_code">
			<h2 class="file_name">ArrayListDemo.java</h2>
			<pre class="brush: java;">
			package com.codingraja;

			import java.util.Collections;
			import java.util.List;
			import java.util.ArrayList;
			import java.util.Iterator;
			
			public class ArrayListDemo {
			
				public static void main(String[] args) {
					
					List&lt;String> course  = new ArrayList&lt;String>();
					course.add("MCA");
					course.add("MBA");
					course.add("M.Tech");
					course.add("B.Tech");
					
					System.out.println("List of Course");
					Iterator&lt;String> itr = course.iterator();
					while(itr.hasNext())
					{
						System.out.println(""+itr.next());
					}
					
					//Another course list
					List&lt;String> course1 = new ArrayList&lt;String>();
					course1.add("BCA");
					course1.add("BBA");
					course1.add("B.Sc");
					course1.add("B.Com.");
					
					//Merge both list
					course.addAll(course1);
					
					//Sort ArrayList
					Collections.sort(course);
					
					Iterator&lt;String> itr1 = course.iterator();
					
					System.out.println("List of Course after Merge");
					while(itr1.hasNext())
					{
						System.out.println(""+itr1.next());
					}
					
					//Get size of list
					System.out.println("List Size="+course.size());
					
					//Check list is empty or not
					System.out.println("List is empty: "+course.isEmpty());		
					
				}
			}</pre>
		</div>
		
		<div class="program_code">
			<h2 class="file_name">Output</h2>
			<pre class="brush: xml; output">
			E:\src>javac -d . ArrayListDemo.java

			E:\src>java com.codingraja.ArrayListDemo
			List of Course
			MCA
			MBA
			M.Tech
			B.Tech
			List of Course after Merge
			B.Com.
			B.Sc
			B.Tech
			BBA
			BCA
			M.Tech
			MBA
			MCA
			List Size=8
			List is empty: false</pre>
		</div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
