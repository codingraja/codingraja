<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Collection Interface  in Java</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

<!-- #####################  CSS and JavaScript Links ######################## -->
<%@ include file="/fragments/links.jsp" %>
    
</head>

<body>

<div class="site_wrapper">

<!-- #####################  Header Course Menu ######################## -->
<%@ include file="/fragments/header.jsp" %>


<div class="clearfix"></div>

<div class="content_fullwidth less2">
<div class="accor_dion">
<div class="container">
	
    <div class="one_third"> 	
		<!-- ###################  Table of Contents   ############################# -->
		<%@ include file="/articles/core-java/core-java-table-of-contents.jsp" %>
    </div>
    
    <!-- ############################  START BLOG HERE   ########################### -->
	<div class="two_third last">
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
		        <h1>Collection Interface</h1>
		        
		        <div class="google_horizontal_ads">
					<%@ include file="/fragments/ads/googleHorizontalAds.jsp" %>
				</div>
				
				<ol class="topic-list">
					<li>The <code>Collection</code> is a root interface in the collection hierarchy.</li>
					<li>A collection represents a group of objects, known as its elements.</li>
					<li>Some collections allow duplicate elements and others do not. Some are ordered and others unordered.</li>
					<li>The JDK does not provide any direct implementations of <code>Collection</code> interface.</li>
					<li> It provides implementations of more specific sub-interfaces like <code>List</code>, <code>Set</code> etc. </li>
				</ol>
				
				<h1>Mostly Used  Methods</h1>
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>Methods</th>
							<th>Description</th>
						</tr>
						<tr>
							<td><code>boolean add(E e)</code></td>
							<td>Adds the Elements into Collection</td>
						</tr>
						<tr>
							<td><code>boolean addAll(Collection&lt;? extends E&gt; c)</code></td>
							<td>Adds all of the elements in the specified collection to this collection</td>
						</tr>
						<tr>
							<td><code>boolean remove(Object o)</code></td>
							<td>Removes a single instance of the specified element from this collection.</td>
						</tr>
						<tr>
							<td><code>boolean removeAll(Collection&lt;?&gt; c)</code></td>
							<td>Removes all of this collection's elements that are also contained in the specified collection </td>
						</tr>
						<tr>
							<td><code>void clear()</code></td>
							<td>Removes all of the elements from this collection</td>
						</tr>
						<tr>
							<td><code>boolean contains(Object o)</code></td>
							<td>Returns true if this collection contains the specified element.</td>
						</tr>
						<tr>
							<td><code>boolean containsAll(Collection&lt;?&gt; c)</code></td>
							<td>Returns true if this collection contains all of the elements in the specified collection.</td>
						</tr>
						<tr>
							<td><code>Iterator&lt;E&gt; iterator()</code></td>
							<td>Returns an iterator over the elements in this collection.</td>
						</tr>
						<tr>
							<td><code>boolean equals(Object o)</code></td>
							<td>Compares the specified object with this collection for equality.</td>
						</tr>
						<tr>
							<td><code>int hashCode()</code></td>
							<td>Returns the hash code value for this collection.</td>
						</tr>
						<tr>
							<td><code>boolean isEmpty()</code></td>
							<td>Returns true if this collection contains no elements.</td>
						</tr>
						<tr>
							<td><code>int size()</code></td>
							<td>Returns the number of elements in this collection.</td>
						</tr>
						<tr>
							<td><code>Object[] toArray()</code></td>
							<td>Returns an array containing all of the elements in this collection.</td>
						</tr>
						<tr>
							<td><code>boolean retainAll(Collection&lt;?&gt; c)</code></td>
							<td>Retains only the elements in this collection that are contained in the specified collection </td>
						</tr>
					</table>
				</div>
				
		    </div>
		</div><!-- /# end post -->
		
		<div class="clearfix"></div>
		
		<div class="blog_post">	
	        <div class="blog_postcontent">
	        	<%@ include file="../../core-java-program-list.jsp" %>
	        </div>
	    </div>
		
		<!-- ################# Google Two Column Ads ################# -->
		<div class="google_two_col_ads"> <!-- Google Two Col Ads -->
			<%@ include file="/fragments/ads/googleTwoColAds.jsp" %>
		</div><!-- #End Google Two Col Ads -->
		
		<div class="clearfix divider_line9 lessm"></div>
    
	    <div class="pagination">
	    	<!-- Share This Post on Social -->
	    	<%@ include file="/fragments/socials.jsp" %>	    	

		    <a href="#" class="navlinks">&lt; Previous</a>
	        <a href="#" class="navlinks">Next &gt;</a>

	    </div><!-- /# end pagination -->
		<div class="clearfix margin_top1"></div>
		

		<!-- #####################   Popular Posts   #################### -->
		<div class="popular_posts">
			<%@ include file="/fragments/popular-posts.jsp" %>
		</div> <!-- #####################  End Popular Posts   #################### -->
		
		
		<!-- #####################   Start Comments and Reply   #################### -->
		<div class="comment_and_reply">
			<%@ include file="/fragments/comment-and-reply.jsp" %>
		</div> <!-- #####################  End Comments and Reply   #################### -->

    </div>
    <!-- ############################  END BLOG   ########################### -->
    
</div> <!-- end container -->
</div>
</div>


<div class="clearfix"></div>

<!-- ##################   Footer   ################# -->
<%@ include file="/fragments/footer.jsp" %>

<div class="clearfix"></div>


<a href="${resourceUrl}/#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ################## Footer Links ################# -->
<%@ include file="/fragments/footer-links.jsp" %>

</body>

</html>
