<div class="sidebar_title"><h4 class="caps">Table of Contents</h4></div>
<div class="clearfix"></div>
<div id="st-accordion-four" class="st-accordion-four">
	<ul>
		<li>
			<a href="${servletUrl}/#">Collections Framework<span class="st-arrow">Open or Close</span></a>
			<div class="st-content">
				<ol class="arrows_list1">	
					<li><a href="${coreJavaUrl}/collections-framework/collection-interface"><i class="fa fa-angle-right"></i> Collection Interface</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/iterator"><i class="fa fa-angle-right"></i> Iterator</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/collections-class"><i class="fa fa-angle-right"></i> Collections Class</a></li>
					
					<li><a href="${coreJavaUrl}/collections-framework/list"><i class="fa fa-angle-right"></i>List</a></li>	
					<li><a href="${coreJavaUrl}/collections-framework/array-list"><i class="fa fa-angle-right"></i> ArrayList</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/linked-list"><i class="fa fa-angle-right"></i> LinkedList</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/vector"><i class="fa fa-angle-right"></i> Vector</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/stack"><i class="fa fa-angle-right"></i> Stack</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/comparable"><i class="fa fa-angle-right"></i> Comparable Interface</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/comprator"><i class="fa fa-angle-right"></i> Comparator Interface</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/comprable-vs-comparator"><i class="fa fa-angle-right"></i> Comparable Vs Comparator</a></li>
					
					<li><a href="${coreJavaUrl}/collections-framework/set"><i class="fa fa-angle-right"></i> Set</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/hash-set"><i class="fa fa-angle-right"></i> HashSet</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/linked-hash-set"><i class="fa fa-angle-right"></i> LinkedHashSet</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/tree-set"><i class="fa fa-angle-right"></i> TreeSet</a></li>
					
					<li><a href="${coreJavaUrl}/collections-framework/map"><i class="fa fa-angle-right"></i> Map</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/hash-map"><i class="fa fa-angle-right"></i> HashMap</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/linked-hash-map"><i class="fa fa-angle-right"></i> LinkedHashMap</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/tree-map"><i class="fa fa-angle-right"></i> TreeMap</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/identity-hash-map"><i class="fa fa-angle-right"></i> IdentityHashMap</a></li>
					<li><a href="${coreJavaUrl}/collections-framework/weak-hash-map"><i class="fa fa-angle-right"></i> WeakHashmap</a></li>
				</ol>
			</div>
		</li>
		
	</ul>
</div>



<!-- #####################   Recent Posts   #################### -->
<div class="recent_posts">
	<%@ include file="/fragments/recent-posts.jsp" %>
</div> <!-- #####################  End Recent Posts   #################### -->