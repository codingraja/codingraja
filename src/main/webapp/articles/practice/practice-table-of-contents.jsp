<div class="sidebar_title"><h4 class="caps">Table of Contents</h4></div>
<div class="clearfix"></div>
<div id="st-accordion-four" class="st-accordion-four">
	<ul>
		<li>
			<a href="${servletUrl}/#">Core Java Practice Paper<span class="st-arrow">Open or Close</span></a>
			<div class="st-content">
				<ol class="arrows_list1">	
					<li><a href="${coreJavaUrl}/"><i class="fa fa-angle-right"></i> Exercise - I</a></li>
				</ol>
			</div>
		</li>
		
	</ul>
</div>



<!-- #####################   Recent Posts   #################### -->
<div class="recent_posts">
	<%@ include file="/fragments/recent-posts.jsp" %>
</div> <!-- #####################  End Recent Posts   #################### -->