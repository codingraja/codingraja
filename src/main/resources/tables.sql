CREATE TABLE contact_master(
	contact_id BIGINT UNIQUE NOT NULL AUTO_INCREMENT,
	full_name VARCHAR(50),
	email VARCHAR(50),
	query VARCHAR(200),
	description VARCHAR(2000),
	PRIMARY KEY(contact_id)
);